<?php
function actionTemplateList() {
    global $dbh; $message = ''; $code = 404; $templates = array(); 
    $temp_res = $dbh->query("select * from templates");
    if($temp_res) {
        while($temp_row = $temp_res->fetch_assoc()) {
            $obj = new stdClass();
            $obj->templateCode = $temp_row['templateCode'];
            $obj->templateName = $temp_row['templateName'];
            $obj->path = $temp_row['path'];
            $obj->templateType = $temp_row['templateType'];
           
            array_push($templates, $obj);
        }
        $code = 200;
        $message = 'Successful';
    }
    else {
        $message = 'No templates available';
    }
    
    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'templates' => $templates));
}


function actionTemplateContents() {
    $message = ''; $tempData = '';  $code = 404; $params = ''; global $dbh;
    $tmpID = $_SERVER['HTTP_TEMPLATECODE'];
    $res = $dbh->query("select * from templates where templateCode = '".$tmpID."'");
        if(mysqli_num_rows($res) > 0) {
            $row = $res->fetch_assoc();
            $tempData = utf8_encode($row['path']);
            $code = 200;
            $message = 'Template found';  
        }
        else {
            $message = 'Template not found';
        }
    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'response' => $tempData));
}