<?php

function actionOrgsList() {
     global $dbh; $message = ''; $code = 404; $orgs = array(); 
   $res = $dbh->query("SELECT o.id, o.code, o.org_name, o.emp_count, o.logo, o.org_type, o.status, o.created_on, ot.id AS contact_person_id, CONCAT(ot.first_name, ' ', ot.last_name) AS contact_person, ot.first_name, ot.last_name, ot.designation, ot.official_email, ot.contact_no FROM organizations AS o LEFT JOIN organization_team AS ot ON ot.org_id = o.id ORDER BY o.id ASC");
    if($res) { 
        while($row = $res->fetch_assoc()) {
           array_push($orgs, $row);
        }
        $code = 200;
        $message = 'Successful';
    }
    else {
        $message = 'No organizations available';
    }
    
    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'orgInfo' => $orgs));
}

function getOrganizationProfileDetails() {
    $message = '';   $code = 404;  global $dbh; $result = array();

    $params = file_get_contents("php://input");
    $params = json_decode($params);

    $get_org_details = $dbh->query("SELECT id, code, org_name, emp_count, industry, address_1, address_2, city, zipcode, state, country, telephone, email, website, logo, status, org_desc, org_type FROM organizations WHERE id = ".$params->org_id);


    $result = mysqli_fetch_assoc($get_org_details);

    if($result) {
        $code = 200;
        $message = "Success";
    }
    else {
        $code = 404;
        $message = "No data found";
    }

    http_response_code($code);
    echo json_encode(array('code' => $code, 'message' => $message, "org_details" => $result));
}