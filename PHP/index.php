<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With, content-type,  templateCode, encryptedValue, authorization');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT');
header('Content-Type: application/json');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('inc.config.php');

ini_set('display_errors', 1);

$dbh = mysqli_connect($host, $dbuser, $dbpass, $dbname);
// Below line is the encryption for Project code. Please do not tamper. - YRB
//$returnValue = hash('adler32', '{"projectId": 1, "projectType": "LS"}');
//$survey_link = hash('adler32', '{"projectId": 2, "userId": 22, "userEmail": "yrb@fp.technology"}');
//echo $survey_link; die;
//echo $returnValue; die;

// $timeTarget = 0.05; // 50 milliseconds 
// $cost = 8;
// do {
//     $cost++;
//     $start = microtime(true);
//     password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
//     $end = microtime(true);
// } while (($end - $start) < $timeTarget);

// echo "Appropriate Cost Found: " . $cost;
// die;

//echo password_hash('password', PASSWORD_BCRYPT, ["cost" => 10]); die;

///////////// Includes Section ////////////////////////////
require('./PHPMailer_5.2.0/class.phpmailer.php');
require_once('inc.login.php');
require_once('inc.org.php');
require_once('inc.employees.php');
require_once('inc.templates.php');
require_once('inc.investors.php');
require_once('inc.surveys.php');
require_once('inc.projects.php');
require_once('inc.mailer.php');
require_once('inc.json.php');
require_once('inc.users.php');
require_once('inc.lsreport.php');
require_once('inc.lsreport1.php');
require_once('inc.datalake.php');

$path = ltrim($_SERVER['REQUEST_URI'], '/');    // Trim leading slash(es)
$elements = explode('/', $path);     

if($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
    die;
}
if(empty($elements[0])) {                       // No path elements means home
    ShowHomepage();
} else switch(array_shift($elements))             // Pop off first item and switch
{
    case 'login':
        actionLogin(); // passes rest of parameters to internal function
        break;
        
    case 'organizations':
        actionOrgsList(); // passes rest of parameters to internal function
        break;
	case 'employees':
        actionOrgsEmpList(); // passes rest of parameters to internal function
        break;
    case 'templates':
        actionTemplateList(); // passes rest of parameters to internal function
        break;
	 case 'htmlContents':
        actionTemplateContents(); // passes rest of parameters to internal function
        break;
    case 'investors':
        actionInvestorsList($elements); // passes rest of parameters to internal function
        break;
	case 'sendEmail':
		//header('Content-Type: text/html');
        actionSendMail();
        break;
	case 'userSurvey':
        actionUserSurvey();
           break; 
	case 'updateSurveyStatus':
        actionUserSurveyUpdate();
           break; 
    case 'json':
        actionGetJsonData($elements);
        break;
    case 'verifyProject':
        actionGetProjectData($elements);
        break;
    case 'registerUser':
        actionRegisterUser($elements);
        break;
    case 'getSurvey':
        actionGetSurvey($elements);
        break;
     case 'getCaSurvey':
        actionGetCaSurvey($elements);
        break;   
    case 'saveSurvey':
        actionSaveSurvey($elements);
        break;
    case 'saveCaSurvey':
        actionSaveCaSurvey($elements);
        break;
    case 'projects':
            actionProjects($elements);
            break;
    case 'project':
            actionProject($elements);
            break;
    case 'lprojects':
            actionLeaderProjects($elements);
            break;
    case 'lproject':
            actionLeaderProject($elements);
            break;
	 case 'codegen':
		 header('Content-Type: text/html');
        require_once('codegen.html');
        break; 
    case 'lsreport':
            actionLeadershipReport($elements);
            break;
    case 'lsreport1':
            actionLeadershipReport1($elements);
            break; 
     case 'lsreport2':
            actionLeadershipUniverse1($elements);
            break;  

    case 'getnetworkdata':
            actionGetAllEmployeesSurvayAnswers($elements);
            break;
    case 'getlastuserdata':
            actionGetLatestEmployeesList();
            break;
    case 'addGenericData':
        actionAddAllEmployeessData($elements);
        break;  
        
    case 'getJobTeamOrgData':
        getJobTeamOrganizationData();
        break;  
        
    case 'getAdaptabilityQuotientScore':
        getAdaptabilityQuotientScore($elements);
        break;   

    case 'dlregistration':
     adddlregistration($elements);
        break;
    case 'getCompanyNameOfEmp':
        getCompanyNameOfEmp($elements);
        break;

    case 'addContactPersonDetails':
        addContactPersonDetails();
        break;

    case 'approveRegistrationRequest':
        approveRegistrationRequest($elements);
        break;

    case 'comASurveyMail':
        sendComAssSurveyMail($elements);
        break;
    case 'getOrganizationProfileDetails':
        getOrganizationProfileDetails($elements);
        break;

    case 'validateEmployees':
        validateEmployees();
        break;

    case 'uploadEmployees':
        uploadEmployees();
        break;

    case 'getOrganizationEmployees':
        getOrganizationEmployees();
        break;

    case 'requestProject':
        requestProject();
        break;
    case 'getComAssSurveyResult':
        getComAssSurveyResult();
        break;    default:
        header('HTTP/1.1 404 Not Found');
        Show404Error();
};

function showHomePage() {
    $message = 'Hello world'; $code = 200; 
    http_response_code($code);
	echo $message; die;
    echo json_encode(array('message' => $message, 'code' => $code));
	die;
}
function Show404Error() {
    $message = 'Sorry'; $code = 404; 
    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code));
}
