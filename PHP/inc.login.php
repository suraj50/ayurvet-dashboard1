<?php

	function actionLogin() {
		$message = ''; $code = 404; $params = ''; global $dbh; $userObj = new stdClass();
		// parse_str(file_get_contents("php://input"),$post_vars);
		
		// foreach($post_vars as $data=> $dummy) {
		// 	$params = json_decode($data);
		// }
		
		$params = file_get_contents("php://input");
		$params = json_decode($params);
		
		if($params !== '' && $params->username !== null && $params->username !== '' && $params->password !== null && $params->password !== '') {
			$res = $dbh->query("select * from users where username = '".$params->username."' or email = '".$params->username."'");
			if($res && mysqli_num_rows($res) > 0) {
				while($row = $res->fetch_assoc()) { 
					$user = (object) $row;
				}
				if (password_verify($params->password, $user->password)) {
					$code = 200;
					$message = 'Successful login';
					$userObj = $user;
					$userObj->token = generateJWT($userObj->id);
					unset($userObj->password);
				}
				else {
					$message = 'password incorrect';
				}    
			}
			else {
				$message = 'User not found';
			}
		}
		else {
			$code = 400;
			if($params->username === null || $params->username === '') {
				$message = 'Username is empty';
			}
			else if($params->password === null || $params->password === '') {
				$message = 'password is empty';
			}
		}
		
		http_response_code($code);
		echo json_encode(array('message' => $message, 'code' => $code, 'user' => $userObj));
	}

	function generateJWT($user_id) {
		$header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

		// Create token payload as a JSON string
		$payload = json_encode(['user_id' => $user_id]);

		// Encode Header to Base64Url String
		$base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

		// Encode Payload to Base64Url String
		$base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

		// Create Signature Hash
		$signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'abC123!', true);

		// Encode Signature to Base64Url String
		$base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

		// Create JWT
		$jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

		return $jwt;
	}