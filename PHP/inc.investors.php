<?php

function actionInvestorsList($params) {
     global $dbh; $message = ''; $code = 404; $investors = array(); 
     if($params[0] !== null && $params[0] !== '') {
        $res = $dbh->query("select i.*, o.ORG_NAME from investors i join organizations o on o.ORG_CODE = i.ORG_CODE where i.ORG_CODE = " . $params[0] . "");
        if($res) {
            while($row = $res->fetch_assoc()) {
                $obj = new stdClass();
                $obj->invCode = $row['INV_CODE'];
                $obj->orgCode = $row['ORG_CODE'];
                $obj->orgName = $row['ORG_NAME'];
                $obj->invName = $row['INV_NAME'];
                $obj->invEmail = $row['INV_EMAIL'];
                $obj->link = $row['LINK'];

                $expFlag = true; $activeTime = "null";
                if(!$row['START_DATETIME']) {
                    $expFlag = true; $activeTime = "null";
                }
                else {
                    $date1 = new DateTime($row['START_DATETIME']);
                    $date2 = new DateTime();
                    $diff = $date2->diff($date1);
                    $hours = $diff->h;
                    $hours = $hours + ($diff->days*24);

                    if ($hours >= $row['EXP_TIME']) {
                        $expFlag = true;
                        $activeTime = "null";
                    } else {
                        
                        $expFlag = true;
                        $activeTime = 'null'; //(48 * 60 * 60) - (Number(hours[0]) * 3600 + Number(hours[1]) * 60 + Number(hours[2]))
                    }



                }

            
                array_push($investors, $obj);
            }
            $code = 200;
            $message = 'Successful';
        }
        else {
            $message = 'No investors available';
        }
     }
     else {
         $message = 'Organization ID is empty';
     }
    
    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'investors' => $investors));
}