<?php

function actionOrgsEmpList() {

    $message = ''; $code = 404; $params = ''; global $dbh; $empployees = array(); $statusPercentage = '';

    $path = ltrim($_SERVER['REQUEST_URI'], '/');    // Trim leading slash(es)

    $elements = explode('/', $path);

    $orgid = $elements[1];

    $qry=$dbh->query("select id,SURVEY_STATUS,END_DATE from organizations where id='".$orgid."'");

    //$qry=$dbh->query("select id,SURVEY_STATUS,END_DATE from organizations where id='".$orgid."'");

     $org = $qry->fetch_assoc();

    $res = $dbh->query("select * from organization_employees where ORG_ID='".$orgid."'");

   if (mysqli_num_rows($res) > 0){

        while($row = $res->fetch_assoc()) {

            $obj = new stdClass();

            $vendors = array();

            $vendors1 = array();



            $eid = $row['id'];

            $surveyStatusNum = $dbh->query("select * from organization_emp_vendor where ORG_ID='".$orgid."' and EMP_ID='".$eid."' and SURVEY_STATUS = 'completed'");

              $surveyStatusNum_no = mysqli_num_rows($surveyStatusNum);

            $vendor_res = $dbh->query("select * from organization_emp_vendor where ORG_ID='".$orgid."' and EMP_ID='".$eid."'");

             $surveyStatusNumTotal_no = mysqli_num_rows($vendor_res);

             $statusPercentage = (($surveyStatusNum_no / $surveyStatusNumTotal_no) * 100);

            if($vendor_res) {

                while($vendor_row = $vendor_res->fetch_assoc()) {

                    $ven_obj = new stdClass();

                    $ven_obj->surveyName = $vendor_row['SURVEY_NAME'];

                    $ven_obj->surveyLink = $vendor_row['SURVEY_LINK'];

                    $ven_obj->surveyStatus = $vendor_row['SURVEY_STATUS'];

                    $ven_obj->surveySent = $vendor_row['SURVEY_SENT'];

                    $ven_obj->surveyCode = $vendor_row['VENDOR_CODE'];

                    $ven_obj->serialNo = $vendor_row['SERIAL_NO'];



                     array_push($vendors, $ven_obj);

                }

            }

            $obj->empCode = $row['EMP_CODE'];

            $obj->empName = $row['EMP_NAME'];

            $obj->empEmailId = $row['EMAIL_ID'];

            $obj->empRole = $row['ROLE'];

            $obj->vendors = $vendors;

           

            // $obj->surveyStatus = $org['SURVEY_STATUS'];

            $obj->surveyStatus = $statusPercentage.'%';

            $obj->EndDate = $org['END_DATE'];



            array_push($empployees, $obj);

        }

        $code = 200;

        $message = 'Successful';

    }

    else {

        $message = 'Organization data not found';

    }

    

    http_response_code($code);

    echo json_encode(array('message' => $message, 'code' => $code, 'employees' => $empployees));

}

function actionGetLatestEmployeesList() { 
    global $dbh; $message = ''; $code = 404; $usrlist = array(); $cvaq1_qoption_list = array(); $cvaq2_qoption_list = array(); $cvaq3_qoption_list = array(); $qst_list = array();
    
    $res = $dbh->query("select pe.*,CONCAT(pe.`first_name`,' ',pe.`last_name`) AS employee_name, p.project_title as project_name from project_employees pe left join projects p on p.id = pe.project_id order by pe.id desc limit 10");
    if($res) {
        while($row = $res->fetch_assoc()) {
            $obj = (object) $row;
            array_push($usrlist, $obj);
        }
        $code = 200;
        $message = 'Successful';
    }
    else {
        $message = 'Employees not available';
    }

    $qlist =  $dbh->query("SELECT * from project_survey_questions where survey_id = 8");
    if($qlist) {
        while($qrlist = $qlist->fetch_assoc()) {
           array_push($qst_list, $qrlist);
        }
    }

    $qo_res = $dbh->query("SELECT option_value,option_label FROM `project_survey_options` WHERE question_id = 13 AND question_type = 'cva'");
    if($qo_res) {
        while($qo_row = $qo_res->fetch_assoc()) {
            array_push($cvaq1_qoption_list, $qo_row);
        }
    }

     $q2o_res = $dbh->query("SELECT option_value,option_label FROM `project_survey_options` WHERE question_id = 14 AND question_type = 'cva'");
    if($q2o_res) {
        while($q2o_row = $q2o_res->fetch_assoc()) {
            array_push($cvaq2_qoption_list, $q2o_row);
        }
    }

    $q3o_res = $dbh->query("SELECT option_value,option_label FROM `project_survey_options` WHERE question_id = 15 AND question_type = 'cva'");
    if($q3o_res) {
        while($q3o_row = $q3o_res->fetch_assoc()) {
            array_push($cvaq3_qoption_list, $q3o_row);
        }
    }



    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'employeeList' => $usrlist, 'cvaq1option' => $cvaq1_qoption_list, 'cvaq2option' => $cvaq2_qoption_list, 'cvaq3option' => $cvaq3_qoption_list, 'cvaqusrtions' => $qst_list ));
}

function actionAddAllEmployeessData($params) { 
   $message = '';   $code = 404;  global $dbh; $user = new stdClass(); $result = array();
       if($params !== null && $params !== '' ) {
        // var_dump($params);

            $postdata = file_get_contents("php://input");
            $user = json_decode($postdata);
            $dt = date('Y/m/d');
            $qry = "insert into project_employees (project_id, first_name, last_name, gender, age_group,merger_bank, duration_at_SBI, type_of_customer, account_type,bank_code,branch_name,region,module, modified_on,completed_on,created_on,created_by,user_type) values ('9','".$user->user_fname."', '".$user->user_lname."', '".$user->user_gender."', '".$user->user_age_group."', '".$user->user_merger_bank."', '".$user->user_duration_at_SBI."', '".$user->user_type_of_customer."', '".$user->user_account_type."', '".$user->user_bank_code."', '".$user->user_branch."','".$user->user_region."', '".$user->user_module."','19-07-2019 10.34','19-07-2019 10.34', '".$dt."','".$user->user_added_by."','customer')";
            // echo $q;
                if($dbh->query($qry) === TRUE) {
                        $user->id = mysqli_insert_id($dbh);
                        // echo $user->id;
                        for($i=0;$i<10;$i++){
                            $answr = $user->userAnswersdata[$i]->option_value;
                            $jk = $i+1;
                            $surans = "INSERT INTO project_emp_survey_answers (project_id,survey_id,employee_id,question_id,answer,answer_order) VALUES ('9','8','".$user->id."','13','".$answr."','".$jk."')";
                            $eadd = $dbh->query($surans);

                        }

                        for($j=0;$j<10;$j++){
                            $answr2 = $user->userAnswersdataq2[$j]->option_value;
                            $m = $j+1;
                            $surans1 = "INSERT INTO project_emp_survey_answers (project_id,survey_id,employee_id,question_id,answer,answer_order) VALUES ('9','8','".$user->id."','14','".$answr2."','".$m."')";
                            $eadd1 = $dbh->query($surans1);

                        }

                        for($n=0;$n<10;$n++){
                            $answr2 = $user->userAnswersdataq3[$n]->option_value;
                            $z = $n+1;
                            $surans2 = "INSERT INTO project_emp_survey_answers (project_id,survey_id,employee_id,question_id,answer,answer_order) VALUES ('9','8','".$user->id."','15','".$answr."','".$z."')";
                            $eadd2 = $dbh->query($surans2);

                        }

                        // print_r($user->userAnswersdata[0]->option_value);
                        $code = 200;
                        $message = 'Success';
                }                   
                else {
                    // echo $dbh->error;
                    $code = 403;
                    $message = 'Failed to add data, try again later.';
                }
        http_response_code($code);
        echo json_encode(array('code' => $code, 'message' => $message, 'user' => $user));
    }
}

function getCompanyNameOfEmp($params) {
    // echo "test";
    $message = '';   $code = 404;  global $dbh; $result = array(); $survey_link = '';
    if($params !== '' && $params !== null) { 
        $survey_link = $params[0];

        $get_emp_details = "SELECT first_name,last_name,company_email,company_name FROM `project_employees` WHERE survey_link = '". $survey_link."'";
        
        $res = $dbh->query($get_emp_details);
	if($res) {
		while($row = $res->fetch_assoc()) {
			$survey = $row;
		}
	}

        //$result = @mysqli_fetch_assoc($res);
        //$company_name = $result['company_name'];
	
        // print_r($result);die;

        if($survey) {
            $code = 200;
            $message = 'Success';
        }
        else {
            $code = 403;
            $message = 'Failed';
        }

        http_response_code($code);
        echo json_encode(array('code' => $code, 'message' => $message, 'persondetail' => $survey));
    }
}

function validateEmployees() {
    $message = '';   $code = 404;  global $dbh; $result = array(); $error_flag = false;

    $postdata = file_get_contents("php://input");
    $postdata = json_decode($postdata);
    $org_id = $postdata->orgid;
    $user_records = $postdata->csvrecords;

    // print_r($org_id);

    foreach ($user_records as $user_row) {
        $error = '';
        // print_r($user_row);die;
        
        // Check if user id already exists
        $sql = "SELECT company_email FROM organization_employees WHERE org_id = ".$org_id." AND company_email = '".$user_row->company_email."'";
        $check_user = $dbh->query($sql);
        // print_r($check_user);die;
        if($user_row->existinguser == 0 && $check_user->num_rows > 0) {
            $error = 'User already exists.';
            $error_flag = true;
        }
        else if($user_row->existinguser == 1 && $check_user->num_rows == 0) {
            $error = 'User does not exists. ';
            $error_flag = true;
        }

        // Validate first name
        $validate_fname =preg_match("/^([a-zA-Z']+)$/ix", $user_row->firstname);

        if($validate_fname == 0) {
            $error .= 'Invalid first name. ';
        }

        // Validate last name
        $validate_lname =preg_match("/^([a-zA-Z']+)$/ix", $user_row->lastname);

        if($validate_lname == 0) {
            $error .= 'Invalid last name. ';
        }

        // Validate user email
        $validate_email = preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $user_row->company_email);

        if($validate_email == 0) {
            $error .= 'Invalid email id. ';
            $error_flag = true;
        }

        // Validate new email
        if($user_row->new_email != '' && $user_row->existinguser == 1) {
            $validate_newemail = preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $user_row->new_email);

            if($validate_newemail == 0) {
                $error .= 'Invalid new email id. ';
                $error_flag = true;
            }
            else {
                $sql = "SELECT company_email FROM organization_employees WHERE org_id = ".$org_id." AND company_email = '".$user_row->new_email."'";
                $check_newemail = $dbh->query($sql);

                if($check_newemail->num_rows > 0) {
                    $error .= 'New email id is already exists. ';
                    $error_flag = true;
                }
            }

           
        }
        if($user_row->new_email != '' && $user_row->existinguser == 0) {
            $error .= 'New email id is only used for existing user. ';
            $error_flag = true;
        }


        // print_r($validate_email);

        $user_row->error = $error;
        array_push($result, $user_row);
    }

    // print_r($result);
    if($result) {
        $code = 200;
    }

    http_response_code($code);
    echo json_encode(array('code' => $code, 'message' => $message, 'validation_summary' => $result, 'all_records_valid' => $error_flag));
}

function uploadEmployees() {
    $message = '';   $code = 200;  global $dbh; $result;

    $postdata = file_get_contents("php://input");
    $postdata = json_decode($postdata);
    // print_r($postdata); 
    $org_id = $postdata->orgid;
    $user_id = $postdata->userid;
    $user_records = $postdata->csvrecords;
    // print_r($org_id);die;

    foreach ($user_records as $user_row) {

        if($user_row->existinguser == 0) {
            $insert_sql = "INSERT INTO organization_employees (org_id, first_name, last_name, dob, gender, marital_status, company_email, designation, total_experience, prefered_language, department, address, created_on, mobile) VALUES(".$org_id.",'".$user_row->firstname."', '".$user_row->lastname."', '".$user_row->dob."', '".$user_row->gender."','".$user_row->marital_status."', '".$user_row->company_email."', '".$user_row->designation."','".$user_row->total_experience."', '".$user_row->prefered_language."', '".$user_row->department."', '".$user_row->address."', now(), '".$user_row->mobile."')";
            // echo $insert_sql;
            $result = $dbh->query($insert_sql);
            // print_r($result);
        }
        else if($user_row->existinguser == 1) {
            $company_email = $user_row->company_email;
            if($user_row->new_email != '') {
                if($user_row->company_email !== $user_row->new_email) {
                    $company_email = $user_row->new_email;
                }
            }

            $update_sql = "UPDATE organization_employees SET first_name = '".$user_row->firstname."', last_name = '".$user_row->lastname."', dob = '".$user_row->dob."', gender = '".$user_row->gender."', marital_status = '".$user_row->marital_status."', company_email = '".$company_email."', designation = '".$user_row->designation."', total_experience = '".$user_row->total_experience."', prefered_language = '".$user_row->prefered_language."', department = '".$user_row->department."', address = '".$user_row->address."', modified_on = now(), modified_by = ".$user_id.", mobile = '".$user_row->mobile."' WHERE org_id = ".$org_id." AND company_email = '".$user_row->company_email."'";
            
            $result = $dbh->query($update_sql);
        }
    }

    http_response_code($code);
    echo json_encode(array('code' => $code, 'message' => 'Success'));
}

function getOrganizationEmployees() {
    $message = '';   $code = 404;  global $dbh; $result = array();

    $org_id = file_get_contents("php://input");
// echo $org_id;
    $sql = "SELECT o.org_name, oe.org_id, oe.first_name, oe.last_name, oe.dob, oe.gender, oe.marital_status, oe.company_email, oe.designation, oe.total_experience, oe.prefered_language, oe.department, oe.address, oe.modified_by, oe.created_on, oe.modified_on, oe.mobile FROM organizations AS o INNER JOIN organization_employees AS oe ON o.id = oe.org_id WHERE oe.org_id = ".$org_id;

    $res = $dbh->query($sql);

    if($res->num_rows > 0) {
        while ($row = $res->fetch_assoc()) {
            array_push($result, $row);
        }
    }

    if($result) {
        $code = 200;
        $message = 'Success';
    }
    else {
        $message = 'No data found';
    }

    http_response_code($code);
    echo json_encode(array('code' => $code, 'message' => 'Success', 'org_employees' => $result));
}