<?php
function approveRegistrationRequest($params) {
	$message = '';   $code = 404;  global $dbh; $user = new stdClass(); $result = array();
	$postdata = file_get_contents("php://input");
	$usr = json_decode($postdata);
	date_default_timezone_set('Asia/Kolkata');
	$dt = date('d-m-Y H:i');

	// print_r($usr);

	$sql = "INSERT INTO users (first_name, last_name, telephone, email, org_id, role_code, status, created_on) VALUES ('".$usr->first_name."','".$usr->last_name."','".$usr->contact_no."','".$usr->official_email."',".$usr->org_id.",'org_contact',1,now())";
	// echo $sql;die;
	$insert_user = $dbh->query($sql);

	if($insert_user) {
		$code = 200;
		$message = 'Success';
		$user_id = mysqli_insert_id($dbh);
		// $username = "orgcontact".$user_id;
		$username = $usr->official_email;
		// set password
		$pwd = htmlspecialchars('Mna'.$user_id);
		$password = password_hash($pwd, PASSWORD_BCRYPT, ["cost" => 10]);

		$set_password = $dbh->query("UPDATE users SET password = '". $password ."' WHERE id = ". $user_id);

		// Update Pending status to Approved
		$update_status = $dbh->query("UPDATE organizations SET status = 1 WHERE id = ".$usr->org_id);

		if($update_status) {
			// Write code for mail sending 
			$subject = "Registration Request";
			$body = "Hi ". $usr->first_name ." ". $usr->last_name . ", <br> Your organization registration request is approved. Please find below credentials to login to Pulse, <br> URL: <a href='http://localhost:4200/#/login?returnUrl=%2Fmain'>http://localhost:4200/#/login</a><br>Username: ". $username ."<br>Password: ". $pwd;
			$mailsent = sendMail($usr->official_email, $subject, $body);
		}
	}
	else {
		$code = 403;
		$message = 'Failed to register, try again later.';
	}

	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message));
}
?>