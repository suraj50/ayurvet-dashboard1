<?php
function actionLeadershipReport($params) {
	$message = '';   $code = 404;  global $dbh; $lsr = new stdClass(); $surveys = new stdClass();$cva_question = new stdClass(); $surveyans = array();$lpmasterData = array(); $result = array();

	if($params !== null && $params !== '' && $params[0] !== null && $params[0] !== '' ) {
		$res = $dbh->query("SELECT * FROM project_leadership_report where id = '" .$params[0]."'");
		if($res && mysqli_num_rows($res) > 0) { 
			while($row = $res->fetch_assoc()) {
				$lsr = (object) $row;

				$lsr->report_date = Date('jS F, Y', strtotime($lsr->report_date));

				// fetch project data
				$prs = $dbh->query("select * from projects where id = " . $lsr->project_id);
				if($prs && mysqli_num_rows($prs) > 0) {
					while($prow = $prs->fetch_assoc()) {
						$lsr->project = (object) $prow;
					}
				}

				// fetch user data
				$urs = $dbh->query("select * from project_employees where id = " . $lsr->employee_id);
				if($urs && mysqli_num_rows($urs) > 0) {
					while($urow = $urs->fetch_assoc()) {
						$lsr->employee = (object) $urow;
					}
				}

				// fetch themes data
				$lsr->themes = array();
				$trs = $dbh->query("select * from project_lr_themes where lrid = " . $lsr->id);
				if($trs && mysqli_num_rows($trs) > 0) {
					while($trow = $trs->fetch_assoc()) {
						array_push($lsr->themes, (object) $trow);
					}
				}

				// fetch universes data
				$lsr->universes = array();
				$urs = $dbh->query("select * from project_lr_universes where lrid = " . $params[0] . "");
				if($urs && mysqli_num_rows($urs) > 0) {
					while($urow = $urs->fetch_assoc()) {
						array_push($lsr->universes, (object) $urow);
					}
					$code = 200;
					$message = 'Successful'; 
				}

				// fetch values master
				$lsr->values = array();
				$vrs = $dbh->query("SELECT * FROM project_lp_values");
				if($vrs && mysqli_num_rows($vrs) > 0) {
					while($vrow = $vrs->fetch_assoc()) {
						array_push($lsr->values, $vrow);
					}
				}

				// fetch user survey answers
				$lsr->cva_answers = array();
				$srs = $dbh->query("SELECT * FROM project_surveys where project_id = " .$lsr->project_id." and  survey_code = 'cva'");
				if($srs && mysqli_num_rows($srs) > 0) { 
					while($srow = $srs->fetch_assoc()) {
						$cva_survey = (object) $srow; 
					}

					$qrs = $dbh->query("SELECT * FROM  project_survey_questions where survey_id = " .$cva_survey->id ." and lower(question_text) = 'personal values' ");
					if($qrs && mysqli_num_rows($qrs) > 0) { 
						while($qrow = $qrs->fetch_assoc()) {
							$cva_question = (object) $qrow; 
						}

						$usrs = $dbh->query("SELECT * FROM project_emp_survey_answers where question_id = " .$cva_question->id ." and employee_id = " .$lsr->employee_id );
						if($usrs && mysqli_num_rows($usrs) > 0) {
							while($usrow = $usrs->fetch_assoc()) {				
								array_push($lsr->cva_answers, (object) $usrow);
							}
						}

					}

				}

			}
			
			$code = 200;
			$message = 'Successful'; 
		}
	}
	else {
		$code = 403; $message = 'This LSR id is not valid.';
	}

	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message, 'lsr' => $lsr));
    
}