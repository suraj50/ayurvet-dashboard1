<?php 
function actionSendMail() {

     $message = ''; $tempData = '';  $code = 404; $params = ''; global $dbh; global $baseurl;
    $post_vars = file_get_contents("php://input");
      //parse_str(file_get_contents("php://input"),$post_vars);
    $params = json_decode($post_vars);
    
  $tempData = "Test";
  $code = 200;
  $message = $params; $surveys = array(); $data = array(); $pr = array(); $encrypted_string1 = ''; $responses = array();
    if($params !== '' && $params !== null) {

		foreach($params as $param) {
		  if(@$surveys[$param->orgCode] === null || @$surveys[$param->orgCode] === '') {
			$surveys[$param->orgCode] = array();
		  }
		  if(@$surveys[$param->orgCode][$param->empCode] === null || @$surveys[$param->orgCode][$param->empCode] === '') {
			$emp_qry = $dbh->query("select * from organization_employees where ORG_ID='". $param->orgCode ."' and EMP_CODE = '" .$param->empCode. "'");
			$emp_row = $emp_qry->fetch_assoc();

			$surveys[$param->orgCode][$param->empCode] = new stdClass();
			$surveys[$param->orgCode][$param->empCode]->email = $param->email;
			$surveys[$param->orgCode][$param->empCode]->empCode = $param->empCode;
			$surveys[$param->orgCode][$param->empCode]->subject = $param->subject;
			$surveys[$param->orgCode][$param->empCode]->orgUnit = $param->serialNo;
			$surveys[$param->orgCode][$param->empCode]->emp_name = $emp_row['EMP_NAME'];
			$surveys[$param->orgCode][$param->empCode]->mail_body = $param->content;
			$surveys[$param->orgCode][$param->empCode]->surveys = array();
		  }
		  array_push($surveys[$param->orgCode][$param->empCode]->surveys, $param);
		}
		
		foreach($surveys as $orgCode => $org) {
		  foreach($org as $empCode => $emp) {
			global $error;
			$to = $emp->email;
		   
			$str = '[{"email": "'. $emp->email .'", "orgCode": "'. $orgCode .'"}]';
			$encrypted = openssl_encrypt($str, "AES-128-ECB", "1660");   


			$appurl = $baseurl."/Cbsurvey?code=";
			$com_url = $appurl.urlencode($encrypted);
			
			$str_1= str_replace("emp_name",$emp->emp_name,$emp->mail_body);
			$str_2 = str_replace("URL",$com_url,$str_1);
			 
			$q = $dbh->query("INSERT into organization_emp_surveys (encryptedUrl,orgCode,empCode, createdon) values('".$encrypted."','".$orgCode."','".$emp->empCode."', '".date("Y-m-d H:i:s",time())."')");
			$qid = $dbh->insert_id;
			
			// 4. update survey record status as survey sent true
			foreach($emp->surveys as $survey) {
			  $dbh->query("update organization_emp_vendor set SURVEY_SENT = 'TRUE', EMP_SURVEY_ID = '".$qid."' where ORG_ID = '".$orgCode."' && SERIAL_NO = '".$survey->serialNo."'");
			}
			
			$mailsent = sendMail($emp->email, $emp->subject, $str_2);			
			
			$robj = new stdClass();
			$robj->mail = $emp->email; $robj->message = 'mail is sent';
			array_push($responses, $robj);
			
		  }
		}
		
		$code = 200;
    }
	http_response_code($code);
    echo json_encode(array('code' => $code, 'emails' => $responses));
}


function actionUserSurvey() {
    $message = ''; $tempData = '';  $code = 404; $params = ''; global $dbh;
	
    $encryptedData = $_SERVER['HTTP_ENCRYPTEDVALUE'];
	$encrypted_string = urldecode($encryptedData); $orgDetails = new stdClass();
	if($encrypted_string !== null && $encrypted_string !== '') {
		$res = $dbh->query("select s.*, o.ORG_NAME, o.ORG_LOGO from organization_emp_surveys s LEFT JOIN organizations o on o.ORG_CODE = s.orgCode where s.encryptedUrl = '".$encrypted_string."'");
		if($res) {
			while($row = $res->fetch_assoc()) {
				$survey = $row;
			}
			if($survey) {
				$orgDetails->orgCode = $survey['orgCode'];
				$orgDetails->orgName = $survey['ORG_NAME'];
				$orgDetails->Survey = array();
				$orgDetails->imageToBase64 = '';
				$orgDetails->surveyStatus = '0%';

				
				$sres = $dbh->query("select * from organization_emp_vendor where EMP_SURVEY_ID = ".$survey['id']);
				$total_surveys = 0; $completed_surveys = 0;
				if($sres) {
					while($srow = $sres->fetch_assoc()) {
						$esurvey = new stdClass();
						$esurvey->surveyLink = $srow['SURVEY_LINK'];
						$esurvey->surveyStatus = $srow['SURVEY_STATUS'];
						$esurvey->serialNo = $srow['SERIAL_NO'];
						
						$total_surveys++;
						if($esurvey->surveyStatus === 'completed') { $completed_surveys++; }
						
						array_push($orgDetails->Survey, $esurvey);
					}
				}
				if($completed_surveys > 0) {
					$statusPercentage = (($completed_surveys / $total_surveys) * 100);
					$orgDetails->surveyStatus = $statusPercentage .'%';
				}
				
				if($survey['ORG_LOGO'] !== '') {
					$path = 'org_logos/'.$survey['ORG_LOGO'];
					$type = pathinfo($path, PATHINFO_EXTENSION);
					$data = file_get_contents($path);
					$orgDetails->imageToBase64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
				}
				
				$code = 200;
				$message = "successful";
			}
			else {
				$code = 401;
				$message = 'Survey not found';
			}
		}
		else {
			$code = 401;
			$message = 'Survey link empty';
		}
	}
	/*
	{
	  "message": "successful",
	  "code": 200,
	  "orgDetails": {
		"orgName": "tapon",
		"orgCode": "1",
		"Survey": [
		  {
			"surveyLink": "https://survey.nfactorialanalytics.com/app/survey/survey-form.html?no=VGVzdA==",
			"surveyStatus": "new",
			"serialNo": "11"
		  },
		  {
			"surveyLink": "https://survey.cttools.com/survey.html?id=t5LboPDruxvLGXrYSHgTZQ68LwxsLl5I",
			"surveyStatus": "new",
			"serialNo": "12"
		  }
		],
		"surveyStatus": "0%",
		"imageToBase64": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQBhMPEA8VFhMQGBYXFhIVEhYWFhIVGBEXFhUWFRUkHDQgGR8mHRUTIjItJSktLi8uGh8zODMtNygtLisBCgoKDg0OGBAQGi0fHyUvLy0tKzEtLS0wKy0tLTUtNy0tLS8tLS03LSs2LS83LTMtLS0tNTUtLS0tLS0tKy0tLf/AABEIAMUA/wMBIgACEQEDEQH/xAAbAAEAAwADAQAAAAAAAAAAAAAABAUGAQMHAv/EAEMQAAIBAgQDBQUDCQUJAAAAAAABAgMRBAUSIQYxQQcTIlFhMnGBkcEUNnMWJkJScoKhsbIzNZKzwhUjJDREU2JjtP/EABgBAQEBAQEAAAAAAAAAAAAAAAABAgME/8QAJxEBAQACAQMCBQUAAAAAAAAAAAECETEDEiEEQRMyUYGhBRQzcZH/2gAMAwEAAhEDEQA/APcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4OT5nNKLbaSW7b5JeoH0CifF2X/AGjR9spXvb21b/Fy/iXVOpGUFKLTT5NO6a9GTcauOU5j7ABWQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMHxVmo03J8krv3LcD6ueQ9rvEFSWZLBQk1TglKaTtrk+SfokbDhDPpY3N8RJyeiKiqcOijqd5Pzb2+Zi+1fJ5Q4ihinF9zW0RlLpFxaTT8ro5daXt8PZ+n9nxd5I/CPZzUxeEjiK9R0qU1eEUk5zXR77RR6Vwvw0svhKFPEVJ0pbqnOz0S84tJWucZ/ms8PgadLC0XUq1FaEYxbUIpe018jz7M8bm1Op3lapiIX9ZRh8l4TfT6Ujn6j1nU6lu+Po9iTOKkW4NJ2b6rp6mD4J4xqVcWsNiXqlL2KlrNu3syN8bs080u2D43zbF4F0u6xDl3mq+uENrW5WS8zRYLD4ipl8Kn2ualOEZbwptXcb8rcviZPtc54f9/6G5yb+6KP4cP6UW8RJyyuacTYzL8dGGLpwq0p+zVppwbXXbldeRrsFioVsJGrTd4zSafozF9qmIhLB0MPHeq6mpRW8tKhKPL1cl8jRcKYOWG4coUqrSlFNtN8nKblb4arfAWeCco3DnEVTFZlWpTw7pqjyk77+K1ntz6mkR0QrQdXSpRcrXsmr25XPt1Yp2bS97Rlp2FPxK6lPK61enVlF04NqK06bq7u9iy+1Q0OWuNouzd1ZPyb+JVcS4iFThfEyhJSXdz3i0+hYlROz/H1cRkbqVpuU3OW7ty2skaYx/Ze/wA23+JL6GrrYiEI3nJRXm2kLyTh2gjUMfSnK0KkZPyUlckXIrkEXE5hSpW7ypGN+SckmKeY0ZJaasXqdlaS3dm7L12YXVSgfOoi1czoRqaZVoJ+WpXCaTDrr1FGk5SdlFNt+SSuzmFRSjdNNPqndFLxnXUeGMT4kpd1Oyur7qxLfDWGPdlIytHPq2KyDE5hGFSahPTQoU5yjpgrLXK28nvd+41vCGKr1eHKFTERaqyj4k1Z82otrzaSfxM72SVIR4Ts5JPvJ7NpdTW5lm1DDYWVStVjGMd3dq/uS5tmMON7ejrzWV6eM904j1MdSjV0Oav5eT6J+RT8IZysTkca85q9SdZpSauo9/NQVvdYr8Xldd4yVotqSaW/hqanU8cpX2sqkef6mxru8eHH4erZl7NgmcnXQi1SSbu0kr+ex2GnMOrF09eFnD9aLXzVjtDA8PyvH1svzfUl4qbcZwe2pX3T+nwN++MsuxGBca6dpLxU5w1X9PJlpn3CuGxk9c04z5d5B2b9/RlXgez7CU6ylOU6lv0ZNKPxSW5u2XliTKXwlTzKX5JTxGDg4dzG9JVE33kKfRq90mk0t/I+uFOIKOaZO3KCUl4atJ7pXXTzTRX9o3EFLCZBPDwa72tHRCmttMXs5NdElyMr2KqX+1MRb2e7jfyvq2+p57nrOYx78PT79Pl1MufZ2PJ3Q49p0Kd7KpGUfSHtc/dc9aRX4fKacc2qYp+KpUSim/0IpWtH6ssTtbt4pNPOe1znh/3/APSajLsopzyaledZXpx9nEVVbwrktVv4GX7XP+n/AH/9JuMl/uij+HD+lFvyxJzWHzqlVynGQrwUa1Ko9LlVgnVi7Xt3qV+Sdr+RssrxlHHZZCsoqUZfoyV9Mls0/Uzfapi4LJYUW1rnNSS8oxjK7+bS+JY9nmDnS4ZjrTTqSlNJ81F2S+dr/EXjZOVD2cr84sYvK6XolVdjScbUIfk7XqaVrUNpW3W66mc7OfvHjffL/OZp+NvuriP2PqhfmJwoOzjCd7k2qpvGEpqC6anbVNrz5JeR3y4dWB4Xxy71z72Mpbq1rJ/x3JPZn911+3P6FrxZ92sT+HL+Qt8mvCm7MPu2/wASf0O7BUcX+WladWmnh3G1ObtaNrWUPJtuVzo7MXbhlv8A9k/oReF8ynmWcV5VpNUqNtFGLaj4pSV523k7IXmkTu0WyyDvIu1SE4OMk7SjvZ2ZecP4mVXI6NSbvKcItvzdtzOdoWBoU+GZOFKEZaoWailLn58y84U+7WHt/wBuP8ieyzlmuHuLsHHFYhYuoqeIVaom6iavFTagovyStsWU6mAxub0KuHxNJ1qE9dozWqcdEotOP6XP4EWHDuXZtQWMlRtOd1PRUaepNpqVub2KDingijgYUsXgpzjUhVpJQctWpymktL53OFuX2e+TpW+LZl+FtxnxFN8R4fLKMnFVZQ76cXaWmT2hF9LpPf1NtSwlOFPRGEVFdEkeZcXZdKl2kYXEy/s686S1dFOPhcb/ACZ6oawu7duXXxxxww7fp+XnuZZ08u4+hRX/AC+LjBzh0hUk3HXHy3Ubr3l9x1gKMuHMTVlSi5xpStNpalblZmR45y6WK7RsNRh0hCUn+pBVJNyfy+ZtuNfulivwpfyJN3u26ZSS9Kzm8/6zPZXldCrwvrqUYSk6k/FKKb2dkSu1LLqK4TrVlRh3l6Xj0rV/bQjz58tjnshl+aS9Kk/6iT2q/civ+1R/+imSfx/Zblf3f3RezbKsPU4NoTqUISk3UvJxTbtWn1NvGCSsunQynZc7cDUG+jq/58zWJm8Pljz+ptvVy/uiRyAbcQAAcMyHGXDGKxE++wWMqUqjVpU+9nGnP129l+psASzbeGdwu48Qpdm2Z1cX/vdCu/FUnV139drt/E9S4T4cp5fl3dQ8UpO86jW85fRIvQYx6eOPDt1vV9Tqzty4cHE43g0na/VdD6B0eZns44Up4vT39apLRe3srnz5ImU8pnHDxpxxVVRiklZU72Sst9Jagu00z1HhDDLHd/V11qnnVnqXptyLnEUJSglGpKFusUvluiQCbXTPZZwrDDYmdSjWqKVW+p+F33v5eZPzXKvtGHlTnVmoTVnFad/ja5ZAu00psnyFYTDqnSrT0KWrS9Lvd7q9rknNMt+0UZU5VZxhOOmUYqO667tXLAE2aU2S5BHCUVTpVZ6E9Wl6Xdvnva5HhwrTp5lLEYerOjOd9SjpcZXd34WvPfY0ILumlPiuH6dfDyhiJSq6lbVKy0fsJK0X68z6yXJVhqMYKrUmqacYKTVopvokt/iWwJs0oMBwzTw0f+FqTpt+1vqjUf60oPa/usS4ZVqxEaleo6sqbvBNKMYS5alFc36stATUbudqHmOX0sRhXSrQUovo+jXJp9GvM6KeBrxjZYqTX/lCMpf4upZgaTuvCvwGV06NadRJyqVba6st5StyV+iXRLY685yn7VQnSlVnGnUjplGKjunz3auWgGlmVl2z+RcMRwWH7uhXqKGrU4vS7t897XLTNMup4nAToVY3hUVmv4q3uJgGprRc8re63yzOVcJrD5f9lWJqyw92+6airqUm5QckruLbd11uaSKsfQEmjLK5XdAAVkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/9k="
	  }
	}
	*/
	http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'orgDetails' => $orgDetails));
}

function actionUserSurveyUpdate() {
	
    $message = ''; $tempData = '';  $code = 404; $params = ''; global $dbh; global $baseurl; $orgDetails = new stdClass();
    $post_vars = file_get_contents("php://input");
    $params = json_decode($post_vars);
	if($params->orgCode !== '' && $params->surveyStatus !== '' && $params->surveyLink !== '' && $params->serialNo !== '') {
		$res = $dbh->query("update organization_emp_vendor set SURVEY_STATUS = '".$params->surveyStatus."' where ORG_ID='".$params->orgCode."' and SURVEY_LINK = '".$params->surveyLink."' and SERIAL_NO = '".$params->serialNo."' ");
		$code = 200;
		$message = 'successfully updated survey status';
	}
	else {
		$message = 'One of the survey details is missing';
	}
	http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'orgDetails' => $params));
    
}
function actionGetProjectData($params) {
		$message = '';   $code = 404;  global $dbh; $project = new stdClass(); $result = array();

		if($params !== null && $params !== '' && $params[0] !== null && $params[0] !== '' ) {
			$res = $dbh->query("SELECT * FROM projects where register_link = '" .$params[0]."'");

			if($res && mysqli_num_rows($res) > 0) { 
					while($row = $res->fetch_assoc()) {
						$project = (object) $row; 
					}
					$code = 200;
					$message = 'Success';
			}
			else {
				$code = 403; $message = 'This survey link is not valid or has expired. Please contact the support team.';
			}
		}
		else {
			$code = 403; $message = 'This survey link is not valid.';
		}
    http_response_code($code);
    echo json_encode(array('code' => $code, 'message' => $message, 'project' => $project));
    
}



function actionRegisterUser($params) {
		$message = '';   $code = 404;  global $dbh; $user = new stdClass(); $result = array();
		$postdata = file_get_contents("php://input");
		$user = json_decode($postdata);
		$res = $dbh->query("select * from project_employees where company_email = '".$user->company_email."' && project_id = ".$user->project_id);
		if($res && mysqli_num_rows($res) > 0) {
				while($row = mysqli_fetch_assoc($res)) {
					$user = (object) $row;
				}

					if($user->survey_link === '') {
						if(setupEmpSurvey($user) === true) {
							$code = 200;
							$message = 'Success';
						}
						else {

						}
					}
					else {
						$code = 200; $message = 'already registered';
					}
					
		}
		else {

			$urs = $dbh->query("insert into project_employees (project_id, first_name, last_name, company_email, linkedin_profile, org_tenure, generation, culture_definition, created_on) values (".$user->project_id.",'".$user->first_name."', '".$user->last_name."', '".$user->company_email."', '".$user->linkedin_profile."', '".$user->org_tenure."', '".$user->generation."', '".$user->culture_definition."', now())");
			if($urs) {
					$user->id = mysqli_insert_id($dbh);
					if(setupEmpSurvey($user) === true) {
						$code = 200;
						$message = 'Success';
					}
					else {

					}
			}					
			else {
				$code = 403;
				$message = 'Failed to register, try again later.';
			}
		}


    http_response_code($code);
    echo json_encode(array('code' => $code, 'message' => $message, 'user' => $user));
}

function adddlregistration($params) {
   $message = '';   $code = 404;  global $dbh; $user = new stdClass(); $result = array();
       if($params !== null && $params !== '' ) {
         $postdata = file_get_contents("php://input");
            $usr = json_decode($postdata);
            date_default_timezone_set('Asia/Kolkata');
            $dt = date('d-m-Y H:i');
          // print_r($usr);die;
           $res = $dbh->query("select * from project_employees where company_email = '".$usr->userEmail."' and project_id = ".$usr->projectId);
			if($res && mysqli_num_rows($res) > 0) {
					while($row = mysqli_fetch_assoc($res)) {
						$user = (object) $row;
					}
						if($user->survey_link === '') {
							if(setupEmpSurvey($user) === true) {
								$code = 200;
								$message = 'Success';
							}
							else {

							}
						}
						else {
							$code = 200; $message = 'already registered';
						}
						
			} else {

				if(isset($usr->userContactNo)){
					$ucontact = $usr->userContactNo;
				}else{
					$ucontact = '';
				}
				// echo $usr->userTypeOfParticipant;
				// if($usr->userTypeOfParticipant === 'investor_firm' || $usr->userTypeOfParticipant === 'individual_investor' || $usr->userTypeOfParticipant ==='investment_bank'){
				// 	$profileType = 'MnAgenome';
				// }else{
					if($usr->userCurAccessDeal === 'Post deal integration'){
						$profileType = 'Ingenome';
					}
					else if($usr->userCurAccessDeal === 'Culture transformation'){
						$profileType = 'Culturegenome';
					}
					else if($usr->userCurAccessDeal === 'Scouting for investment opportunities' || $usr->userCurAccessDeal ==='Term sheet'|| $usr->userCurAccessDeal ==='Due diligence'|| $usr->userCurAccessDeal ==='Investment and portfolio management') {
						$profileType = 'MnAgenome';
					}
				// }
				// print_r($usr);die;
            	$urs = $dbh->query("insert into project_employees (project_id, first_name, last_name, company_email, type_of_participant, mobile, cur_assessing_deal,company_name, profile_type, created_on, describes_you) values (".$usr->projectId.",'".$usr->userFirstName."', '".$usr->userLastName."', '".$usr->userEmail."', '".$usr->userTypeOfParticipant."','".$ucontact."','".$usr->userCurAccessDeal."','".$usr->userCompanyName."','".$profileType."', now(), '".$usr->userDescribeYou."')");
					if($urs) {
							$user->id = mysqli_insert_id($dbh);
							$tst = "select * from project_employees where id = '".$user->id."' and project_id = ".$usr->projectId;
							// echo $tst;die;
							$respe = $dbh->query("select * from project_employees where id = '".$user->id."' and project_id = ".$usr->projectId);
							if($respe && mysqli_num_rows($respe) > 0) {
								while($rowpe = mysqli_fetch_assoc($respe)) {
									$user = (object) $rowpe;
								}
								// print_r($rowpe);die;
								if(setupEmpSurvey($user) === true) {
									$code = 200;
									$message = 'Success';
								}
								else {

								}
							}
						
					}					
					else {
						$code = 403;
						$message = 'Failed to register, try again later.';
					}
            }
        http_response_code($code);
        echo json_encode(array('code' => $code, 'message' => $message, 'user' => $user));
    }
}

function setupEmpSurvey(& $user) {
		global $dbh; $ret = false; $psrs = array();
		// generate survey link
		$user->survey_link = hash('adler32', '{"projectId": '.$user->project_id.', "userId": '.$user->id.', "userEmail": "'.$user->company_email.'"}');
		$user->survey_status = 'active';
		$user->survey_link = urlencode($user->survey_link);
		
		$srs = $dbh->query("update project_employees set survey_link='".$user->survey_link."', survey_status= '".$user->survey_status."' where id = ".$user->id);
		if($srs) { $ret = true; } else { $ret = false; }

		// setup emp surveys
		$cres = $dbh->query("delete from project_employee_surveys where project_id = ".$user->project_id . " and employee_id = ".$user->id);

		$projqry = $dbh->query("select id,project_type from projects where id = " . $user->project_id);

		if($projqry && mysqli_num_rows($projqry)>0) {
			$projrow = mysqli_fetch_assoc($projqry);
			if($projrow['project_type'] === 'datalake'){
				$psrs = $dbh->query("select * from project_surveys where project_id = " . $user->project_id." and survey_role = '".$user->profile_type."' OR survey_role = 'ca'");
			} else {
				$psrs = $dbh->query("select * from project_surveys where project_id = " . $user->project_id);
			}
		}

		if($psrs && mysqli_num_rows($psrs)>0) {
			while($prow = mysqli_fetch_assoc($psrs)) {
				$urs = $dbh->query("insert into project_emp_surveys (project_id, employee_id, survey_id, survey_status, created_on, modified_on,completed_on) values (".$user->project_id.",'".$user->id."', '".$prow['id']."', 'open', now(),  now(), now())");
				if($urs) {
					$ret = true;
				}
				else {
					echo 'error ' . mysqli_error($dbh); die;
				}
			}
		}


		return $ret;
}
 

function actionGetSurvey($params) {
	$message = '';   $code = 404;  $survey = new stdClass(); global $dbh; $user = new stdClass(); $result = array();
	

	if($params !== null && $params !== '' && $params[0] !== null && $params[0] !== '' ) {
			$surveyCode = $params[0];
			$ers = $dbh->query("Select pe.*, p.project_title, p.status as project_status, p.project_logo, p.project_type, p.survey_welcome_text, p.survey_instructions,p.user_source from project_employees pe JOIN projects p on pe.project_id = p.id where pe.survey_link = '".$surveyCode."' and p.status = 'active'");
			if($ers && mysqli_num_rows($ers) > 0) {
				 
				while($row = mysqli_fetch_assoc($ers)) {
					$uproj = (object) $row;
				}
				if(isset($uproj->project_type) && $uproj->project_type === 'datalake'){
					$srs = $dbh->query("select ps.id, ps.survey_name from project_surveys ps where ps.id in (select es.survey_id from project_emp_surveys es where es.employee_id = ".$uproj->id." and es.project_id = ".$uproj->project_id." ) AND ps.survey_role = '".$uproj->profile_type."'");
				}else{
					$srs = $dbh->query("select ps.id, ps.survey_name from project_surveys ps where ps.id in (select es.survey_id from project_emp_surveys es where es.employee_id = ".$uproj->id." and es.project_id = ".$uproj->project_id." )");
				}
				if($srs && mysqli_num_rows($srs) > 0) {
					
					$survey->userId = $uproj->id;
					$survey->projectId = $uproj->project_id;
					$survey->projectTitle = $uproj->project_title;
					$survey->projectLogo =  $uproj->project_logo;
					$survey->projectType = $uproj->project_type;
					$survey->user_source = $uproj->user_source;
					$survey->comSurveyStatus = $uproj->communication_survey_status;
					$survey->welcomeNote = $uproj->survey_welcome_text;
					$survey->instructions = $uproj->survey_instructions;
					$survey->survey_status =  $uproj->survey_status;
					$survey->onaEmployees = array();
					$survey->questions = array();

					while($srow = mysqli_fetch_assoc($srs)) {
						$qrs = $dbh->query("select * from project_survey_questions where survey_id = ". $srow['id']);
						if($qrs && mysqli_num_rows($qrs)>0) {
							while($qrow = mysqli_fetch_assoc($qrs)) {
									$question = new stdClass();
									$question->id = $qrow['id'];
									$question->surveyId = $srow['id'];
									$question->surveyTitle = $srow['survey_name'];
									$question->questionText = $qrow['question_text'];
									$question->questionIcon = $qrow['question_icon'];
									$question->questionType = $qrow['question_type'];
									$question->questionInstruction = $qrow['question_instruction'];
									$question->optionsInstruction = $qrow['options_instruction'];
									$question->dependent_on_ques_id = $qrow['dependent_on_ques_id'];
									$question->dependent_ques_config = $qrow['dependent_ques_config'];
									
									$question->options = array();
									//echo "select * from project_survey_options where question_id = ". $qrow['id'] . " and survey_id = ". $srow['id'];
									$ors = $dbh->query("select * from project_survey_options where question_id = ". $qrow['id'] . " and survey_id = ". $srow['id']);
									if($ors && mysqli_num_rows($ors)>0) {
										while($orow = mysqli_fetch_assoc($ors)) {
												$option = new stdClass();
												$option->id = $orow['id'];
												$option->label = $orow['option_label'];
												$option->value = $orow['option_value'];

												array_push($question->options, $option);
											
										}
									}

									if($question->questionType === 'eum') {
										$question->optBuckets = array();
										$obrs = $dbh->query("select * from project_survey_opt_categories where question_id = ". $qrow['id']);
										if($obrs && mysqli_num_rows($obrs)>0) {
											while($obrow = mysqli_fetch_assoc($obrs)) {
													$optionBucket = new stdClass();
													$optionBucket->id = $obrow['id'];
													$optionBucket->label = $obrow['opt_category_label'];
													$optionBucket->value = $obrow['opt_category_value'];
													$optionBucket->icon = $obrow['opt_category_icon'];

													array_push($question->optBuckets, $optionBucket);
												
											}
										}
									}
									else if($question->questionType === 'ona') {
										if(count($survey->onaEmployees) === 0) {
											$oers = $dbh->query("select pe.*,md.designation_name as desName from project_employees pe left join master_designations md on md.id = pe.designation where pe.project_id = ".$survey->projectId);
											if($oers && mysqli_num_rows($oers)>0) {
												while($oerow = mysqli_fetch_assoc($oers)) {
													if($oerow['id'] !== $survey->userId) {
														$onaEmp = new stdClass();
														$onaEmp->id = $oerow['id'];
														$onaEmp->empId = $oerow['emp_code'];
														$onaEmp->empDesignation = $oerow['desName'];
														$onaEmp->empBranchName = $oerow['branch_name'];
														$onaEmp->empName = $oerow['first_name']. ' '.$oerow['last_name'];
														$onaEmp->email = $oerow['company_email'];
														$onaEmp->searchEmpString = $onaEmp->empName.', '.$onaEmp->empDesignation.', '.$onaEmp->empBranchName;
	
														array_push($survey->onaEmployees, $onaEmp);
													}
												}
											}

										}
									}

									array_push($survey->questions, $question);
								
							}
						}
					}


					$code = 200;
					$message = 'Success';
				}
				else {
					$code = 403;
					$message = 'Assessment code not found 1';
				}

			}
			else  {
				$code = 403;
				$message = 'Assessment code not found 2';
			}

	}
	else {
		$code = 403;
		$message = 'Assessment code not valid 2 3';
	}


	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message, 'survey' => $survey));
}

function actionGetCaSurvey($params) {
	$message = '';   $code = 404;  $survey = new stdClass(); global $dbh; $user = new stdClass(); $result = array();
	

	if($params !== null && $params !== '' && $params[0] !== null && $params[0] !== '' ) {
			$surveyCode = $params[0];
			$ers = $dbh->query("Select pe.*, p.project_title, p.status as project_status, p.project_logo, p.project_type, p.survey_welcome_text, p.survey_instructions,p.user_source from project_employees pe JOIN projects p on pe.project_id = p.id where pe.survey_link = '".$surveyCode."' and p.status = 'active'");
			if($ers && mysqli_num_rows($ers) > 0) {
				 
				while($row = mysqli_fetch_assoc($ers)) {
					$uproj = (object) $row;
				}
				if(isset($uproj->project_type) && $uproj->project_type === 'datalake'){
					$srs = $dbh->query("select ps.id, ps.survey_name from project_surveys ps where ps.id in (select es.survey_id from project_emp_surveys es where es.employee_id = ".$uproj->id." and es.project_id = ".$uproj->project_id." ) AND ps.survey_role = 'ca'");
				}
				if($srs && mysqli_num_rows($srs) > 0) {
					
					$survey->userId = $uproj->id;
					$survey->userName = $uproj->first_name.' '.$uproj->last_name;
					$survey->userEmail = $uproj->company_email;
					$survey->projectId = $uproj->project_id;
					$survey->user_source = $uproj->user_source;
					$survey->projectTitle = $uproj->project_title;
					$survey->projectLogo =  $uproj->project_logo;
					$survey->projectType = $uproj->project_type;
					$survey->welcomeNote = $uproj->survey_welcome_text;
					$survey->comSurveyStatus = $uproj->communication_survey_status;
					$survey->instructions = $uproj->survey_instructions;
					$survey->survey_status =  $uproj->survey_status;
					$survey->onaEmployees = array();
					$survey->questions = array();

					while($srow = mysqli_fetch_assoc($srs)) {
						$qrs = $dbh->query("select id,question_text,question_icon,question_type from project_survey_questions where survey_id = '". $srow['id']."' ORDER BY RAND()");
						if($qrs && mysqli_num_rows($qrs)>0) {
							while($qrow = mysqli_fetch_assoc($qrs)) {
									$question = new stdClass();
									$question->id = $qrow['id'];
									$question->surveyId = $srow['id'];
									$question->surveyTitle = $srow['survey_name'];
									$question->questionText = $qrow['question_text'];
									$question->questionIcon = $qrow['question_icon'];
									$question->questionType = $qrow['question_type'];
									
									$question->options = array();
									//echo "select * from project_survey_options where question_id = ". $qrow['id'] . " and survey_id = ". $srow['id'];
									$ors = $dbh->query("select * from project_survey_options where question_id = ". $qrow['id'] . " and survey_id = ". $srow['id']);
									if($ors && mysqli_num_rows($ors)>0) {
										while($orow = mysqli_fetch_assoc($ors)) {
												$option = new stdClass();
												$option->id = $orow['id'];
												$option->label = $orow['option_label'];
												$option->value = $orow['option_value'];

												array_push($question->options, $option);
											
										}
									}

									if($question->questionType === 'eum') {
										$question->optBuckets = array();
										$obrs = $dbh->query("select * from project_survey_opt_categories where question_id = ". $qrow['id']);
										if($obrs && mysqli_num_rows($obrs)>0) {
											while($obrow = mysqli_fetch_assoc($obrs)) {
													$optionBucket = new stdClass();
													$optionBucket->id = $obrow['id'];
													$optionBucket->label = $obrow['opt_category_label'];
													$optionBucket->value = $obrow['opt_category_value'];
													$optionBucket->icon = $obrow['opt_category_icon'];

													array_push($question->optBuckets, $optionBucket);
												
											}
										}
									}
									else if($question->questionType === 'ona') {
										if(count($survey->onaEmployees) === 0) {
											$oers = $dbh->query("select pe.*,md.designation_name as desName from project_employees pe left join master_designations md on md.id = pe.designation where pe.project_id = ".$survey->projectId);
											if($oers && mysqli_num_rows($oers)>0) {
												while($oerow = mysqli_fetch_assoc($oers)) {
													if($oerow['id'] !== $survey->userId) {
														$onaEmp = new stdClass();
														$onaEmp->id = $oerow['id'];
														$onaEmp->empId = $oerow['emp_code'];
														$onaEmp->empDesignation = $oerow['desName'];
														$onaEmp->empBranchName = $oerow['branch_name'];
														$onaEmp->empName = $oerow['first_name']. ' '.$oerow['last_name'];
														$onaEmp->email = $oerow['company_email'];
														$onaEmp->searchEmpString = $onaEmp->empName.', '.$onaEmp->empDesignation.', '.$onaEmp->empBranchName;
	
														array_push($survey->onaEmployees, $onaEmp);
													}
												}
											}

										}
									}

									array_push($survey->questions, $question);
								
							}
						}
					}


					$code = 200;
					$message = 'Success';
				}
				else {
					$code = 403;
					$message = 'Assessment code not found 1';
				}

			}
			else  {
				$code = 403;
				$message = 'Assessment code not found 2';
			}

	}
	else {
		$code = 403;
		$message = 'Assessment code not valid 2 3';
	}

// var_dump($survey);die;
	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message, 'survey' => $survey));

}

function actionSaveSurvey($params) {
	$message = '';   $code = 404;  global $dbh; $survey = new stdClass();
	$postdata = file_get_contents("php://input");
	$surveyData = json_decode($postdata);
	$drs = $dbh->query("delete from project_emp_survey_answers where project_id = ".$surveyData->projectId." and employee_id = ".$surveyData->userId);
	foreach($surveyData->userAnswers as $q => $qobj) {
		$aorder = 1;	
		foreach($qobj->answers as $ans) {
			if($qobj->questionType === 'ona') {
				$ans->value = $ans->id;
			}
			if($qobj->questionType === 'pri_order') {
				$ans_order = $ans->order;
			}else{
				$ans_order = ($qobj->questionType === 'eum') ? $ans->option_order : $aorder;
			}
			if($qobj->questionType === 'st' || $qobj->questionType === 'tt') {
				$rs = $dbh->query("insert into project_emp_survey_answers (project_id, survey_id, employee_id, question_id,  answer, answer_order) values('".$surveyData->projectId."', '".$qobj->surveyId."', '".$surveyData->userId."', '".$qobj->questionId."', '".$ans."', '".$ans_order."')");
			} else if($qobj->questionType != 'mcs'  || $qobj->questionType != 'mcm' || $qobj->questionType != 'mcm2'){
				$rs = $dbh->query("insert into project_emp_survey_answers (project_id, survey_id, employee_id, question_id, answer, answer_order) values(".$surveyData->projectId.", ".$qobj->surveyId.", ".$surveyData->userId.", ".$qobj->questionId.", '".$ans->value."', ".$ans_order.")");
			}else {
				if(property_exists($ans, 'option_category') === false) { $ans->option_category = ''; }
				if(property_exists($ans, 'empRating') === false) { $ans->empRating = ''; }
				$rs = $dbh->query("insert into project_emp_survey_answers (project_id, survey_id, employee_id, question_id, answer_category, answer, answer_order, answer_rating) values(".$surveyData->projectId.", ".$qobj->surveyId.", ".$surveyData->userId.", ".$qobj->questionId.", '".$ans->option_category."', '".$ans->value."', ".$ans_order.", '".$ans->empRating."')");
			}

			// $rs = $dbh->query("insert into project_emp_survey_answers (project_id, survey_id, employee_id, question_id, answer_category, answer, answer_order, answer_rating) values(".$surveyData->projectId.", ".$qobj->surveyId.", ".$surveyData->userId.", ".$qobj->questionId.", '".$ans->option_category."', '".$ans->label."', ".$ans_order.", '".$ans->empRating."')");

			if($rs) {

				$code = 200;
				$message = 'Success';
			}
			else {
				$message = 'Failed to save Survey data ' . mysqli_error($dbh);
			}

			$aorder++;
		}
	}
	
	if($code === 200) {
		if(isset($surveyData->comAssessStatus) && $surveyData->comAssessStatus === 'completed'){
			$ers = $dbh->query("update project_employees set communication_survey_status = 'complete', modified_on=now(), completed_on=now() where id = ".$surveyData->userId. " and project_id = " . $surveyData->projectId);
		}
		$ers = $dbh->query("update project_employees set survey_status = 'complete', modified_on=now(), completed_on=now() where id = ".$surveyData->userId. " and project_id = " . $surveyData->projectId);

		$eers = $dbh->query("update project_emp_surveys set survey_status = 'complete',  modified_on=now(), completed_on=now() where project_id = ".$surveyData->projectId." and employee_id = ".$surveyData->userId);
		
		if($ers) {
			$code = 200;
			$surveyData->survey_status = 'complete';
			$message = 'Success';
		}
		else {
			$message = 'Failed to save Survey data' . mysqli_error($dbh);
		}
	}
	// print_r($surveyData);die;
	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message, 'survey' => $surveyData));
}
function actionSaveCaSurvey($params) {
	$message = '';   $code = 404;  global $dbh; $survey = new stdClass();
	$postdata = file_get_contents("php://input");
	$surveyData = json_decode($postdata);
	
	if($surveyData->userAnswers !=''){
		foreach($surveyData->userAnswers as $q => $qobj) {
			$aorder = 1;	
			if(!empty($qobj->answers)){
			foreach($qobj->answers as $ans) {
				$ans_order = ($qobj->questionType === 'eum') ? $ans->option_order : $aorder;
				if(property_exists($ans, 'option_category') === false) { $ans->option_category = ''; }
				if(property_exists($ans, 'empRating') === false) { $ans->empRating = ''; }
				$rs = $dbh->query("insert into project_emp_survey_answers (project_id, survey_id, employee_id, question_id, answer_category, answer, answer_order, answer_rating) values(".$surveyData->projectId.", ".$qobj->surveyId.", ".$surveyData->userId.", ".$qobj->questionId.", '".$ans->option_category."', '".$ans->label."', ".$ans_order.", '".$ans->empRating."')");
				if($rs) {
					$code = 200;
					$message = 'Success';
				}
				else {
					$message = 'Failed to save Survey data ' . mysqli_error($dbh);
				}
				$aorder++;
			}
		}
		}
	}
	if($code === 200) {
		if(isset($surveyData->comAssessStatus) && $surveyData->comAssessStatus === 'completed'){
			$ers = $dbh->query("update project_employees set communication_survey_status = 'complete', modified_on=now(), completed_on=now() where id = ".$surveyData->userId. " and project_id = " . $surveyData->projectId);
		}
		$ers = $dbh->query("update project_employees set survey_status = 'complete', modified_on=now(), completed_on=now() where id = ".$surveyData->userId. " and project_id = " . $surveyData->projectId);

		$eers = $dbh->query("update project_emp_surveys set survey_status = 'complete',  modified_on=now(), completed_on=now() where project_id = ".$surveyData->projectId." and employee_id = ".$surveyData->userId);
		$surveyData->survey_result = communicationAssessResult($surveyData->userAnswers, $surveyData->userEmail, $surveyData->userName,$surveyData->userSource);
		if($ers) {
			// die;
			$code = 200;
			$surveyData->survey_status = 'complete';
			$surveyData->comSurveyStatus = 'complete';
			$message = 'Success';
		}
		else {
			$message = 'Failed to save Survey data' . mysqli_error($dbh);
		}
	}
	// print_r($surveyData);die;
	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message, 'survey' => $surveyData));
}
function communicationAssessResult($usrAnswer,$usrmail,$usrname,$usrsource) {
	$sec_a = 'ca_a'; $sec_b = 'ca_b'; $sec_c = 'ca_c'; $sec_d = 'ca_d';
	$secArray = [];	$descArray = []; $resultArray = [];
	$secA_yes = 0; $secA_per = 0;
	$secB_yes = 0; $secB_per = 0;
	$secC_yes = 0; $secC_per = 0;
	$secD_yes = 0; $secD_per = 0;
	// print_r($usrAnswer);
	if(!empty($usrAnswer)){
		// print_r($usrAnswer);
		
		$result_a = array_filter($usrAnswer, function ($item) use ($sec_a) {
		    if (!empty($item->questionType) && stripos($item->questionType, $sec_a) !== false) { return true; }  return false;
		});
		if(!empty($result_a)){
			foreach($result_a as $resulans){
				foreach ($resulans->answers as $h) {
				    if($h->value === 'Yes'){ 	$secA_yes++; }
				}
			}
			$secA_per = ($secA_yes/15)*100;
		}

		$result_b = array_filter($usrAnswer, function ($item) use ($sec_b) {
		    if (!empty($item->questionType) && stripos($item->questionType, $sec_b) !== false) { return true; } return false;
		});
		if(!empty($result_b)){
			foreach($result_b as $resulans){
				foreach ($resulans->answers as $h) {
				    if($h->value === 'Yes'){  	$secB_yes++; }
				}
			}
			$secB_per = ($secB_yes/15)*100;
		}

		$result_c = array_filter($usrAnswer, function ($item) use ($sec_c) {
		    if (!empty($item->questionType) && stripos($item->questionType, $sec_c) !== false) { return true; } return false;
		});
		if(!empty($result_c)){
			foreach($result_c as $resulans){
				foreach ($resulans->answers as $h) {
				    if($h->value === 'Yes'){	$secC_yes++; }
				}
			}
			$secC_per = ($secC_yes/15)*100;
		}
		$result_d = array_filter($usrAnswer, function ($item) use ($sec_d) {
		    if (!empty($item->questionType) && stripos($item->questionType, $sec_d) !== false) { return true; } return false;
		});
		if(!empty($result_d)){
			foreach($result_d as $resulans){
				foreach ($resulans->answers as $h) {
				    if($h->value === 'Yes'){  	$secD_yes++; }
				}
			} 
			$secD_per = ($secD_yes/15)*100;
		}
	}
	$secArray = array("secA"=>$secA_per, "secB"=>$secB_per, "secC"=>$secC_per, "secD"=>$secD_per);
	arsort($secArray);
	$sA_behaviors = '
		<li>Reserved<li>
		<li>Approaches work systematically<li>
		<li>Pays attention to details<li>
		<li>Focuses attention on immediate task<li>
		<li>Prefers to stick to established guidelines and practices<li>
		<li>Likes to plan for change<li>
	';
	$sA_needs = '
		<li>High standards<li>
		<li>Appreciation<li>
		<li>Quality work<li>
	';
	$sA_fears = '
		<li>Criticism of work<li>
		<li>Imperfection<li>
		<li>Not having things adequately explained</li>
	';
	$sB_behaviors = '
		<li>Reserved<li>
		<li>Works well in a team<li>
		<li>Accommodates others<li>
		<li>Maintains status quo<li>
		<li>Recovers slowly from hurt<li>
		<li>Prefers steady rather than sudden change<li>
	';
	$sB_needs = '
		<li>Security<li>
		<li>Acceptance<li>
		<li>Teamwork<li>
	';
	$sB_fears = '
		<li>Isolation<li>
		<li>Standing out as better or worse<li>
		<li>Unplanned challenges</li>
	';

	$sC_behaviors = '
		<li>Outgoing<li>
		<li>Leads by enthusing others<li>
		<li>Prefers a global approach<li>
		<li>Steers away from details<li>
		<li>Acts on impulse<li>
		<li>Keen to promote change<li>
	';
	$sC_needs = '
		<li>Change acknowledgement<li>
		<li>New trends and ideas<li>
	';
	$sC_fears = '
		<li>Disapproval<li>
		<li>Stagnation<li>
		<li>Detailed work</li>
	';


	$sD_behaviors = '
		<li>Outgoing<li>
		<li>Challenges status quo<li>
		<li>Keen to get things done<li>
		<li>Resists authority<li>
		<li>Likes to take the lead<li>
		<li>Takes action to bring about change<li>
	';
	$sD_needs = '
		<li>Results<li>
		<li>Recognition<li>
		<li>Challenges<li>
	';
	$sD_fears = '
		<li>Challenges to their authority<li>
		<li>Lack of results from others<li>
	';

	$key = array_search(max($secArray), $secArray);
	if($key === 'secA'){
		$descArray = array('section' => 'Section_A', 'main_result'=>'Introverted and Task Oriented', 'behaviors_result'=>$sA_behaviors,'needs_result'=>$sA_needs, 'fears_result'=>$sA_fears);
	} else if($key === 'secB'){
		$descArray = array('section' => 'Section_B', 'main_result'=>'Introverted and People Oriented',  'behaviors_result'=>$sB_behaviors,'needs_result'=>$sB_needs, 'fears_result'=>$sB_fears);
	} else if($key === 'secC'){
		$descArray = array('section' => 'Section_C', 'main_result'=>'Extraverted and People Oriented',  'behaviors_result'=>$sC_behaviors,'needs_result'=>$sC_needs, 'fears_result'=>$sC_fears);
	} else if($key === 'secD'){
		$descArray = array('section' => 'Section_D', 'main_result'=>'Extraverted and Task Oriented',  'behaviors_result'=>$sD_behaviors,'needs_result'=>$sD_needs, 'fears_result'=>$sD_fears);
	}
	$resultArray = array("section_per"=>$secArray, "section_details"=>$descArray, "usrmail" => $usrmail, 'usrname' => $usrname, 'usersource' => $usrsource);
	return $resultArray;
}
function actionGetAllEmployeesSurvayAnswers($params) {
	$message = '';   $code = 404;  global $dbh; $project = new stdClass(); $result = array(); $branches = array();
	$ona_results = array();

	if($params !== null && $params !== '' && $params[0] !== null && $params[0] !== '' ) {
		//$project_id = $params[0];
		$project_id = '4';

		$sql = "";

		$sql = "SELECT pesa.*, CONCAT(pe.`first_name`,' ',pe.`last_name`) AS employee_name, pe.`emp_code`, pe.`branch_name`, psq.`question_text`, psq.`question_type` FROM `project_emp_survey_answers` AS pesa INNER JOIN `project_employees` AS pe ON pe.`id` = pesa.`employee_id` INNER JOIN `project_survey_questions` AS psq ON psq.`id` = pesa.`question_id` WHERE psq.`question_type` = 'ona' AND pesa.`project_id` = '$project_id'";
		if($sql != "") {
			$rs = $dbh->query($sql);
			if($rs && mysqli_num_rows($rs) > 0) {
				$code = 200;
				$message = 'Success';
				while ($row = mysqli_fetch_assoc($rs)) {
					$sql_respondent = "SELECT CONCAT(`branch_name`, '(', `emp_code`, ')') AS network_graph_respondent FROM `project_employees` WHERE `id` = ".$row['employee_id'];
					$rs1 = $dbh->query($sql_respondent);
					$rs1 = mysqli_fetch_assoc($rs1);

					// print_r($rs1['network_graph_respondent']);
					$sql_response = "SELECT CONCAT(`branch_name`, '(', `emp_code`, ')') AS network_graph_response FROM `project_employees` WHERE `id` = ".$row['answer'];
					$rs2 = $dbh->query($sql_response);
					$rs2 = mysqli_fetch_assoc($rs2);
					$row['network_graph_respondent'] = $rs1['network_graph_respondent'];
					$row['network_graph_response'] = $rs2['network_graph_response'];
					// $row['network_graph'] = "'".$rs1['network_graph_respondent']. "','". $rs2['network_graph_response']."'";

					array_push($result, $row);
				}

				$get_all_branches = "SELECT DISTINCT `branch_name` FROM `project_employees`";
				$rs = $dbh->query($get_all_branches);
				if($rs && mysqli_num_rows($rs) > 0) { 
					while ($row = mysqli_fetch_assoc($rs)) {
						array_push($branches, $row);
					}
				}

				$get_all_onaresult = "SELECT peor.`project_id`,peor.`employee_id`,peor.`influencer_scores`,peor.`network_leadership_scores`,pe.`id`,CONCAT(pe.`first_name`,' ',pe.`last_name`) AS employee_name,pe.`branch_name`,pe.`emp_code` FROM `project_emp_survey_ona_result` AS peor INNER JOIN `project_employees` AS pe ON pe.`id` = peor.`employee_id`;";
				$onaresult = $dbh->query($get_all_onaresult);
				if($onaresult && mysqli_num_rows($onaresult) > 0) { 
					while ($onarow = mysqli_fetch_assoc($onaresult)) {
						array_push($ona_results, $onarow);
					}
				}
			}
			http_response_code($code);
			echo json_encode(array('code' => $code, 'message' => $message, 'emp_survey_answers' => $result, 'all_branches' => $branches, 'ona_result' => $ona_results));
		}
	}
}
function getJobTeamOrganizationData() {
	$message = '';   $code = 404;  global $dbh; $project = new stdClass(); $result = array();$params = '';
    $job_arr = array(); $team_arr = array(); $org_arr = array(); $user_opt_transform_value = array(); 
    $summarydata = array(); $ratingdata = array(); $rating_arr = array(); $promoter = 0; $passive = 0; $detrator = 0; $where_cond = ''; $job_count = 0; $team_count = 0; $org_count = 0; $nps_score = '';
    // echo "welcome";
    $filterdata = array(); $project_id = '';
    $post_vars = file_get_contents("php://input");
    $params = json_decode($post_vars);
    foreach($params as $param => $paramvalue) {
    	// print_r($param);die;
    	if($param === "empfilterdetails") {
    		$filterdata = $paramvalue;
    	}
    	else {
    		$project_id = $paramvalue;
    	}
    }
   	// print_r($filterdata);
	if($filterdata) {
		foreach ($filterdata as $key) {
			if($key->value !== "all") {
				if(is_int($key->value)) {
					$where_cond .= " AND $key->name = ".$key->value;
				} else {
					$where_cond .= " AND $key->name = '".$key->value."'";
				}
			}
		}
		$where_cond .= " ORDER BY pesa.`employee_id` ASC";
	}
	else {
		$where_cond = " ORDER BY pesa.`employee_id` ASC";
	}
    // echo $where_cond;
    // die;
    // $transform_value_arr = array(100,90.909,81.818,72.727,63.636,54.545,45.455,36.364,27.273,18.182,9.0909,0);

	if($params !== null && $params !== '') {
		// get answers of question 2 (all users) -> mna1;
		$get_all_users_rating = "SELECT pesa.answer FROM `project_emp_survey_answers` AS pesa INNER JOIN `project_employees` AS pe ON pe.id = pesa.employee_id  WHERE pesa.project_id = ".$project_id." AND pesa.`survey_id` = 2 AND pesa.`question_id` = 2 AND pe.user_type = 'employee' ".$where_cond;

		$ratingresult = $dbh->query($get_all_users_rating);
		if($ratingresult) {
			while ($row = mysqli_fetch_assoc($ratingresult)) {
				array_push($ratingdata, $row['answer']);
			}
			// print_r(array_count_values($ratingdata));
			// echo count($ratingdata);die;
			$get_rating_count = array_count_values($ratingdata);
			foreach ($get_rating_count as $key => $value) {
				// echo $key. "---". $value."++++++";

				if($key >=1 && $key <= 6) {
					$cal_percentage_of_detrator = ($value/count($ratingdata))*100;
					$detrator += $cal_percentage_of_detrator;
				}
				else if($key >= 7 && $key <= 8) {
					$cal_percentage_of_passive = ($value/count($ratingdata))*100;
					$passive += $cal_percentage_of_passive;
				}
				else if($key >= 9 && $key <= 10) {
					$cal_percentage_of_promoter = ($value/count($ratingdata))*100;
					$promoter += $cal_percentage_of_promoter;
				}
			}

			// echo $promoter ." --- ". $passive ." --- ". $detrator;

			// NPS Score = % of promoter - % of detrator
			$nps_score = $promoter - $detrator;
			$nps_score = number_format((float)$nps_score, 2, '.', '');
			// echo $nps_score."%";
			// die;
		}
			
		// get answers of question 3 (all users) -> mna2;
		$get_all_users_summary = "SELECT pesa.* FROM `project_emp_survey_answers` AS pesa INNER JOIN `project_employees` AS pe ON pe.id = pesa.employee_id WHERE pesa.project_id = ".$project_id." AND pesa.`survey_id` = 3 AND pesa.`question_id` = 3 AND pe.user_type = 'employee' ".$where_cond;

		$summaryresult = $dbh->query($get_all_users_summary);
		if($summaryresult && mysqli_num_rows($summaryresult) > 0) { 
			while ($summaryrow = mysqli_fetch_assoc($summaryresult)) {
				$transform_value = (((13 - $summaryrow['answer_order'])-1)/11)*100; // formula to calculate transform value
				// echo $transform_value;
				$summaryrow['transform_value'] = $transform_value;
				array_push($summarydata, $summaryrow['transform_value']);
			}
		}
	}

	// print_r(array_chunk($summarydata,12));die;
	if($summarydata) {
		$user_opt_transform_value = array_chunk($summarydata,12);

		foreach ($user_opt_transform_value as $key) {
			$job = 0; $team = 0; $org = 0;
			for($i = 0; $i < 5; $i++) {
				$job += $key[$i];
			}
			$jobavg = $job/5;
			array_push($job_arr, $jobavg);

			for($i = 5; $i < 8; $i++) {
				$team += $key[$i];
			}
			$teamavg = $team/3;
			array_push($team_arr, $teamavg);

			for($i = 8; $i < 12; $i++) {
				$org += $key[$i];
			}
			$orgavg = $org/4;
			array_push($org_arr, $orgavg);
		}

		

		for($i = 0; $i < count($job_arr); $i++) {
			if($job_arr[$i] >= 60) {
				$job_count++;
			}
		}

		for($i = 0; $i < count($team_arr); $i++) {
			if($team_arr[$i] >= 60) {
				$team_count++;
			}
		}

		for($i = 0; $i < count($org_arr); $i++) {
			if($org_arr[$i] >= 60) {
				$org_count++;
			}
		}

		$code = 200;
		$message = 'Success';
	}
	else {
		$message = 'No Data Found';
	}

	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message, 'job_count' => $job_count, 'team_count' => $team_count, 'org_count' => $org_count, "nps_score" => $nps_score));
		
}

function getAdaptabilityQuotientScore($params) {
	$message = '';   $code = 404;  global $dbh; $project = new stdClass(); $result = array(); $users_cvs_ans = array(); $x_count = 0; $y_count = 0; $z_count = 0; $users_adq_score_arr = array(); $total_adq_score = 0;
	$average_adq_score = '';

	$dg = array(); $dgms = array("20-25", "26-30", "31-40", "41 & above", "Male", "Female", "1-3 Years", "4-5 Years", "6-10 Years", "11 Years & above");
	$poscore['highly_resistive'] = 0; 
	$poscore['more_resistive'] = 0; 
	$poscore['neutral'] = 0; 
	$poscore['more_adaptive'] = 0; 
	$poscore['highly_adaptive'] = 0; 
	
	foreach($poscore as $ps) {
		$ps = 0;
	}

	foreach($dgms as $ag) {
		$dg[$ag]['adq_total'] = 0;
		$dg[$ag]['mi_total'] = 0;
		$dg[$ag]['mi_avg'] = 0;
		$dg[$ag]['empcount'] = 0;
		$dg[$ag]['adq_avg'] = 0;
	}

	// $params = '';
	$project_id = '';
	if($params !== '' && $params !== null) {

		$get_all_users_cva_ans = "SELECT pesa.*, pe.age_group, pe.duration_at_SBI, pe.gender FROM project_emp_survey_answers AS pesa INNER JOIN project_employees AS pe ON pe.id = pesa.employee_id WHERE pesa.project_id = ".$params[0]." AND pesa.survey_id = 1 AND pesa.question_id = 1 AND pe.user_type = 'employee' ";

		$get_cva_ans_result = $dbh->query($get_all_users_cva_ans);

		while ($row = mysqli_fetch_assoc($get_cva_ans_result)) {
			$employee_id = $row['employee_id'];
			$answer = $row['answer'];

			// get PV LV
			$get_pvlv = "SELECT pvlv, level FROM project_lp_values WHERE value = '".$answer."'";
			$result_pvlv = $dbh->query($get_pvlv);
			$pvlv = mysqli_fetch_assoc($result_pvlv);
			$row['pvlv'] = $pvlv['pvlv'];
			$row['level'] = $pvlv['level'];

			// get mood index
			$get_moodindex = "SELECT answer FROM project_emp_survey_answers WHERE project_id = ".$params[0]." AND survey_id = 6 AND question_id = 11 AND employee_id = ".$employee_id;
			$result_moodindex = $dbh->query($get_moodindex);
			$moodindex = mysqli_fetch_assoc($result_moodindex);
			$row['moodindex'] = $moodindex['answer'];
			array_push($result, $row);
		}

		$users_cvs_ans = array_chunk($result,10);
		// print_r($users_cvs_ans);die;

		foreach ($users_cvs_ans as $cvs_ans_key) {
			// print_r($cvs_ans_key);die;
			// print_r(count($cvs_ans_key));die;
			$ans_count = count($cvs_ans_key);
			$moodindex = '';
			$users_adq_score = array();
			for($i = 0; $i < $ans_count; $i++) {
				// echo $i . "<br>";
				if($cvs_ans_key[$i]['pvlv'] === "PV" && $cvs_ans_key[$i]['level'] >= 4) {
					$x_count++;
				}
				else if($cvs_ans_key[$i]['pvlv'] === "PV" && $cvs_ans_key[$i]['level'] <= 3) {
					$y_count++;
				}
				else {
					$z_count++;
				}

				if($i == 9) {
					$moodindex = $cvs_ans_key[$i]['moodindex'];
					// echo $x_count ."--". $y_count ."--". $z_count ."--". $moodindex; die;
					$users_adq_score['user_id'] = $cvs_ans_key[$i]['employee_id'];
					$users_adq_score['age_group'] = $cvs_ans_key[$i]['age_group'];
					$users_adq_score['gender'] = $cvs_ans_key[$i]['gender'];
					$users_adq_score['duration_at_SBI'] = $cvs_ans_key[$i]['duration_at_SBI'];
					$users_adq_score['moodindex'] = $cvs_ans_key[$i]['moodindex'];
					$users_adq_score['adq_score'] = ($x_count*2)+($y_count)-($z_count*2)+$moodindex;
					$total_adq_score += $users_adq_score['adq_score'];

					$dg[$cvs_ans_key[$i]['age_group']]['adq_total'] += $users_adq_score['adq_score'];
					$dg[$cvs_ans_key[$i]['age_group']]['mi_total'] += $cvs_ans_key[$i]['moodindex'];
					$dg[$cvs_ans_key[$i]['age_group']]['empcount']++;

					$dg[$cvs_ans_key[$i]['duration_at_SBI']]['adq_total'] += $users_adq_score['adq_score'];
					$dg[$cvs_ans_key[$i]['duration_at_SBI']]['mi_total'] += $cvs_ans_key[$i]['moodindex'];
					$dg[$cvs_ans_key[$i]['duration_at_SBI']]['empcount']++;

					$dg[$cvs_ans_key[$i]['gender']]['adq_total'] += $users_adq_score['adq_score'];
					$dg[$cvs_ans_key[$i]['gender']]['mi_total'] += $cvs_ans_key[$i]['moodindex'];
					$dg[$cvs_ans_key[$i]['gender']]['empcount']++;

					if($users_adq_score['adq_score'] >= -7 && $users_adq_score['adq_score'] <= 0){
						$poscore['highly_resistive']++;
					} else if($users_adq_score['adq_score'] >= 1 && $users_adq_score['adq_score'] <= 5){
						$poscore['more_resistive']++;
					}else if($users_adq_score['adq_score'] >= 6 && $users_adq_score['adq_score'] <= 14){
						$poscore['neutral']++;
					}else if($users_adq_score['adq_score'] >= 15 && $users_adq_score['adq_score'] <= 20){
						$poscore['more_adaptive']++;
					}else if($users_adq_score['adq_score'] >= 21 && $users_adq_score['adq_score'] <= 25){
						$poscore['highly_adaptive']++;
					}



				}


			}
			array_push($users_adq_score_arr, $users_adq_score);
			// echo $moodindex;
		}

		$average_adq_score = $total_adq_score/count($users_cvs_ans);
		// print_r($poscore);

		foreach($dgms as $ag) {
			$dg[$ag]['adq_avg'] = $dg[$ag]['adq_total'] / $dg[$ag]['empcount'];
			$dg[$ag]['mi_avg'] = $dg[$ag]['mi_total'] / $dg[$ag]['empcount'];
		}
		

		if($users_adq_score_arr) {
			$code = 200;
			$message = 'Success';
		} else {
			$message = 'No Data Found';
		}

		http_response_code($code);
		echo json_encode(array('code' => $code, 'message' => $message, 'users_adq_score' => $users_adq_score_arr, 'average_adq_score' => $average_adq_score, 'demograph' => $dg, 'po_score' =>$poscore ));
	}
}
function addContactPersonDetails() {
	$message = '';   $code = 404;  global $dbh; $user = new stdClass(); $result = array();
	$postdata = file_get_contents("php://input");
	$usr = json_decode($postdata);
	date_default_timezone_set('Asia/Kolkata');
	$dt = date('d-m-Y H:i');

	// Insert Organization Details
	$add_org_details = $dbh->query("INSERT INTO organizations (org_name, emp_count, address_1, website) VALUES ('".$usr->companyName."',".$usr->empCount.", '".$usr->companyAddress."', '".$usr->companyWebsite."')");

	if($add_org_details) {
		$org_id = mysqli_insert_id($dbh);
		$urs = $dbh->query("insert into organization_team (org_id, first_name, last_name, designation, official_email, contact_no, created_on) values (".$org_id.",'".$usr->empFirstName."', '".$usr->empLastName."', '".$usr->empDesignation."', '".$usr->empEmail."','".$usr->empContactNo."', now())");
		if($urs) {
			$code = 200;
			$message = 'Success';

		}
		else {
			$code = 403;
			$message = 'Failed to register, try again later.';
		}
	}
	else {
		$code = 403;
		$message = 'Failed to register, try again later.';
	}
	
	// print_r($usr);die;
		
	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message));
}

function sendComAssSurveyMail($params){
	$message = '';   $code = 404;  global $dbh; $project = new stdClass(); $result = array(); global $baseurl;
	 $post_vars = file_get_contents("php://input");
    $params = json_decode($post_vars);
    if($params !== null && $params !== '' ) {
		$result = $params;
		// print_r($result);

		$mysecArray = json_decode(json_encode($result->section_per), true); 
		$clr = '#E74C3C';
		$sA = (array_search('secA', array_keys($mysecArray))+1);
		$sB = (array_search('secB', array_keys($mysecArray))+1);
		$sC = (array_search('secC', array_keys($mysecArray))+1);
		$sD = (array_search('secD', array_keys($mysecArray))+1);
		$email = $result->usrmail;
		$subject = 'Communication Assessment Results';
		$checkclr = array("red","#70AD47","#A9D18E","#F8CBAD","#C55A11");
		// echo $checkclr[$sA];
		if($result->usersource === 'VI'){
			$type_title = "Venture Intelligence";
			$type_logo ="vi.jpg";
			$profile_logo ="mna-login-banner.jpg";
		}
		if($result->usersource === 'TIE'){
			$type_title = "The IndUS Entrepreneurs survey";
			$type_logo ="tie.png";
			$profile_logo ="culturelytics_logo.jpg";
		}
		if($result->usersource === 'NHRD'){
			$type_title = "National HRD Network survey";
			$type_logo ="nhrd.png";
			$profile_logo ="culturelytics_logo.jpg";
		}

		$to = $result->usrmail;
		
		$appurl = $baseurl."/dlsurveyresult/". $result->survey_code;
		$body ="Hi ". $result->usrname . ", <br> To get your TIPE assessment result: <a href='".$appurl."'>Click Here</a>";
		global $smtpun; global $smtpps;
	  	$mail = new PHPMailer();   
		$mail->IsSMTP();
		$mail->SMTPDebug = 0;
		$mail->SMTPAuth = TRUE;
		$mail->SMTPSecure = "ssl";
		$mail->Port     = 465;  
		$mail->Username = $smtpun;
		$mail->Password = $smtpps;
		$mail->Host     = "smtp.gmail.com";
		$mail->Mailer   = "smtp";
		$mail->SetFrom($smtpun, "e2epeoplepractices");
		// $mail->AddReplyTo($emp->email, "e2epeoplepractices");
		// $mail->AddAttachment($file_name);  
		$mail->AddAddress($to, "e2epeoplepractices");
		$mail->Subject = $subject;
		$mail->WordWrap   = 80;
		$mail->MsgHTML($body);
		$mail->IsHTML(true);
		if($mail->Send()){
			// unlink($file_name);
			$code = 200;
			$message = 'Success';
		}else {
			$code = 403;
			$message = 'Failed to register, try again later.';
		}
	}
	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message));

}

function getComAssSurveyResult() {
	$message = '';   $code = 404;  global $dbh; $project = new stdClass(); $result = array(); 
	$post_vars = file_get_contents("php://input");
	$params = json_decode($post_vars);
	if($params !== null && $params !== '' ) {
		$survey_code = $params->survey_code;

    	// Get employee details
		$sql = "SELECT p.user_source, pe.id, pe.first_name, pe.last_name, pe.company_email, pe.completed_on FROM project_employees AS pe INNER JOIN projects AS p ON p.id = pe.project_id WHERE survey_link = '".$survey_code."'";

		$result = $dbh->query($sql);
		$result = mysqli_fetch_assoc($result);
		$date_after_10days = date("Y-m-d", strtotime($result['completed_on']." +120 days"));
		$current_date = strtotime(date("Y-m-d"));
		$date_after_10days = strtotime($date_after_10days);
		// print_r($current_date);

		if($current_date > $date_after_10days) {
			$code = 403;
			$message = "Sorry, This link has expired";
		}
		else {
			
			// Get Communication Assessment Result
			$get_user_ans = "SELECT pesa.`employee_id`, pesa.`question_id`, pesa.`answer`, psq.`question_type` FROM `project_emp_survey_answers` AS pesa INNER JOIN `project_survey_questions` AS psq ON psq.`id` = pesa.`question_id` WHERE psq.`question_type` IN ('ca_a','ca_b','ca_c','ca_d') AND pesa.`employee_id` = ".$result['id'];
			$user_ans = $dbh->query($get_user_ans);
			$user_answers = array();
			while ($row = mysqli_fetch_assoc($user_ans)) {
				array_push($user_answers, $row);
			}
			// print_r($user_answers);die;

			$sec_a = 'ca_a'; $sec_b = 'ca_b'; $sec_c = 'ca_c'; $sec_d = 'ca_d';
			$secArray = [];	$descArray = []; $resultArray = [];
			$secA_yes = 0; $secA_per = 0;
			$secB_yes = 0; $secB_per = 0;
			$secC_yes = 0; $secC_per = 0;
			$secD_yes = 0; $secD_per = 0;
			$result_a = array_filter($user_answers, function ($item) use ($sec_a) {
				if (stripos($item['question_type'], $sec_a) !== false) { return true; }  return false;
			});
			$result_b = array_filter($user_answers, function ($item) use ($sec_b) {
				if (stripos($item['question_type'], $sec_b) !== false) { return true; } return false;
			});
			$result_c = array_filter($user_answers, function ($item) use ($sec_c) {
				if (stripos($item['question_type'], $sec_c) !== false) { return true; } return false;
			});
			$result_d = array_filter($user_answers, function ($item) use ($sec_d) {
				if (stripos($item['question_type'], $sec_d) !== false) { return true; } return false;
			});
			foreach($result_a as $resulans){
				if($resulans['answer'] === 'Yes'){ 	$secA_yes++; }
			}
			foreach($result_b as $resulans){
				if($resulans['answer'] === 'Yes'){  $secB_yes++; }
			}
			foreach($result_c as $resulans){
				if($resulans['answer'] === 'Yes'){	$secC_yes++; }
			}
			foreach($result_d as $resulans){
				if($resulans['answer'] === 'Yes'){  $secD_yes++; }
			}
			// print_r($secA_yes);die;

			// get total counts of Sect A options, Sect B options, Sect C options and Sect D options
			$get_count_sectA = $dbh->query("SELECT COUNT(pso.`option_value`) AS opt_count FROM project_survey_options AS pso WHERE pso.`question_type` = 'ca_a'");
			$res_count_sectA = mysqli_fetch_assoc($get_count_sectA);
			$count_sectA = $res_count_sectA['opt_count'];
			$get_count_sectB = $dbh->query("SELECT COUNT(pso.`option_value`) AS opt_count FROM project_survey_options AS pso WHERE pso.`question_type` = 'ca_b'");
			$res_count_sectB = mysqli_fetch_assoc($get_count_sectB);
			$count_sectB = $res_count_sectB['opt_count'];
			$get_count_sectC = $dbh->query("SELECT COUNT(pso.`option_value`) AS opt_count FROM project_survey_options AS pso WHERE pso.`question_type` = 'ca_c'");
			$res_count_sectC = mysqli_fetch_assoc($get_count_sectC);
			$count_sectC = $res_count_sectC['opt_count'];
			$get_count_sectD = $dbh->query("SELECT COUNT(pso.`option_value`) AS opt_count FROM project_survey_options AS pso WHERE pso.`question_type` = 'ca_d'");
			$res_count_sectD = mysqli_fetch_assoc($get_count_sectD);
			$count_sectD = $res_count_sectD['opt_count'];

			if(count($result_a) != 0) {
				$secA_per = round(($secA_yes/$count_sectA)*100);
			}
			else {
				$secA_per = 0;
			}
			if(count($result_b) != 0) {
				$secB_per = round(($secB_yes/$count_sectB)*100);
			}
			else {
				$secB_per = 0;
			}
			if(count($result_c) != 0) {
				$secC_per = round(($secC_yes/$count_sectC)*100);
			}
			else {
				$secC_per = 0;
			}
			if(count($result_d) != 0) {
				$secD_per = round(($secD_yes/$count_sectD)*100);
			}
			else {
				$secD_per = 0;
			}

			$secArray = array("secA"=>$secA_per, "secB"=>$secB_per, "secC"=>$secC_per, "secD"=>$secD_per);

			arsort($secArray);


			$sA_behaviors = '
			<li>Reserved</li>
			<li>Approaches work systematically</li>
			<li>Pays attention to details</li>
			<li>Focuses attention on immediate task</li>
			<li>Prefers to stick to established guidelines and practices</li>
			<li>Likes to plan for change</li>
			';
			$sA_needs = '
			<li>High standards</li>
			<li>Appreciation</li>
			<li>Quality work</li>
			';
			$sA_fears = '
			<li>Criticism of work</li>
			<li>Imperfection</li>
			<li>Not having things adequately explained</li>
			';
			$sB_behaviors = '
			<li>Reserved</li>
			<li>Works well in a team</li>
			<li>Accommodates others</li>
			<li>Maintains status quo</li>
			<li>Recovers slowly from hurt</li>
			<li>Prefers steady rather than sudden change</li>
			';
			$sB_needs = '
			<li>Security</li>
			<li>Acceptance</li>
			<li>Teamwork</li>
			';
			$sB_fears = '
			<li>Isolation</li>
			<li>Standing out as better or worse</li>
			<li>Unplanned challenges</li>
			';


			$sC_behaviors = '
			<li>Outgoing</li>
			<li>Leads by enthusing others</li>
			<li>Prefers a global approach</li>
			<li>Steers away from details</li>
			<li>Acts on impulse</li>
			<li>Keen to promote change</li>
			';
			$sC_needs = '
			<li>Change acknowledgement</li>
			<li>New trends and ideas</li>
			';
			$sC_fears = '
			<li>Disapproval</li>
			<li>Stagnation</li>
			<li>Detailed work</li>
			';


			$sD_behaviors = '
			<li>Outgoing</li>
			<li>Challenges status quo</li>
			<li>Keen to get things done</li>
			<li>Resists authority</li>
			<li>Likes to take the lead</li>
			<li>Takes action to bring about change</li>
			';
			$sD_needs = '
			<li>Results</li>
			<li>Recognition</li>
			<li>Challenges</li>
			';
			$sD_fears = '
			<li>Challenges to their authority</li>
			<li>Lack of results from others</li>
			';




			$key = array_search(max($secArray), $secArray);
			// echo $key;
			// print_r($secArray);
			if($key === 'secA'){
				$descArray = array('section' => 'Section_A', 'main_result'=>'Task Oriented and Introverted', 'behaviors_result'=>$sA_behaviors,'needs_result'=>$sA_needs, 'fears_result'=>$sA_fears);
			} else if($key === 'secB'){
				$descArray = array('section' => 'Section_B', 'main_result'=>'People Oriented and Introverted',  'behaviors_result'=>$sB_behaviors,'needs_result'=>$sB_needs, 'fears_result'=>$sB_fears);
			} else if($key === 'secC'){
				$descArray = array('section' => 'Section_C', 'main_result'=>'People Oriented and Extroverted',  'behaviors_result'=>$sC_behaviors,'needs_result'=>$sC_needs, 'fears_result'=>$sC_fears);
			} else if($key === 'secD'){
				$descArray = array('section' => 'Section_D', 'main_result'=>'Task Oriented and Extroverted',  'behaviors_result'=>$sD_behaviors,'needs_result'=>$sD_needs, 'fears_result'=>$sD_fears);
			}
			$resultArray = array("section_per"=>$secArray, "section_details"=>$descArray);
			// $res = communicationAssessResult($user_answers,$result['company_email']);
			// print_r($res);die;
			$code = 200;
			$message = "Success";
		}
	}

	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message, 'user_details' => $result, 'user_result' => $resultArray));
}