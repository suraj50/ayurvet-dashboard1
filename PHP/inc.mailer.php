<?php 

function sendMail($to, $subject, $body) {
	  global $smtpun; global $smtpps;
	  $mail = new PHPMailer();   
	  $mail->IsSMTP();
	  $mail->SMTPDebug = 0;
	  $mail->SMTPAuth = TRUE;
	  $mail->SMTPSecure = "ssl";
	  $mail->Port     = 465;  
	  $mail->Username = $smtpun;
	  $mail->Password = $smtpps;
	  $mail->Host     = "smtp.gmail.com";
	  $mail->Mailer   = "smtp";
	  $mail->SetFrom($smtpun, "e2epeoplepractices");
	  // $mail->AddReplyTo($emp->email, "e2epeoplepractices");
	  $mail->AddAddress($to, "e2epeoplepractices");
	  $mail->Subject = $subject;
	  $mail->WordWrap   = 80;
	  $mail->MsgHTML($body);
	  $mail->IsHTML(true);
	  return $mail->Send();
}