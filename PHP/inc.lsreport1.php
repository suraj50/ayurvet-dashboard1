<?php 
function actionLeadershipReport1($params) {
     global $dbh; $message = ''; $code = 404; $lrtheme = array(); 
     if($params[0] !== null && $params[0] !== '') {
        $res = $dbh->query("select * from project_lr_theme where lrid = " . $params[0] . "");
        if($res) {
            while($row = $res->fetch_assoc()) {
                $obj = new stdClass();
                $obj->id = $row['id'];
                $obj->lrid = $row['lrid'];
                $obj->theme_name = $row['theme_name'];
                $obj->theme_notes = $row['theme_notes'];
            
                array_push($lrtheme, $obj);
            }
            $code = 200;
            $message = 'Successful'; 
        }
        else {
            $message = 'No theme available';
        }
     }
     else {
         $message = 'Leadership Report ID is empty';
     }
    
    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'lrtheme' => $lrtheme));
}

function actionLeadershipUniverse1($params) {
     global $dbh; $message = ''; $code = 404; $lruniverse = array(); 
     if($params[0] !== null && $params[0] !== '') {
        $res = $dbh->query("select * from project_lr_universe where lrid = " . $params[0] . "");
        if($res) {
            while($row = $res->fetch_assoc()) {
                $obj = new stdClass();
                $obj->id = $row['id'];
                $obj->lrid = $row['lrid'];  
                $obj->universe_name = $row['universe_name'];
                $obj->sc = $row['sc'];
                $obj->si = $row['si'];
                $obj->others = $row['others'];
                $obj->universe_notes = $row['universe_notes'];
            
                array_push($lruniverse, $obj);
            }
            $code = 200;
            $message = 'Successful'; 
        }
        else {
            $message = 'No Universe available';
        }
     }
     else {
         $message = 'Leadership Report ID is empty';
     }
    
    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'lruniverse' => $lruniverse));
}

