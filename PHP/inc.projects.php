<?php

function actionProjects($params) {
	global $dbh; $message = ''; $code = 404; $projects = array(); 
    $res = $dbh->query("select p.*, o.org_name as org_name, torg.org_name as target_org_name from projects p left join organizations o on o.id = p.org_id left join organizations torg on torg.id = p.target_org_id where p.status = 'active' order by p.id desc");
    if($res) {
        while($row = $res->fetch_assoc()) {
            $obj = (object) $row;
            $obj->surveys = array();
			$obj->surveys_label = '';            
			
			if($obj->start_date) $obj->start_date = Date('d-m-Y', strtotime($obj->start_date));
            if($obj->end_date) $obj->end_date = Date('d-m-Y', strtotime($obj->end_date));
            
            $obj->project_type_label = ''; 

            if($obj->project_type === 'LS') {
                $obj->project_type_label = 'Leadership Survey';
            }
            else if($obj->project_type === 'HRIS') {
                $obj->project_type_label = $obj->project_category;
            }

            $prs = $dbh->query("select * from project_surveys where project_id = ". $row['id']); $survey_labels = array();
            while($prow = $prs->fetch_assoc()) {
				$sobj = (object) $prow;
				
				array_push($obj->surveys, $sobj);    
                array_push($survey_labels,$sobj->survey_code);
            }
            $obj->surveys_label = implode(", ", $survey_labels);

            $obj->participants = array();
            $obj->participants_completed_count = 0;
            
            $ers = $dbh->query("select * from project_employees where project_id = ". $row['id']); $survey_labels = array();
            while($erow = $ers->fetch_assoc()) {
                $eobj = (object) $erow;    
                array_push($obj->participants, $eobj);    
                if($eobj->survey_status == 'complete') {
                    $obj->participants_completed_count++;
                }
            }

            $obj->progress =  (count($obj->participants) > 0 && $obj->participants_completed_count > 0) ? ceil( ($obj->participants_completed_count/count($obj->participants)) * 100 ) : 0;



            array_push($projects, $obj);
        }
        $code = 200;
        $message = 'Successful';
    }
    else {
        $message = 'No projects available';
    }
    
    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'projects' => $projects));
}

function actionProject($params) {
	$message = '';   $code = 404;  $project = new stdClass(); global $dbh;
	
	if($params !== null && $params !== '' && $params[0] !== null && $params[0] !== '' ) {
		$res = $dbh->query("select p.*, o.org_name as org_name, torg.org_name as target_org_name from projects p left join organizations o on o.id = p.org_id left join organizations torg on torg.id = p.target_org_id where p.id = ".$params[0]." AND p.status = 'active' order by p.id desc");
		if($res) {
			while($row = $res->fetch_assoc()) {
				$obj = (object) $row;
				$obj->surveys = array();
				$obj->survey_questions = array();
				$obj->surveys_label = '';            
				if($obj->start_date) $obj->start_date = Date('d-m-Y', strtotime($obj->start_date));
				if($obj->end_date) $obj->end_date = Date('d-m-Y', strtotime($obj->end_date));
				
				$obj->project_type_label = ''; 

				if($obj->project_type === 'LS') {
					$obj->project_type_label = 'Leadership Survey';
				}
				else if($obj->project_type === 'HRIS') {
					$obj->project_type_label = $obj->project_category;
				}

				$survey_ids = array();
				$prs = $dbh->query("select * from project_surveys where project_id = ". $row['id']); $survey_labels = array();
				while($prow = $prs->fetch_assoc()) {
					$sobj = (object) $prow;    
					
					array_push($survey_ids, $sobj->id);
					array_push($obj->surveys, $sobj);    
					array_push($survey_labels, $sobj->survey_code);
				}

				$obj->surveys_label = implode(", ", $survey_labels);
				
				if(count($survey_ids) > 0) {
					$qrs = $dbh->query("select * from project_survey_questions where survey_id in (".implode(", ", $survey_ids).")");
					while($qrow = $qrs->fetch_assoc()) {
						$qobj = (object) $qrow;    

						if($qobj->question_type === 'eum') {
							$qobj->opt_categories = array();

							$ors = $dbh->query("select * from project_survey_opt_categories where question_id = ".$qobj->id);
							while($orow = $ors->fetch_assoc()) {
								$oobj = (object) $orow;    
								array_push($qobj->opt_categories, $oobj);    
							}
						}

						array_push($obj->survey_questions, $qobj);    
					}

				}

				$obj->survey_answers = array();
				$ars = $dbh->query("select a.* from project_emp_survey_answers a where a.project_id = ". $row['id'] . " order by a.employee_id, a.question_id, a.answer_category, a.answer_order");
				while($arow = $ars->fetch_assoc()) {
					$aobj = (object) $arow;
					array_push($obj->survey_answers, $aobj); 
				}



				$obj->participants = array();
				$obj->participants_completed_count = 0;
				
				$ers = $dbh->query("select * from project_employees where project_id = ". $row['id']); $survey_labels = array();
				while($erow = $ers->fetch_assoc()) {
					$eobj = (object) $erow;    
					array_push($obj->participants, $eobj);    
					if($eobj->survey_status == 'complete') {
						$obj->participants_completed_count++;
					}
				}

				$obj->progress =  (count($obj->participants) > 0 && $obj->participants_completed_count > 0) ? ceil( ($obj->participants_completed_count/count($obj->participants)) * 100 ) : 0;


				$project = $obj;
			}
			$code = 200;
			$message = 'Successful';
		}
		else {
			$message = 'No projects available';
		}

	}
	else {
		$code = 403;
		$message = 'Project ID not valid';
	}


	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message, 'project' => $project));
}


function actionLeaderProjects($params) {
	global $dbh; $message = ''; $code = 404; $projects = array(); $project_ids = array();
	
	if($params !== null && $params !== '' && $params[0] !== null && $params[0] !== '' ) {
		$res = $dbh->query("select p.*, o.org_name as org_name, torg.org_name as target_org_name, torg.logo as target_org_logo from projects p left join organizations o on o.id = p.org_id left join organizations torg on torg.id = p.target_org_id where p.org_id = ".$params[0]." and p.target_org_id is not null order by p.id desc");
		if($res) {
			while($row = $res->fetch_assoc()) {
				$obj = (object) $row;
				
				$obj->participants = array();
				$obj->participants_completed_count = 0;
				
				$ers = $dbh->query("select * from project_employees where project_id = ". $row['id']); $survey_labels = array();
				while($erow = $ers->fetch_assoc()) {
					$eobj = (object) $erow;    
					array_push($obj->participants, $eobj);    
					if($eobj->survey_status == 'complete') {
						$obj->participants_completed_count++;
					}
				}

				$obj->progress =  (count($obj->participants) > 0 && $obj->participants_completed_count > 0) ? ceil( ($obj->participants_completed_count/count($obj->participants)) * 100 ) : 0;



				array_push($projects, $obj);
			}
			$code = 200;
			$message = 'Successful';
		}
		else {
			$message = 'No projects available';
		}
	}
	else {
		$code = 403;
		$message = 'Leader ID not valid';
	}
		
    http_response_code($code);
    echo json_encode(array('message' => $message, 'code' => $code, 'projects' => $projects));
}


function actionLeaderProject($params) {
	$message = '';   $code = 404;  $project = new stdClass(); global $dbh;
	
	if($params !== null && $params !== '' && $params[0] !== null && $params[0] !== '' ) {

		$user_email = $params[1]; $user_id = '';

		$res = $dbh->query("select p.* from projects p where p.id = ".$params[0]." AND p.status = 'active' order by p.id desc");
		if($res && mysqli_num_rows($res)>0) {
			while($row = $res->fetch_assoc()) {
				$obj = (object) $row;
				$obj->org = new stdClass();

				
				$obj->participants = array();
				$obj->myparticipant = '';
				$obj->participants_completed_count = 0;
				
				$ers = $dbh->query("select pe.*, md.designation_name as designationName from project_employees pe LEFT JOIN master_designations md on pe.designation = md.id where project_id = ". $row['id']); $survey_labels = array();
				while($erow = $ers->fetch_assoc()) {
					$eobj = (object) $erow;    
					if($user_email !== null && $user_email !== '') {
						if($erow['company_email'] === $user_email) {
							$user_id = $erow['id'];
							$obj->myparticipant = $eobj;
						}
					}
					array_push($obj->participants, $eobj);    
					if($eobj->survey_status == 'complete') {
						$obj->participants_completed_count++;
					}
				}

				
				if($obj->target_org_id !== null) {
					$ors = $dbh->query("select o.*, mi.industry_name, mc.country_name from organizations o LEFT JOIN master_industries mi on o.industry = mi.id LEFT JOIN master_countries mc on mc.id = o.country where o.id = ".$obj->target_org_id);
					if($ors && mysqli_num_rows($ors) >  0) {
						while($orow = $ors->fetch_assoc()) {
							$obj->org = (object) $orow;
						}
					}

					$jrs = $dbh->query("select * from tmp_json_data where jkey = ".$params[0]);
					if($jrs && mysqli_num_rows($jrs) > 0) {
						while($jrow = $jrs->fetch_assoc()) {
							//var_dump($jrow);
							$data = json_decode($jrow['json']);
							// var_dump($data);
							$obj->org->footPrints = $data->footPrints;
							$obj->org->finanicalEntities = $data->finanicalEntities;
							$obj->org->socialMedia = $data->socialMedia;
							$obj->org->newsUpdates = $data->newsUpdates;
						}
					}else{
							$obj->org->footPrints = [];
							$obj->org->finanicalEntities =  [];
							$obj->org->socialMedia =  [];
							$obj->org->newsUpdates =  [];
					}
				}

				$obj->ces = new stdClass();
				$obj->lsp = new stdClass();
				$obj->ilsp = '';
				$obj->competencies = new stdClass();
				$obj->icompetencies = new stdClass();
				$crs = $dbh->query("select * from project_ces_summary where project_id = ".$obj->id);
				if($crs && mysqli_num_rows($crs) >  0) {
					while($crow = $crs->fetch_assoc()) {
						$obj->ces = (object) $crow;
					}

					$obj->ces->kpis = array();

					$krs = $dbh->query("select * from project_ces_kpis where ces_id = ". $obj->ces->id);
					if($krs && mysqli_num_rows($krs) >  0) {
						while($krow = $krs->fetch_assoc()) {
							array_push($obj->ces->kpis, (object) $krow);
						}
						
					}

					// Leadership Pulse
					$lrs = $dbh->query("select * from project_leadership_pulse where ces_id = ". $obj->ces->id);
					if($lrs && mysqli_num_rows($lrs) >  0) {
						while($lrow = $lrs->fetch_assoc()) {
							$obj->lsp = (object) $lrow;
						}
					}

					// ILeadership Pulse
					if($user_id !== '') {
						$lrs = $dbh->query("select * from project_lp_empdata where ces_id = ". $obj->ces->id . " && employee_id = ". $user_id);
						if($lrs && mysqli_num_rows($lrs) >  0) {
							while($lrow = $lrs->fetch_assoc()) {
								$obj->ilsp = (object) $lrow;
							}
						}
					}

					
					// Competencies
					$crs = $dbh->query("select * from project_lp_competencies where ces_id = ". $obj->ces->id );
					if($crs && mysqli_num_rows($crs) >  0) {
						while($crow = $crs->fetch_assoc()) {
							if($crow['type'] === 'combined') {
								$obj->competencies = (object) $crow;
							}
							if($crow['type'] === 'individual' && $user_id !== '' && $crow['employee_id'] === $user_id) {
								$obj->icompetencies = (object) $crow;
							}
						}
					}

					
					// Values
					$obj->values = array();
					$vrs = $dbh->query("select * from project_lp_values");
					if($vrs && mysqli_num_rows($vrs) >  0) {
						while($vrow = $vrs->fetch_assoc()) {
							array_push($obj->values, (object) $vrow);
						}
					}

				}

				$obj->surveys = array();
				$obj->survey_questions = array();
				$obj->surveys_label = '';            
				if($obj->start_date) $obj->start_date = Date('d-m-Y', strtotime($obj->start_date));
				if($obj->end_date) $obj->end_date = Date('d-m-Y', strtotime($obj->end_date));
				
				$obj->project_type_label = ''; 

				if($obj->project_type === 'LS') {
					$obj->project_type_label = 'Leadership Survey';
				}
				else if($obj->project_type === 'HRIS') {
					$obj->project_type_label = $obj->project_category;
				}

				$survey_ids = array();
				$prs = $dbh->query("select * from project_surveys where project_id = ". $row['id']); $survey_labels = array();
				while($prow = $prs->fetch_assoc()) {
					$sobj = (object) $prow;    
					
					array_push($survey_ids, $sobj->id);
					array_push($obj->surveys, $sobj);    
					array_push($survey_labels, $sobj->survey_code);
				}

				$obj->surveys_label = implode(", ", $survey_labels);
				
				if(count($survey_ids) > 0) {
					$qrs = $dbh->query("select * from project_survey_questions where survey_id in (".implode(", ", $survey_ids).")");
					while($qrow = $qrs->fetch_assoc()) {
						$qobj = (object) $qrow;    

						if($qobj->question_type === 'eum') {
							$qobj->opt_categories = array();

							$ors = $dbh->query("select * from project_survey_opt_categories where question_id = ".$qobj->id);
							while($orow = $ors->fetch_assoc()) {
								$oobj = (object) $orow;    
								array_push($qobj->opt_categories, $oobj);    
							}
						}

						array_push($obj->survey_questions, $qobj);    
					}

				}

				$obj->survey_answers = array();
				$ars = $dbh->query("select a.* from project_emp_survey_answers a where a.project_id = ". $row['id'] . " order by a.employee_id, a.question_id, a.answer_category, a.answer_order");
				while($arow = $ars->fetch_assoc()) {
					$aobj = (object) $arow;
					array_push($obj->survey_answers, $aobj); 
				}

				$obj->progress =  (count($obj->participants) > 0 && $obj->participants_completed_count > 0) ? ceil( ($obj->participants_completed_count/count($obj->participants)) * 100 ) : 0;

				$obj->lsreport = '';
				if($obj->myparticipant !== '') {
					$ars = $dbh->query("select * from project_leadership_report where project_id = ".$obj->id. " and employee_id = ". $obj->myparticipant->id );
					if($ars && mysqli_num_rows($ars) > 0){
						while($arow = $ars->fetch_assoc()) {
							$obj->lsreport = (object) $arow;
						}
					}
				}

				$project = $obj;
			}
			$code = 200;
			$message = 'Successful';
		}
		else {
			$message = 'No projects available';
		}

	}
	else {
		$code = 403;
		$message = 'Project ID not valid';
	}


	http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message, 'project' => $project));
}

function requestProject() {
	$message = '';   $code = 404;  global $dbh; $result = array(); $org_name = '';

    $org_id = file_get_contents("php://input");

    // get organization name
    $sql = "SELECT org_name FROM organizations WHERE id = ".$org_id;
    $res = $dbh->query($sql);
    if($res) {
    	$res_org_name = mysqli_fetch_assoc($res);
    	$org_name = $res_org_name['org_name'];
    }
    // print_r($org_name);

    if($org_name != '') {
    	$create_project = "INSERT INTO projects(org_id,project_title,status,created_on,modified_on) VALUES(".$org_id.", '".$org_name."', 'requested', now(), now())";
    	$res = $dbh->query($create_project);

    	if($res) {
    		$code = 200;
    		$message = "Success";
    	}
    	else {
    		$message = "Failed";
    	}
    }

    http_response_code($code);
	echo json_encode(array('code' => $code, 'message' => $message));

}
