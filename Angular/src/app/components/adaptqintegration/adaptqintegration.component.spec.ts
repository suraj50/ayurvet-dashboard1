import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdaptqintegrationComponent } from './adaptqintegration.component';

describe('AdaptqintegrationComponent', () => {
  let component: AdaptqintegrationComponent;
  let fixture: ComponentFixture<AdaptqintegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdaptqintegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdaptqintegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
