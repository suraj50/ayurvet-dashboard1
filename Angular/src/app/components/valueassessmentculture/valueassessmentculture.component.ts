import { Component, OnInit, Input } from '@angular/core';
import { Project } from '../../models/project.model';
import { from } from 'rxjs';
import { pluck, groupBy } from 'rxjs/operators';

declare var ldBar: any;
@Component({
  selector: 'app-valueassessmentculture',
  templateUrl: './valueassessmentculture.component.html',
  styleUrls: ['./valueassessmentculture.component.css']
})
export class ValueassessmentcultureComponent implements OnInit {
@Input() project: Project;
public rcs: any;
public ivop: any;
public ivls: any;
public ivls_op1:any;
public ivls_op2: any;
public ivls_op3:any;
public ivoptions :any;
public currentView: any;
public cvls: any;
public branchno: any;
public uniqueresponse: any;
public responsedata_per: any;
public countUpView: any;
public mysource : any;
public selectedLocation: any;
public option: any;

public valueDysfunction:  number;
public departments: number;
public response: number;
public participants: number;
public s1: number;
public s2: number;
public s3: number;
public v1: any;
public v2: any;
public v3: any;
public v4: any;
public a1: any;
public a2: any;
public a3: any;
public a4: any;
public a5: any;
public a6: any;
public b1: any;
public b2: any;
public b3: any;
public b4: any;
public b5: any;
public b6: any;
public b7: any;
public b8: any;
public b9: any;
public c1: any;
public c2: any;
public c3: any;
public c4: any;
public c5: any;
public c6: any;
public c7: any;
public c8: any;
public c9: any;




  constructor() {
    this.selectedLocation = "Ayurvet Overall";
    this.option ="o1";
    this.changeimage();
    this.mysource = "assets/images/o1l1.JPG";
    this.valueDysfunction = 19;
    this.departments = 25;
    this.response = 93;
    this.participants= 330;
    this.s1 = 6;
    this.s2 = 4;
    this.s3 = 3;
    this.v1 = "Control: 22.15%";
    this.v2 = "Hierarchy: 9.77%";
    this.v3 = "Confusion: 9.45%";
    this.v4 = "";
    this.a1 = "Leadership";
    this.a2 = "Customer Satisfaction";
    this.a3 = "Brand Image";
    this.a4 = "Trust";
    this.a5 = "Honesty";
    this.a6 = "Accountability";
    this.b1 = "Continuous Improvement";
    this.b2 = "Trust";
    this.b3 = "Customer Satisfaction";
    this.b4 = "Brand Image";
    this.b5 = "";
    this.b6 = "";
    this.b7 = "";
    this.b8 = "";
    this.b9 = "";
    this.c1 = "Innovation";
    this.c2 = "Risk-Taking";
    this.c3 = "Mission Focus";
    this.c4 = "";

  	console.log(this.project);
  	this.rcs = [
		{'code': 'rcwl', 'prop': 'rc_work_life_balance', 'label': 'Personal Values', 'icon': 'balance-scale', 'opvalue': 'op1'},
		{'code': 'rcoe', 'prop': 'rc_org_experience', 'label': 'Current Organizational Values', 'icon': 'sitemap', 'opvalue': 'op2'},
		{'code': 'rcsb', 'prop': 'rc_salary_benefits', 'label': 'Desired Organizational Values', 'icon': 'wallet', 'opvalue': 'op3'}
		];
		this.ivls_op1 = [
		{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [] },
		{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [] },
		{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Honesty: 360', 'state':'PV'},{'label': 'Humour/Fun: 297', 'state': 'PV'},{'label': 'Integrity: 267', 'state':'PV'},{'label': 'Commitment: 225', 'state':'PV'},{'label': 'Positivity: 201', 'state':'PV'}] },
		{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Accountability: 249', 'state':'PV'},{'label': 'Balance (Home/Work): 201', 'state':'PV'}] },
		{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [] },
		{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Respect: 249', 'state':'PV'},{'label': 'Caring: 231', 'state':'PV'},{'label': 'Family: 213', 'state':'PV'}] },
		{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [] }
		];
		this.ivls_op2 = [
		{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [] },
		{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Making a difference: 309', 'state':'PV'}] },
		{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Commitment: 174', 'state':'PV'}] },
		{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Continuous improvement: 264', 'state': 'PV'},{'label': 'Teamwork: 222', 'state':'PV'},{'label': 'Accountability: 204', 'state': 'PV'}] },
		{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Brand: 231', 'state':'PV'},{'label': 'Bureaucray: 216', 'state': 'LV'},{'label': 'Hierarchy(L): 159', 'state':'LV'}] },
		{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Client Satisfaction: 378', 'state': 'PV'},{'label': 'Caring: 162', 'state':'PV'}] },
		{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [] }
		];
		this.ivls_op3 = [
		{'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [] },
		{'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Employee Fullfilment: 159', 'state':'PV'}] },
		{'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [] },
		{'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Accountability: 408', 'state':'PV'},{'label': 'Continuous improvement: 336', 'state':'PV'},{'label': 'Teamwork: 228', 'state':'PV'},{'label': 'Adaptability: 210', 'state':'PV'},{'label': 'Information sharing: 165', 'state':'PV'}] },
		{'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Professionalism: 189', 'state':'PV'}] },
		{'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Client Satisfaction: 447', 'state': 'PV'},{'label': 'open communication: 210', 'state':'PV'},{'label': 'Respect: 180', 'state': 'PV'}] },
		{'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [] }
		];
		this.ivop = 'op1';
		this.ivls = this.ivls_op1;
  }

	showKnob(el, prop) {
		var bar = new ldBar('#' + el);
		bar.set( (this.project.cvac[prop] * 10));
	}

	hideKnob(el, prop) {
		var bar = new ldBar('#' + el);
		bar.set(0);
	}
	ivopChange(opval){
		this.ivop = opval;
		if(this.ivls !== undefined){
		if(opval == 'op1')this.ivls = this.ivls_op1;

		if(opval == 'op2')this.ivls = this.ivls_op2;

		if(opval == 'op3')this.ivls = this.ivls_op3;
		}
  }

  selectChangeHandler (event: any) {

    this.selectedLocation = event.target.value;

  }


  changeoption(opt){
     this.option = opt;
    this.changeimage();
  }
  changeimage(){
    
  if(this.selectedLocation == 'Ayurvet Overall'){
    this.valueDysfunction = 19;
    this.departments = 25;
    this.response = 93;
    this.participants= 330;
    this.s1 = 6;
    this.s2 = 4;
    this.s3 = 3;
    this.v1 = "Control: 22.15%";
    this.v2 = "Hierarchy: 9.77%";
    this.v3 = "Confusion: 9.45%";
    this.v4 = "";
    this.a1 = "Leadership";
    this.a2 = "Customer Satisfaction";
    this.a3 = "Brand Image";
    this.a4 = "Trust";
    this.a5 = "Honesty";
    this.a6 = "Accountability";
    this.b1 = "Continuous Improvement";
    this.b2 = "Trust";
    this.b3 = "Customer Satisfaction";
    this.b4 = "Brand Image";
    this.b5 = "";
    this.b6 = "";
    this.b7 = "";
    this.b8 = "";
    this.b9 = "";
    this.c1 = "Innovation";
    this.c2 = "Risk-Taking";
    this.c3 = "Mission Focus";
    this.c4 = "";
  }

  if(this.selectedLocation == 'Senior Level'){
    this.valueDysfunction = 45.65;
    this.departments = 9;
    this.response = 100;
    this.participants= 10;
    this.s1 = 2;
    this.s2 = 2;
    this.s3 = 3;
    this.v1 = "Heirarchy: 50%";
this.v2 = "Bureaucracy: 30%";
this.v3 = "Control: 30%";
this.v4 = "";
this.a1 = "Integrity";
this.a2 = "Accountability";
this.a3 = "";
this.a4 = "";
this.a5 = "";
this.a6 = "";
this.b1 = "Accountability";
this.b2 = "Brand Image";
this.b3 = "";
this.b4 = "";
this.b5 = "";
this.b6 = "";
this.b7 = "";
this.b8 = "";
this.b9 = "";
this.c1 = "Global Thinking";
this.c2 = "Long-Term Perspective";
this.c3 = "Innovation";
this.c4 = "";
  }

  if(this.selectedLocation == 'Mid Level'){
    this.valueDysfunction = 15.47;
    this.departments = 13;
    this.response = 98.4;
    this.participants= 62;
    this.s1 = 5;
    this.s2 = 5;
    this.s3 = 3;
this.v1 = "Heirarchy: 14.75%";
this.v2 = "Control: 14.75%";
this.v3 = "Confusion: 6.56%";
this.v4 = "Bureaucracy: 6.56%"
this.a1 = "Ethics";
this.a2 = "Continuous Improvement";
this.a3 = "Customer Satisfaction";
this.a4 = "Brand Image";
this.a5 = "Accountability";
this.a6 = "";
this.b1 = "Long-Term Perspective";
this.b2 = "Employee Engagement";
this.b3 = "Continuous Improvement";
this.b4 = "Customer Satisfaction";
this.b5 = "Brand Image";
this.b6 = "Accountability";
this.b7 = "";
this.b8 = "";
this.b9 = "";
this.c1 = "Risk-Taking";
this.c2 = "Innovation";
this.c3 = "Competitive";
this.c4 = "";

  }

  if(this.selectedLocation == 'Junior Level'){
    this.valueDysfunction = 18.74;
    this.departments = 17;
    this.response = 91.5;
    this.participants= 258;
    this.s1 = 6;
    this.s2 = 3;
    this.s3 = 3;
    this.v1 = "Control: 23.7%";
this.v2 = "Confusion: 10.2%";
this.v3 = "Hierarchy: 6.8%";
this.v4 = "";
this.a1 = "Leadership";
this.a2 = "Customer Satisfaction";
this.a3 = "Brand Image";
this.a4 = "Trust";
this.a5 = "Accountability";
this.a6 = "Honesty";
this.b1 = "Leadership";
this.b2 = "Accountability";
this.b3 = "Honesty";
this.b4 = "";
this.b5 = "";
this.b6 = "";
this.b7 = "";
this.b8 = "";
this.b9 = "";
this.c1 = "Leadership Development";
this.c2 = "Mission Focus";
this.c3 = "Innovation";
this.c4 = "";
  }

  if(this.selectedLocation == 'Corporate Centre'){
    this.valueDysfunction = 20;
    this.departments = 15;
    this.response = 70.9;
    this.participants= 471;
    this.s1 = 1;
    this.s2 = 3;
    this.s3 = 3;
    this.v1 = "Shot-term Focus";
this.v2 = "Blame";
this.v3 = "Power";
this.a1 = "Accountability";
this.a2 = "";
this.a3 = "";
this.a4 = "";
this.a5 = "";
this.a6 = "";
this.b1 = "Financial Stability";
this.b2 = "Vision";
this.b3 = "Accountability";
this.b4 = "";
this.b5 = "";
this.b6 = "";
this.b7 = "";
this.b8 = "";
this.b9 = "";
this.c1 = "Customer Satisfaction: 141%";
this.c2 = "Long-term Perspective: 206%";
this.c3 = "Leadership Development: 418%";
this.c4 = "";
  }


    if(this.option == 'o1' && this.selectedLocation == 'Ayurvet Overall'){
      this.mysource = "assets/images/o1l1.JPG";
      

    }



    if(this.option == 'o2' && this.selectedLocation == 'Ayurvet Overall'){
      this.mysource = "assets/images/o2l1.JPG";



    }

    if(this.option == 'o3' && this.selectedLocation == 'Ayurvet Overall'){
      this.mysource = "assets/images/o3l1.JPG";

    }
    if(this.option == 'o4' && this.selectedLocation == 'Ayurvet Overall'){
      this.mysource = "assets/images/o4l1.JPG";

    }


    if(this.option == 'o1' && this.selectedLocation == 'Senior Level'){
      this.mysource = "assets/images/o1l2.JPG";

    }



    if(this.option == 'o2' && this.selectedLocation == 'Senior Level'){
      this.mysource = "assets/images/o2l2.JPG";

    }

    if(this.option == 'o3' && this.selectedLocation == 'Senior Level'){
      this.mysource = "assets/images/o3l2.JPG";

    }
    if(this.option == 'o4' && this.selectedLocation == 'Senior Level'){
      this.mysource = "assets/images/o4l2.JPG";

    }







    if(this.option == 'o1' && this.selectedLocation == 'Mid Level'){
      this.mysource = "assets/images/o1l3.JPG";

    }



    if(this.option == 'o2' && this.selectedLocation == 'Mid Level'){
      this.mysource = "assets/images/o2l3.JPG";

    }

    if(this.option == 'o3' && this.selectedLocation == 'Mid Level'){
      this.mysource = "assets/images/o3l3.JPG";

    }
    if(this.option == 'o4' && this.selectedLocation == 'Mid Level'){
      this.mysource = "assets/images/o4l3.JPG";

    }




    if(this.option == 'o1' && this.selectedLocation == 'Junior Level'){
      this.mysource = "assets/images/o1l4.JPG";

    }



    if(this.option == 'o2' && this.selectedLocation == 'Junior Level'){
      this.mysource = "assets/images/o2l4.JPG";

    }

    if(this.option == 'o3' && this.selectedLocation == 'Junior Level'){
      this.mysource = "assets/images/o3l4.JPG";

    }
    if(this.option == 'o4' && this.selectedLocation == 'Junior Level'){
      this.mysource = "assets/images/o4l4.JPG";

    }







    if(this.option == 'o1' && this.selectedLocation == 'Corporate Centre'){
      this.mysource = "assets/images/o1l5.JPG";

    }



    if(this.option == 'o2' && this.selectedLocation == 'Corporate Centre'){
      this.mysource = "assets/images/o2l5.JPG";

    }

    if(this.option == 'o3' && this.selectedLocation == 'Corporate Centre'){
      this.mysource = "assets/images/o3l5.JPG";

    }
    if(this.option == 'o4' && this.selectedLocation == 'Corporate Centre'){
      this.mysource = "assets/images/o4l5.JPG";

    }


/* <option value="Overall">Overall</option>
<option value="Angul">Angul</option>
<option   value="Raigarh">Raigarh</option>
<option value="Oman">Oman</option>
<option value="Corporate Centre">*/



}

	resetCountUp() {
		this.countUpView = false;
	}

	setCountUp() {
		this.countUpView = true;
	}
	initStuff() {
    let self = this;
    self.resetCountUp();
    setTimeout(function() {
      self.setCountUp();
    }, 1000);

  }
	ngOnInit() {
		this.project.cvac = {
			"executiveSummary": "This report gives us an overview of what is important to the customers, their current experiences and what they view to be important for the future of the organization.",
			// "opinion_index": 3.7,
			"review": "1170",
			"interview":"693",
			"ceo_approval":"84",
			"friend_recommend":"62",
			"rc_work_life_balance": 3.7,
			"rc_org_experience": 4.2,
			"rc_salary_benefits": 2.4,
			"rc_job_security": 5.3,
			"rc_management": 2.4
		};
		let self = this;
		setTimeout(function() {
		for(let rc of self.rcs) {
		self.hideKnob(rc.code, rc.prop);
		}

		}, 1000);
		this.ivopChange(this.ivop);
	}
}
