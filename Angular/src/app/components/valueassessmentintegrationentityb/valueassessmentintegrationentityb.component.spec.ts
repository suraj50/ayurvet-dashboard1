import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueassessmentintegrationentitybComponent } from './valueassessmentintegrationentityb.component';

describe('ValueassessmentintegrationentitybComponent', () => {
  let component: ValueassessmentintegrationentitybComponent;
  let fixture: ComponentFixture<ValueassessmentintegrationentitybComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValueassessmentintegrationentitybComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueassessmentintegrationentitybComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
