import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

import { DataService } from '../../services/data.service';
import { User } from '../../models/user.model';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

	@BlockUI() blockUI: NgBlockUI;
	@ViewChild('auto') auto;

	public registerCode: any;
	public project: any;
	public projectValid: any;
	public errorMessage: any;
	public errors: any;

	constructor(
		private dataService: DataService,
		private route: ActivatedRoute,
		private title: Title
		)  { 
		this.blockUI.start('Loading...'); // Start blocking
		this.registerCode = this.route.snapshot.params.registerCode;
		this.project = {};
		this.projectValid = true;
		this.errorMessage = '';      
		this.errors = [];   
	}

	ngOnInit() {
		if(this.registerCode !== '' && this.registerCode !== undefined) {
			this.dataService.getRegisterAccess(this.registerCode).subscribe(
				data => {
					if(data.project !== undefined && data.project !== '') {
						this.project = data.project;
						console.log(data.project);
						this.title.setTitle(this.project.project_title);
					}
					else {
						this.projectValid = false;
						this.errorMessage = JSON.stringify(data.message);
					}
					this.blockUI.stop();
				},
				err => {
					this.projectValid = false;
					this.errorMessage = JSON.stringify(err.message);
					console.log("inside Registration error", JSON.stringify(err.message));
					this.blockUI.stop();
				}
				);
		}
		else {
			this.projectValid = false;
		}
	}

}
