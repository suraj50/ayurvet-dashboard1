import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

import { DataService } from '../../services/data.service';
import { User } from '../../models/user.model';
@Component({
	selector: 'app-dlregister',
	templateUrl: './dlregister.component.html',
	styleUrls: ['./dlregister.component.css']
})
export class DlregisterComponent implements OnInit {
	@BlockUI() blockUI: NgBlockUI;
	@ViewChild('auto') auto;

	contactForm: FormGroup;
	submitted = false;
  
	public registerCode: any;
    public project: any;
	public errors: any;
	public user: any;
	public errorMessage: any;
    public userdata: any;
	public userForm: any;
	public showentryform: boolean;
	public dlCurAccessDeal: boolean;
	public projectValid: any;
	public registerResponse: any;
	public emailPattern: any;
	public particType: any;
	public particTypeerror: any;
	public dataflag:boolean;
	public describeyou: any;
	// public designationRequired: any;
	constructor(
		private dataService: DataService,
		private route: ActivatedRoute,
		private router: Router,
		private formBuilder: FormBuilder,
		private title: Title
	) {
		this.blockUI.start('Loading...'); // Start blocking
        this.registerCode = this.route.snapshot.params.registerCode;
		this.projectValid = true;
		this.user = {};
		this.userdata = {};
		this.errors = [];
		this.registerResponse = '';
		// this.designationRequired = false;
		// this.userdata.companyName = '';
		// this.userdata.companyWebsite = '';
		// this.userdata.empDesignation = '';
		// this.userdata.empContactNo = '';
	 }

	ngOnInit() {
		this.showentryform = true;
		this.dlCurAccessDeal= false;
		this.particTypeerror = 'no';
		this.userdata.userTypeOfParticipant = '';
		this.userdata.userCurAccessDeal = '';
		this.userdata.userDescribesYou = '';
		// this.userdata.userDesignation = 'NA';
		// if(this.dlCurAccessDeal === false){
		// 	this.userdata.userCurAccessDeal = 'NA';
		// } 
 		this.userdata.userDesignation = '-';
 		this.userdata.userDescribe = '-';
		this.userdata.userContactNo = '91';
		this.emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,64}$';
		// this.registerResponse = '';
		// console.log(this.registerResponse);

	    this.contactForm = this.formBuilder.group({
		     	userFirstName: ['', Validators.required],
		        userLastName: ['', Validators.required],
		        userEmail: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
		        userTypeOfParticipant: ['', Validators.required],
		        userCompanyName: ['', Validators.required],
		        userContactNo: ['', Validators.required],
		        userCurAccessDeal: ['', Validators.required],
		        userDesignation: ['', Validators.required],
		        userDescribesYou: ['', Validators.required],
		        userDescribe: ['', Validators.required]
	    });
		if(this.registerCode !== '' && this.registerCode !== undefined) {
            this.dataService.getRegisterAccess(this.registerCode).subscribe(
                data => {
                    if(data.project !== undefined && data.project !== '') {
                        this.project = data.project;
                        this.title.setTitle(this.project.project_title);
                        this.user = new User();
                        this.user.generation = '-';
                    }
                    else {
                        this.projectValid = false;
                        this.errorMessage = JSON.stringify(data.message);
                    }
                    this.blockUI.stop();
                },
                err => {
                    this.projectValid = false;
                  this.errorMessage = JSON.stringify(err.message);
                  console.log("Inside Registration error", JSON.stringify(err.message));
                  this.blockUI.stop();
                }
            );
        }
        else {
            this.projectValid = false;
        }
	}
	checkdeal(){
		// if(this.userdata.userTypeOfParticipant ==='CHRO' || this.userdata.userTypeOfParticipant ==='CEO' || this.userdata.userTypeOfParticipant ==='CFO' || this.userdata.userTypeOfParticipant ==='Others'){
		if(this.userdata.userTypeOfParticipant ==='CHRO' || this.userdata.userTypeOfParticipant ==='CEO' || this.userdata.userTypeOfParticipant ==='CFO'){
			this.userdata.userCurAccessDeal = '';
			// this.dlCurAccessDeal = true;
			// if(this.userdata.userTypeOfParticipant ==='Others') {
			// 	this.userdata.userDesignation = '';
			// }
			// else {
			// 	this.userdata.userDesignation = 'NA';
			// }
		} else {
			// this.dlCurAccessDeal = false;
			// this.userdata.userCurAccessDeal = 'NA';
		}

	}

	formSubmit() {
		this.submitted = true;

		// if(this.userdata.userTypeOfParticipant ==='Others') {
		// 	this.userdata.userTypeOfParticipant = this.userdata.userDesignation;
		// }
		// else {
		// 	this.userdata.userDesignation = 'NA';
		// }
		if (this.contactForm.invalid) {
			return;
		}
		if(this.userdata.userTypeOfParticipant === 'Others'){
			console.log(this.userdata.userDesignation);
			if(this.userdata.userDesignation != '-'){
				this.particTypeerror = 'no';
				this.particType = this.userdata.userDesignation;
			} else{
				this.particTypeerror = 'yes';
			}
			console.log('other');
		}else{
			console.log('not other');
			this.particTypeerror = 'no';
			this.particType = this.userdata.userTypeOfParticipant;
		}

		if(this.userdata.userDescribesYou === 'Others'){
			if(this.userdata.userDescribe != '-') {
				this.describeyou = this.userdata.userDescribe;
			}else{
				return;
			}
		}
		else {
			this.describeyou = this.userdata.userDescribesYou;
		}
		console.log(this.describeyou);

		console.log(this.particTypeerror);
		if(this.particTypeerror === 'no'){
			var empdataObj = {
				userFirstName: this.userdata.userFirstName,
				userLastName: this.userdata.userLastName,
				userEmail: this.userdata.userEmail,
				userTypeOfParticipant: this.particType,
				userDescribeYou: this.describeyou,
				userContactNo: this.userdata.userContactNo,
				userCompanyName: this.userdata.userCompanyName,
				userCurAccessDeal: this.userdata.userCurAccessDeal,
				projectId: this.project.id
			}
			console.log(empdataObj);
			//alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))

			

			this.blockUI.start('Submitting...'); // Start blocking
			this.dataService.dlregistration(empdataObj).subscribe(
				data => {
					// console.log(data);
					if(data.code === 200) {
						if(data.message === 'Success') { 
							this.registerResponse = 'success';
							// Swal.fire('Addedd!','Details added successfully!', 'success' );
							this.showentryform = true;
							this.user = data.user;
							this.router.navigate(['dlresearchsurveyins/'+this.user.survey_link]);
							// document.getElementById("closemodal").click();
						}
						else if(data.message === 'already registered') {
							Swal.fire('Warning!','User already registered with this email!', 'warning' );
						}
						else if(data.message === 'Failed to add data, try again later.') { console.log('here'); this.registerResponse = 'error'; }                    
					}
					else {
						console.log('error');
						this.registerResponse = 'error';
					}
					this.blockUI.stop();
				},
				err => {
					this.registerResponse = 'error';
					console.log("inside Registration error", JSON.stringify(err.message));
					this.blockUI.stop();
				});
		} else{
			this.particTypeerror = 'yes';
		}
	}
}
