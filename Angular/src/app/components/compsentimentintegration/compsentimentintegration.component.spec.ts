import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompsentimentintegrationComponent } from './compsentimentintegration.component';

describe('CompsentimentintegrationComponent', () => {
  let component: CompsentimentintegrationComponent;
  let fixture: ComponentFixture<CompsentimentintegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompsentimentintegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompsentimentintegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
