import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';


import { DataService } from '../../services/data.service';
import { User } from '../../models/user.model';

@Component({
	selector: 'app-dlcontactform',
	templateUrl: './dlcontactform.component.html',
	styleUrls: ['./dlcontactform.component.css']
})
export class DlcontactformComponent implements OnInit {

	@BlockUI() blockUI: NgBlockUI;
	@ViewChild('auto') auto;
	registerForm: FormGroup;
	submitted = false;

	public errors: any;
	public employeedata: any;
	public userForm: any;
	public showentryform: boolean;
	public surveylink: any;
	constructor(
		private dataService: DataService,
		private route: ActivatedRoute,
		 private router: Router,
		private formBuilder: FormBuilder
		) { 
		this.blockUI.start('Loading...'); // Start blocking
		this.errors = [];
		this.employeedata = {};
		this.showentryform = false;
		// this.employeedata.empGender = 'Male';
	}

	ngOnInit() {
		this.showentryform = true;
		this.employeedata.companyAddress = '-';
		this.surveylink = this.route.snapshot.params.surveyLink;
        console.log(this.surveylink);

        this.dataService.getEmpCompanyName(this.surveylink).subscribe(
			data => {

				// 1. setup left panel of A
				console.log(data);
				this.employeedata.companyName = data.persondetail.company_name;
				this.employeedata.empFirstName = data.persondetail.first_name;
				this.employeedata.empLastName = data.persondetail.last_name;
				this.employeedata.empEmail = data.persondetail.company_email;
			},
			err => {
				Swal.fire('SORRY', "No Data Found", 'error');
				this.blockUI.stop();
			});

			this.registerForm = this.formBuilder.group({
				companyName: ['', Validators.required],
				companyWebsite: ['', Validators.required],
				empCount: ['', Validators.required],
				companyAddress: ['', Validators.required],
				empFirstName: ['', Validators.required],
				empLastName: ['', Validators.required],
				empDesignation: ['', Validators.required],
				empContactNo: ['', Validators.required],
				empEmail: ['', [Validators.required, Validators.email]]
			});


	}

	dlformSubmit() {
		this.submitted = true;
		if (this.registerForm.invalid) {
			return;
		}
		
		var empdataObj = {
			companyName: this.employeedata.companyName,
			companyWebsite: this.employeedata.companyWebsite,
			empCount: this.employeedata.empCount,
			companyAddress: this.employeedata.companyAddress,

			empFirstName: this.employeedata.empFirstName,
			empLastName: this.employeedata.empLastName,
			empDesignation: this.employeedata.empDesignation,
			empContactNo: this.employeedata.empContactNo,
			empEmail: this.employeedata.empEmail
		}
		console.log(empdataObj);
		//alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
		this.blockUI.start('Submitting...'); // Start blocking
		this.dataService.addContactPersonDetails(empdataObj).subscribe(
			data => {
				console.log(data);
				if(data.code === 200) {
					if(data.message === 'Success') { 
						Swal.fire('Thank You!','Your organization registration request sent successfully. We will get back to you soon.', 'success' );
						this.showentryform = true;
						this.router.navigate(['login/']);
						// document.getElementById("closemodal").click();
						this.blockUI.stop();
					}
					else if(data.message === 'Failed to add data, try again later.') { console.log('here'); }                    
				}
				else {
					console.log('error');
				}
				this.blockUI.stop();
			},
			err => {
				console.log("inside Registration error", JSON.stringify(err.message));
				this.blockUI.stop();
			}
			);

	}

}
