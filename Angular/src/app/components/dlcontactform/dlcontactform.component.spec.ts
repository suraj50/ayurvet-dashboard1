import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlcontactformComponent } from './dlcontactform.component';

describe('DlcontactformComponent', () => {
  let component: DlcontactformComponent;
  let fixture: ComponentFixture<DlcontactformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlcontactformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlcontactformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
