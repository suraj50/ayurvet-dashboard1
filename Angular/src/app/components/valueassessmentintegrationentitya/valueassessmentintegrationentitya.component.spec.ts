import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValueassessmentintegrationentityaComponent } from './valueassessmentintegrationentitya.component';

describe('ValueassessmentintegrationentityaComponent', () => {
  let component: ValueassessmentintegrationentityaComponent;
  let fixture: ComponentFixture<ValueassessmentintegrationentityaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValueassessmentintegrationentityaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValueassessmentintegrationentityaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
