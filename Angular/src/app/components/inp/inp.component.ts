import { Component, OnInit, Input } from '@angular/core';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';

import { Project } from '../../models/project.model';

declare var ldBar: any;
declare var require: any;
var Highcharts = require('highcharts/highcharts.js');

@Component({
  selector: 'app-inp',
  templateUrl: './inp.component.html',
  styleUrls: ['./inp.component.css']
})
export class InpComponent implements OnInit {
  @Input() project: Project;
  public rcs: any;
  public rcs1: any;
  public inpTab: any;
  public tdData: any;
  public initLoaded: any;
  public currentView: any;
  public overallRating: any;
  public pro1: any;
  public pro2: any;
  public pro3: any;
  public con1: any;
  public con2: any;
  public con3: any;

  public reviews: any;
  public interviews: any;
  public approval: any;
  public recommendation: any;


  constructor() {

    this.currentView = "industry";
    this.overallRating = -0.09;

    this.pro1 = "The company pay is at par with expectation";
    this.pro2 = "On-time salary";
    this.pro3 = "A lot of time for self, can study while working";

    this.con1 = "Heavy work pressure, need to improve culture of work";
    this.con2 = "Slow processes, long chain of approvals";
    this.con3 = "Transferable jobs and promotions always at extended fixed time";

    this.reviews = 1470;
    this.interviews = 29;
    this.approval= 52;
    this.recommendation = 70;


    this.rcs = [
      {'code': 'rcwl', 'prop': 3.66, 'label': 'Overall', 'icon': 'tachometer-alt'},
      {'code': 'rcwl2', 'prop': 3.82, 'label': 'Work / Life Balance', 'icon': 'balance-scale'},
      {'code': 'rcoe2', 'prop': 3.86, 'label': 'Organizational Experience', 'icon': 'sitemap'},
      {'code': 'rcsb2', 'prop': 3.77, 'label': 'Salary / Benefits', 'icon': 'wallet'},
      {'code': 'rcjc', 'prop': 3.67, 'label': 'Job Security', 'icon': 'user-shield'},
      {'code': 'rcmn', 'prop': 3.28, 'label': 'Management', 'icon': 'tasks'}
    ]

    this.rcs1 = [
      {'code': 'rcwl2', 'prop': 3, 'label': 'Work / Life Balance', 'icon': 'balance-scale'},
      {'code': 'rcoe2', 'prop': 3.38, 'label': 'Culture and Values', 'icon': 'sitemap'},
      {'code': 'rcsb2', 'prop': 3.30, 'label': 'Compensation and Benefits', 'icon': 'wallet'},
      {'code': 'rcjc', 'prop': 3.48, 'label': 'Career Opportunities', 'icon': 'user-shield'},
      {'code': 'rcmn', 'prop': 3.13, 'label': 'Senior Management', 'icon': 'tasks'}
    ]
  }

  showKnob(el, prop) {

    var bar = new ldBar('#' + el);
    bar.set(prop*20);
  }

  hideKnob(el, prop) {
    var bar = new ldBar('#' + el);
    bar.set(0);
  }

 switchView(view) {
  this.currentView = view;


  if(this.currentView == "industry"){
    this.overallRating = -0.26;

    this.pro1 = "The company pay is at par with expectation";
    this.pro2 = "On-time salary";
    this.pro3 = "A lot of time for self, can study while working";

    this.con1 = "Heavy work pressure, need to improve culture of work";
    this.con2 = "Slow processes, long chain of approvals";
    this.con3 = "Transferable jobs and promotions always at extended fixed time";

    this.reviews = 1470;
    this.interviews = 29;
    this.approval= 52;
    this.recommendation = 70;

    this.rcs = [
      {'code': 'rcwl', 'prop': 3.66, 'label': 'Overall', 'icon': 'tachometer-alt'},
      {'code': 'rcwl2', 'prop': 3.82, 'label': 'Work / Life Balance', 'icon': 'balance-scale'},
      {'code': 'rcoe2', 'prop': 3.86, 'label': 'Organizational Experience', 'icon': 'sitemap'},
      {'code': 'rcsb2', 'prop': 3.77, 'label': 'Salary / Benefits', 'icon': 'wallet'},
      {'code': 'rcjc', 'prop': 3.67, 'label': 'Job Security', 'icon': 'user-shield'},
      {'code': 'rcmn', 'prop': 3.28, 'label': 'Management', 'icon': 'tasks'}

   ]
   
   }

  if(this.currentView == "jsw"){
    this.overallRating = 0.43;

    this.pro1 = "Interactive management";
    this.pro2 = "Driven by policies";
    this.pro3 = "High professional growth";

    this.con1 = "Need for better compensation policies to appreciate good performers";
    this.con2 = "Bureaucracy at the manager level";
    this.con3 = "Need for a better Grievance management system";

    this.reviews = 123;
    this.interviews = 7;
    this.approval= "NA";
    this.recommendation = 82;

    this.rcs = [
      {'code': 'rcwl', 'prop': 3.14, 'label': 'Overall', 'icon': 'tachometer-alt'},
      {'code': 'rcwl2', 'prop': 3.04, 'label': 'Work / Life Balance', 'icon': 'balance-scale'},
      {'code': 'rcoe2', 'prop': 3.09, 'label': 'Organizational Experience', 'icon': 'sitemap'},
      {'code': 'rcsb2', 'prop': 3.43, 'label': 'Salary / Benefits', 'icon': 'wallet'},
      {'code': 'rcjc', 'prop': 3.63, 'label': 'Job Security', 'icon': 'user-shield'},
      {'code': 'rcmn', 'prop': 3.85, 'label': 'Management', 'icon': 'tasks'}
    ]

    
   }

 if(this.currentView == "tata"){
  this.overallRating = -0.15;

  this.pro1 = "Great exposure to multiple work functions";
  this.pro2 = "Great work environment";
  this.pro3 = "Appreciated product marketing strategies";

  this.con1 = "Majorly contractual employee base";
  this.con2 = "Long working hours";
  this.con3 = "Favourism and age bias";

  this.reviews = 1270;
  this.interviews = 19;
  this.approval= "NA";
  this.recommendation = 57;

  this.rcs = [
    {'code': 'rcwl', 'prop': 3.72, 'label': 'Overall', 'icon': 'tachometer-alt'},
   {'code': 'rcwl2', 'prop': 3.89, 'label': 'Work / Life Balance', 'icon': 'balance-scale'},
   {'code': 'rcoe2', 'prop': 3.94, 'label': 'Organizational Experience', 'icon': 'sitemap'},
   {'code': 'rcsb2', 'prop': 3.82, 'label': 'Salary / Benefits', 'icon': 'wallet'},
   {'code': 'rcjc', 'prop': 3.69, 'label': 'Job Security', 'icon': 'user-shield'},
   {'code': 'rcmn', 'prop': 3.24, 'label': 'Management', 'icon': 'tasks'}

 ]
 }

 if(this.currentView == "sail"){
  this.overallRating = 0.17;

  this.pro1 = "Adequate performance benefits";
  this.pro2 = "Supportive work environments";
  this.pro3 = "Ample scope for future growth";

  this.con1 = "Less suppport from Seniors";
  this.con2 = "Lesser compensation then industry standards";
  this.con3 = "Delayed incentives and benefits";

  this.reviews = 21;
  this.interviews = "NA";
  this.approval= "NA";
  this.recommendation = "NA";

  this.rcs = [
    {'code': 'rcwl', 'prop': 3.40, 'label': 'Overall', 'icon': 'tachometer-alt'},
   {'code': 'rcwl2', 'prop': 4.29, 'label': 'Work / Life Balance', 'icon': 'balance-scale'},
   {'code': 'rcoe2', 'prop': 3.71, 'label': 'Organizational Experience', 'icon': 'sitemap'},
   {'code': 'rcsb2', 'prop': 3.62, 'label': 'Salary / Benefits', 'icon': 'wallet'},
   {'code': 'rcjc', 'prop': 3.38, 'label': 'Job Security', 'icon': 'user-shield'},
   {'code': 'rcmn', 'prop': 2, 'label': 'Management', 'icon': 'tasks'},

  //  {'code': 'rcwl', 'prop': 3.57, 'label': 'Overall', 'icon': 'tachometer-alt'},
  //  {'code': 'rcwl2', 'prop': 3.83, 'label': 'Work / Life Balance', 'icon': 'balance-scale'},
  //  {'code': 'rcoe2', 'prop': 3.65, 'label': 'Organizational Experience', 'icon': 'sitemap'},
  //  {'code': 'rcsb2', 'prop': 3.42, 'label': 'Salary / Benefits', 'icon': 'wallet'},
  //  {'code': 'rcjc', 'prop': 3.43, 'label': 'Job Security', 'icon': 'user-shield'},
  //  {'code': 'rcmn', 'prop': 3.55, 'label': 'Management', 'icon': 'tasks'},

 ]
 }
 let self = this;
 setTimeout(function() {
     for(let rc of self.rcs) {
        self.hideKnob(rc.code, rc.prop);
       self.showKnob(rc.code, rc.prop);
     }

 }, 1000);
}
 initStuff(){

   let self = this;
   setTimeout(function() {
       for(let rc of self.rcs) {
          self.hideKnob(rc.code, rc.prop);
         self.showKnob(rc.code, rc.prop);
       }

   }, 1000);
 }
 ngOnInit() {
   this.project.pskc = {
     "executiveSummary": "People Speak is a comprehensive report telling your organizations's story with actionable insights taking into account factors that offer more value than internal employee engagement surveys.",
     "opinion_index": 3.7,
     "review": "1612",
     "interview":"72",
     "ceo_approval":"90",
     "friend_recommend":"73",
     "rc_work_life_balance1": 3.63,
     "rc_org_experience1": 3.02,
     "rc_salary_benefits1": 3.98,
     "rc_job_security": 4.42,
     "rc_management": 3.88
   };

// this.initStuff();

 }

}

