import { Component, OnInit, Input, ElementRef} from '@angular/core';

import { Project } from '../../models/project.model';

@Component({
  selector: 'app-adaptqculture',
  templateUrl: './adaptqculture.component.html',
  styleUrls: ['./adaptqculture.component.css']
})
export class AdaptqcultureComponent implements OnInit {
	@Input() project: Project;
	adaptqdash: any;
	public poscore: any;
	public mdindex: any;
  public adapq: any;
  public selectedLocation: any;
  public highlyAdaptive: any;
  public moreAdaptive: any;
  public neutral: any;
  public lessAdaptive: any;
  public notAdaptive: any;
  public highlyAdaptiveCount: any;
  public moreAdaptiveCount: any;
  public neutralCount: any;
  public lessAdaptiveCount: any;
  public notAdaptiveCount: any;
  public needlestyle: any;
  public dynamicStyles: any;
  public nps: any;
  public a1p: any;
  public a2p: any;
  public a3p: any;
  public a4p: any;
  public t1p: any;
  public t2p: any;
  public t3p: any;
  public t4p: any;
  public g1p: any;
  public g2p: any;

  public a1a: any;
  public a2a: any;
  public a3a: any;
  public a4a: any;
  public t1a: any;
  public t2a: any;
  public t3a: any;
  public t4a: any;
  public g1a: any;
  public g2a: any;

  public a1pv:any;
  public a1lv:any;
  public a2pv:any;
  public a2lv:any;
  public a3pv:any;
  public a3lv:any;
  public a4pv:any;
  public a4lv:any;

  public t1pv:any;
  public t1lv:any;
  public t2pv:any;
  public t2lv:any;
  public t3pv:any;
  public t3lv:any;
  public t4pv:any;
  public t4lv:any;


  public g1pv:any;
  public g1lv:any;
  public g2pv:any;
  public g2lv:any;




  constructor(private el: ElementRef) {

    this.nps= 54.1;
    this.selectedLocation = 'l1';
    this.highlyAdaptive=2.6;
    this.moreAdaptive=72.3;
    this.neutral=24.8; 
    this.lessAdaptive=0.3;
    this.notAdaptive= 0.0;
    this.highlyAdaptiveCount=8;
    this.moreAdaptiveCount=222;
    this.neutralCount=76;
    this.lessAdaptiveCount=1;
    this.notAdaptiveCount=0;


  	this.poscore = [];
     this.poscore = {'0':'0','1':'0'}
     this.needlestyle ={

     }

  	 this.mdindex = [
      {'color':'#188a34', 'label': 'Very Happy', 'icon': 'smile-beam', 'modindex': 33.2},
      {'color':'#90b240', 'label': 'Happy', 'icon': 'smile', 'modindex': 50.5},
      {'color':'#ffc02e', 'label': 'Just Ok', 'icon': 'meh', 'modindex': 14.3},
      {'color':'#ff622b', 'label': 'Sad', 'icon': 'frown-open', 'modindex': 0.7},
      {'color':'#d94334', 'label': 'Very Sad', 'icon': 'frown', 'modindex': 1.3}
    ];
    this.adapq = {
    	"overall_adq_score": 56.5
    }
  	this.adaptqdash = {
  		"executiveSummary": "<p> Adaptability quotient is an individuals ability to change. The view here give multiple perspectives on how to make the whole bank adaptive to change. The range for our individual adaptability is between 0 to 25 and the overall organization adaptability is calculated through the weighted average method.</p>",
  		"receptiveness": "Receptiveness to change",
  		 "opinion_index": 3.76

    }

    this.a1p= 10;
    this.a2p= 10;
    this.a3p= 10;
    this.a4p= 10;
    this.t1p= 10;
    this.t2p= 10;
    this.t3p= 10;
    this.t4p= 10;
    this.g1p= 10;
    this.g2p= 10;


    this.a1a= 10;
    this.a2a= 10;
    this.a3a= 10;
    this.a4a= 10;
    this.t1a= 10;
    this.t2a= 10;
    this.t3a= 10;
    this.t4a= 10;
    this.g1a= 10;
    this.g2a= 10;


   this.a1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ |casdfadasdad |adsadasda |asdada';
   this.a1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';


   this.t1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

   this.g1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';







   }



   addAnimation(body) {


    if (!this.dynamicStyles) {
      this.dynamicStyles = document.createElement('style');
     this.dynamicStyles.type = 'text/css';
      document.head.appendChild(this.dynamicStyles);
    }

    this.dynamicStyles.sheet.insertRule(body, this.dynamicStyles.length);
  }


   changelocation(event: any){




    this.selectedLocation = event.target.value;
     if(this.selectedLocation == 'l1'){
      this.nps = 54.1;
      this.mdindex = [
        {'color':'#188a34', 'label': 'Very Happy', 'icon': 'smile-beam', 'modindex': 33.2},
        {'color':'#90b240', 'label': 'Happy', 'icon': 'smile', 'modindex': 50.5},
        {'color':'#ffc02e', 'label': 'Just Ok', 'icon': 'meh', 'modindex': 14.3},
        {'color':'#ff622b', 'label': 'Sad', 'icon': 'frown-open', 'modindex': 0.7},
        {'color':'#d94334', 'label': 'Very Sad', 'icon': 'frown', 'modindex': 1.3}
      ];
      this.adapq = {
        "overall_adq_score": 56.5
      }

      this.adaptqdash = {

         "opinion_index": 3.76

      }
      
      this.highlyAdaptive=2.6;
      this.moreAdaptive=72.3;
      this.neutral=24.8; 
      this.lessAdaptive=0.3;
      this.notAdaptive= 0.0;
      this.highlyAdaptiveCount=8;
      this.moreAdaptiveCount=222;
      this.neutralCount=76;
      this.lessAdaptiveCount=1;
      this.notAdaptiveCount=0;



     this.a1p= 10;
    this.a2p= 10;
    this.a3p= 10;
    this.a4p= 10;
    this.t1p= 10;
    this.t2p= 10;
    this.t3p= 10;
    this.t4p= 10;
    this.g1p= 10;
    this.g2p= 10;


     this.a1a= 10;
     this.a2a= 10;
     this.a3a= 10;
     this.a4a= 10;
     this.t1a= 10;
     this.t2a= 10;
     this.t3a= 10;
     this.t4a= 10;
     this.g1a= 10;
     this.g2a= 10;



   this.a1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';


   this.t1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

   this.g1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

  }

  if(this.selectedLocation == 'l2'){

    this.nps = 40;
    this.mdindex = [
      {'color':'#188a34', 'label': 'Very Happy', 'icon': 'smile-beam', 'modindex': 0},
      {'color':'#90b240', 'label': 'Happy', 'icon': 'smile', 'modindex': 50},
      {'color':'#ffc02e', 'label': 'Just Ok', 'icon': 'meh', 'modindex': 50},
      {'color':'#ff622b', 'label': 'Sad', 'icon': 'frown-open', 'modindex': 0},
      {'color':'#d94334', 'label': 'Very Sad', 'icon': 'frown', 'modindex': 0}
    ];
    this.adapq = {
      "overall_adq_score": 49.2
    }

    this.adaptqdash = {

      "opinion_index": 3.34

   }
   this.highlyAdaptive=0;
   this.moreAdaptive=80;
   this.neutral=20;
   this.lessAdaptive=0;
   this.notAdaptive= 0;
   
   this.highlyAdaptiveCount=0;
   this.moreAdaptiveCount=8;
   this.neutralCount=2;
   this.lessAdaptiveCount=0;
   this.notAdaptiveCount=0;

     this.a1p= 20;
     this.a2p= 20;
     this.a3p= 20;
     this.a4p= 20;
     this.t1p= 20;
     this.t2p= 20;
     this.t3p= 20;
     this.t4p= 20;
     this.g1p= 20;
     this.g2p= 20;

     this.a1a= 20;
     this.a2a= 20;
     this.a3a= 20;
     this.a4a= 20;
     this.t1a= 20;
     this.t2a= 20;
     this.t3a= 20;
     this.t4a= 20;
     this.g1a= 20;
     this.g2a= 20;



   this.a1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';


   this.t1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

   this.g1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

}


if(this.selectedLocation == 'l3'){
  this.nps = 48.3;
  this.mdindex = [
    {'color':'#188a34', 'label': 'Very Happy', 'icon': 'smile-beam', 'modindex': 22},
    {'color':'#90b240', 'label': 'Happy', 'icon': 'smile', 'modindex': 61},
    {'color':'#ffc02e', 'label': 'Just Ok', 'icon': 'meh', 'modindex': 15.3},
    {'color':'#ff622b', 'label': 'Sad', 'icon': 'frown-open', 'modindex': 1.7},
    {'color':'#d94334', 'label': 'Very Sad', 'icon': 'frown', 'modindex': 0}
  ];
  this.adapq = {
    "overall_adq_score": 57.2
  }


  this.adaptqdash = {

    "opinion_index": 3.72

 }
 this.highlyAdaptive=1.7;
 this.moreAdaptive=90;
 this.neutral=8.3;
 this.lessAdaptive=0;
 this.notAdaptive=0;
 
 this.highlyAdaptiveCount=1;
 this.moreAdaptiveCount=54;
 this.neutralCount=5;
 this.lessAdaptiveCount=0;
 this.notAdaptiveCount=0;

  this.a1p= 30;
     this.a2p= 30;
     this.a3p= 30;
     this.a4p= 30;
     this.t1p= 30;
     this.t2p= 30;
     this.t3p= 30;
     this.t4p= 30;
     this.g1p= 30;
     this.g2p= 30;

     this.a1a= 30;
     this.a2a= 30;
     this.a3a= 30;
     this.a4a= 30;
     this.t1a= 30;
     this.t2a= 30;
     this.t3a= 30;
     this.t4a= 30;
     this.g1a= 30;
     this.g2a= 30;


   this.a1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.a4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';


   this.t1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.t4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

   this.g1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
   this.g2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

}

if(this.selectedLocation == 'l4'){
  this.nps = 55.7;
  this.mdindex = [
    {'color':'#188a34', 'label': 'Very Happy', 'icon': 'smile-beam', 'modindex': 37.5},
    {'color':'#90b240', 'label': 'Happy', 'icon': 'smile', 'modindex': 47.8},
    {'color':'#ffc02e', 'label': 'Just Ok', 'icon': 'meh', 'modindex': 12.5},
    {'color':'#ff622b', 'label': 'Sad', 'icon': 'frown-open', 'modindex': 0.4},
    {'color':'#d94334', 'label': 'Very Sad', 'icon': 'frown', 'modindex': 1.7}
  ];
  this.adapq = {
    "overall_adq_score": 56.7
  }

  this.adaptqdash = {

    "opinion_index": 3.79

 }
 this.highlyAdaptive=3;
 this.moreAdaptive=67.5;
 this.neutral=29.1;
 this.lessAdaptive=0.4;
 this.notAdaptive=0;
 
 this.highlyAdaptiveCount=7;
 this.moreAdaptiveCount=160;
 this.neutralCount=69;
 this.lessAdaptiveCount=1;
 this.notAdaptiveCount=0;


  this.a1p= 40;
  this.a2p= 40;
  this.a3p= 40;
  this.a4p= 40;
  this.t1p= 40;
  this.t2p= 40;
  this.t3p= 40;
  this.t4p= 40;
  this.g1p= 40;
  this.g2p= 40;

  this.a1a= 40;
  this.a2a= 40;
  this.a3a= 40;
  this.a4a= 40;
  this.t1a= 40;
  this.t2a= 40;
  this.t3a= 40;
  this.t4a= 40;
  this.g1a= 40;
  this.g2a= 40;


  this.a1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.a1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.a2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.a2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.a3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.a3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.a4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.a4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';


  this.t1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.t1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.t2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.t2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.t3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.t3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.t4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.t4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

  this.g1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.g1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.g2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
  this.g2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
}

if(this.selectedLocation == 'l5'){
  this.nps = 10.6;
  this.mdindex = [
    {'color':'#188a34', 'label': 'Very Happy', 'icon': 'smile-beam', 'modindex': 21},
    {'color':'#90b240', 'label': 'Happy', 'icon': 'smile', 'modindex': 52},
    {'color':'#ffc02e', 'label': 'Just Ok', 'icon': 'meh', 'modindex': 23},
    {'color':'#ff622b', 'label': 'Sad', 'icon': 'frown-open', 'modindex': 4},
    {'color':'#d94334', 'label': 'Very Sad', 'icon': 'frown', 'modindex': 1}
  ];
  this.adapq = {
    "overall_adq_score": 80
  }
  this.highlyAdaptive=32.13;
  this.moreAdaptive=44.44;
  this.neutral=19.52;
  this.lessAdaptive=3.9;
  this.notAdaptive=0.0;
  
  this.highlyAdaptiveCount=107;
  this.moreAdaptiveCount=148;
  this.neutralCount=65;
  this.lessAdaptiveCount=13;
  this.notAdaptiveCount=0;

  this.adaptqdash = {

    "opinion_index": 3.96

 }


 this.a1p= 50;
 this.a2p= 50;
 this.a3p= 50;
 this.a4p= 50;
 this.t1p= 50;
 this.t2p= 50;
 this.t3p= 50;
 this.t4p= 50;
 this.g1p= 50;
 this.g2p= 50;

 this.a1a= 50;
 this.a2a= 50;
 this.a3a= 50;
 this.a4a= 50;
 this.t1a= 50;
 this.t2a= 50;
 this.t3a= 50;
 this.t4a= 50;
 this.g1a= 50;
 this.g2a= 50;



 this.a1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.a1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.a2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.a2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.a3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.a3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.a4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.a4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';


 this.t1pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.t1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.t2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.t2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.t3pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.t3lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.t4pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.t4lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

 this.g1pv = '| kdjhaqjhd | jhSADJ';
 this.g1lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.g2pv = 'ajhsdhja | kdjhaqjhd | jhSADJ';
 this.g2lv = 'ajhsdhja | kdjhaqjhd | jhSADJ';

}

// this.addAnimation(`
// @keyframes loop {
//   0% { transform: rotate(${(this.adaptqdash.opinion_index*12)-22}deg);}
//   100% { transform: rotate(${(this.adaptqdash.opinion_index*12)-28}deg); }
// `);

  }




  ngOnInit() {
  }

}

