import { Component, OnInit, Input } from '@angular/core';

import { Project } from '../../models/project.model';

declare var ldBar: any;
@Component({
  selector: 'app-peoplespeakculture',
  templateUrl: './peoplespeakculture.component.html',
  styleUrls: ['./peoplespeakculture.component.css']
})
export class PeoplespeakcultureComponent implements OnInit {
  @Input() project: Project;
  public rcs: any;
  public rc_work_life: number;
  constructor() {

    this.rc_work_life = 3.2;

  	 this.rcs = [

      {'code': 'rcwl2', 'prop': 3.83, 'label': 'Work / Life Balance', 'icon': 'balance-scale'},
      {'code': 'rcoe2', 'prop': 3.65, 'label': 'Ogranizational Experience', 'icon': 'sitemap'},
      {'code': 'rcsb2', 'prop': 3.42, 'label': 'Salary / Benefits', 'icon': 'wallet'},
      {'code': 'rcjc', 'prop': 3.43, 'label': 'Job Security', 'icon': 'user-shield'},
      {'code': 'rcmn', 'prop': 3.55, 'label': 'Management', 'icon': 'tasks'}
    ]

   }



 showKnob(el, prop) {

    var bar = new ldBar('#' + el);
    bar.set(prop*20);
  }

  hideKnob(el, prop) {
    var bar = new ldBar('#' + el);
    bar.set(0);
  }

  initStuff(prm1){
    this.project = prm1;
    let self = this;
    setTimeout(function() {
        for(let rc of self.rcs) {
           self.hideKnob(rc.code, rc.prop);
          self.showKnob(rc.code, rc.prop);
        }

    }, 1000);
  }
  ngOnInit() {
    this.project.pskc = {
      "executiveSummary": "People Speak is a comprehensive report telling your organizations's story with actionable insights taking into account factors that offer more value than internal employee engagement surveys.",
      "opinion_index": 3.33,
      "review": "2026",
      "interview":"72",
      "ceo_approval":"90",
      "friend_recommend":"73",
      "rc_work_life_balance1": 3.53,
      "rc_org_experience1": 3.02,
      "rc_salary_benefits1": 3.98,
      "rc_job_security": 4.42,
      "rc_management": 3.88
    };



  }

}

