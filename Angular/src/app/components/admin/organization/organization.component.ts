import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';  
import { Observable } from 'rxjs'; 
import Swal from 'sweetalert2';

import { DataService } from '../../../services/data.service';
import { ExcelServicesService } from '../../../services/excel-services.service'; 
@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {
	 @BlockUI() blockUI: NgBlockUI;

	public orgTab: any;
	public allorganizations: any;
	public activeorg: any;
	public orgrequest: any;
  

  constructor(
  	 	private dataService: DataService,
        private title: Title,
        private route: ActivatedRoute,
        private excelService:ExcelServicesService,
        private http: HttpClient
  	) { 
  	this.orgTab = 'allOrg';
  }

  ngOnInit() {
    this.dataService.getOrganization().subscribe(
            data => { 
            	this.allorganizations = data;
              // console.log(data);
              this.allorganization();
              this.reqorganization();
              this.blockUI.stop();
            },
            err => {
              console.log("inside projects list error", JSON.stringify(err.message));
              alert(JSON.stringify(err.message));
              this.blockUI.stop();
        });
    }
  
  switchOrgTab(tab) {
    this.orgTab = tab;
    console.log(this.orgTab);
  }

  approveRequest(org_info) {
    // alert("Test");
    let contact_person_details = {
      id: org_info.contact_person_id,
      first_name: org_info.first_name,
      last_name: org_info.last_name,
      designation: org_info.designation,
      official_email: org_info.official_email,
      contact_no: org_info.contact_no,
      org_id: org_info.id
    };
    // console.log(this.contact_person_details);

    this.dataService.approveRegistrationRequest(contact_person_details).subscribe(
      data => {
        console.log(data);
        if(data.code === 200) {
          if(data.message === 'Success') { 
            console.log('done');
            this.allorganization();
            console.log(this.allorganization());
            this.reqorganization();
            console.log(this.reqorganization());

            Swal.fire('Approved!','Registration request approved successfully!', 'success' );
            // document.getElementById("closemodal").click();
          }
          else if(data.message === 'Failed approve registration request, try again later.') { console.log('here'); }                    
        }
        else {
          console.log('error');
        }
        this.blockUI.stop();
      },
      err => {
        console.log("Inside Registration Request Approval error", JSON.stringify(err.message));
        this.blockUI.stop();
      }
      );
  }

  allorganization(){
    this.activeorg = this.allorganizations.orgInfo.filter(q => q.status === '1');
  }
  reqorganization(){
    this.orgrequest = this.allorganizations.orgInfo.filter(o => o.status === '2');
  }

}
