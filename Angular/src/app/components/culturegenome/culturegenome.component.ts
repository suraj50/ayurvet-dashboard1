import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ActivatedRoute } from '@angular/router';

import { PeoplespeakcultureComponent } from '../peoplespeakculture/peoplespeakculture.component';
import { LeadershippulsecultureComponent } from '../leadershippulseculture/leadershippulseculture.component';
import { ValueassessmentComponent } from '../valueassessment/valueassessment.component';
import { ValueassessmentempComponent} from '../valueassessmentemp/valueassessmentemp.component';
import { ValueassessmentcultureComponent } from '../valueassessmentculture/valueassessmentculture.component';

import { CompsentimentcultureComponent } from '../compsentimentculture/compsentimentculture.component';

import { AdaptqcultureComponent } from '../adaptqculture/adaptqculture.component';

import { InpComponent } from '../inp/inp.component';

import  { BnscultureComponent } from '../bnsculture/bnsculture.component';

import { DataService } from '../../services/data.service';
import { AuthenticationService } from '../../services/authentication.service';

import { User } from '../../models/user.model';
import { AgmCoreModule } from '@agm/core';

import { from } from 'rxjs';
import { pluck, groupBy } from 'rxjs/operators';
import Swal from 'sweetalert2';


declare var ldBar: any;
declare var $: any;
declare var require: any;

var Highcharts = require('highcharts/highmaps.js'),
Highcharts1 = require('highcharts/highcharts.js'),
mapGeoInJSON = require('@highcharts/map-collection/countries/in/in-all.geo.json'),
mapGeoJSON = require('@highcharts/map-collection/countries/in/in-all.geo.json');
$.support.touch = 'ontouchend' in document;
@Component({
    selector: 'app-culturegenome',
    templateUrl: './culturegenome.component.html',
    styleUrls: ['./culturegenome.component.css']
})
export class CulturegenomeComponent implements OnInit {
    @ViewChild('divClick') divClick: ElementRef;

    @ViewChild('pskcComponent') pskcComponent:PeoplespeakcultureComponent;
    @ViewChild('lspComponent') lspComponent:LeadershippulsecultureComponent;
    @ViewChild('cvaComponent') cvaComponent:ValueassessmentComponent;
    @ViewChild('evaComponent') evaComponent:ValueassessmentempComponent;
    @ViewChild('cvacComponent') cvacComponent:ValueassessmentcultureComponent;
    @ViewChild('inpComponent') inpComponent:InpComponent;
    @ViewChild('bnsComponent') bnsComponent:BnscultureComponent;
    @ViewChild('cstcComponent') cstcComponent:CompsentimentcultureComponent;
    @ViewChild('adqcComponent') adqcComponent:AdaptqcultureComponent;

    @ViewChild('myDiv') myDiv: ElementRef<HTMLElement>;


    @BlockUI() blockUI: NgBlockUI;
    public projectId: any;
    public project: any;
    public org: any;
    public currentUser: User;
    public currentDash: any;
    public slideSMConfig: any;
    public slideNUConfig: any;
    public kpi_titles: any;
    public kpi_short_titles: any;
    public projObj: any;

    public ivop: any;
    public ivop1: any;
    public ivop2: any;
    public ivop3: any;
    public ivls: any;
    public ivls1: any;
    public ivls_op1:any;
    public ivls_op2: any;
    public ivls_op3:any;
    public knobConfig: any;
    public knobValues: any;
    public data1: any;
    public ccqscrore: any;
    public custEmpTab: any;
    public categoriesTab: any;
    public selectedCategoryTab: any;
    public selectedCustCategoryTab: any;
    public empcategoriesTab: any;
    public cvls: any;
    public empcvls: any;
    public filter_array: any;
    public chartdata: any;
    public allchartresponse: any;
    public cust_dropdn: any;
    public emp_dropdn: any;
    public empdem1: any;
    public empdem2: any;
    public selectedNetwork: any;
    public selectedModule: any;
    public selectedBranch: any;
    public selectedCategoryDropDown1: any;
    public selectedCategoryDropDown2: any;
    public selectedCustCategoryDropDown1: any;
    public selectedCustCategoryDropDown2: any;
    public showmianscreen: boolean;
    public countrydata: any;
    public currentView: any;
    public LEmployees: any;
    public LDepartments: any;
    public LDesignations: any;
    public r1a: any;
    public r1b: any;
    public r2a: any;
    public r2b: any;
    public r3a: any;
    public r3b: any;
    constructor(
        private dataService: DataService,
        private route: ActivatedRoute,
        private authService: AuthenticationService,
        element: ElementRef
        ) {
        this.r1a="4 years 11 months";
        this.r1b="2 years 6 months";
        this.r2a="28 years 9 months";
        this.r2b="29 years 3 months";
        this.r3a="33 years 8 months";
        this.r3b="31 years 9 months";
        // console.log(element.nativeElement);
        this.LEmployees= "330";
        this.LDepartments = "92.4 %";
        this.LDesignations ="10";
        this.currentView ="Overall";
        this.projectId = this.route.snapshot.params.projectId;
        this.blockUI.start('Loading...'); // Start blocking
        this.currentDash = '';
        this.kpi_titles = {
            'psk': 'People Speak',
            'pskc': 'People Speak',
            'lsp': 'Leadership Pulse',
            'cvac': 'Value Assessment',
            // 'eva': 'Value Assessment (Employee)',
            'adqc': 'Adaptability Quotient',
            'cstc': 'Influencer Networks',
            'inp': 'INpressions',
            'bns': 'Culture Needs Scorecard'
        }
        this.kpi_short_titles = {
            'psk': 'PS',
            'lsp': 'LP',
            'cva': 'VA (Customers)',
            'eva': 'VA (Employee)',
            'adq': 'ADQ',
            'cst': 'CS',
            'inp': 'IP',
            'bns': 'CNS'
        }
        this.slideSMConfig = {
            centerMode:true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            autoplay: true,
            mobileFirst:true,
            speed: 800,
            autoplaySpeed: 3000,
            infinite: true,
            cssEase: 'linear',
            variableWidth: true,
            variableHeight: true

        };
        this.slideNUConfig = {
            centerMode:true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            autoplay:true,
            mobileFirst:true,
            speed: 800,
            autoplaySpeed: 3000,
            infinite: true,
            cssEase: 'linear',
            variableWidth: true,
            variableHeight: true

        };
        this.projObj = {

        }
        this.cvls = [

        ];

        this.empcvls = [

        ]
        this.ivls_op1 = [
        {'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [] },
        {'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [] },
        {'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Honesty: 360', 'state':'PV'},{'label': 'Humour/Fun: 297', 'state': 'PV'},{'label': 'Integrity: 267', 'state':'PV'},{'label': 'Commitment: 225', 'state':'PV'},{'label': 'Positivity: 201', 'state':'PV'}] },
        {'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Accountability: 249', 'state':'PV'},{'label': 'Balance (Home/Work): 201', 'state':'PV'}] },
        {'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [] },
        {'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Respect: 249', 'state':'PV'},{'label': 'Caring: 231', 'state':'PV'},{'label': 'Family: 213', 'state':'PV'}] },
        {'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [] }
        ];
        this.ivls_op2 = [
        {'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [] },
        {'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Making a difference: 309', 'state':'PV'}] },
        {'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [{'label': 'Commitment: 174', 'state':'PV'}] },
        {'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Continuous improvement: 264', 'state': 'PV'},{'label': 'Teamwork: 222', 'state':'PV'},{'label': 'Accountability: 204', 'state': 'PV'}] },
        {'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Brand: 231', 'state':'PV'},{'label': 'Bureaucray: 216', 'state': 'LV'},{'label': 'Hierarchy(L): 159', 'state':'LV'}] },
        {'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Client Satisfaction: 378', 'state': 'PV'},{'label': 'Caring: 162', 'state':'PV'}] },
        {'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [] }
        ];
        this.ivls_op3 = [
        {'id': 1, 'level': 7, 'label': 'Service', 'dlabel': 'Serving', 'bubbles': [] },
        {'id': 2, 'level': 6, 'label': 'Making a difference', 'dlabel': 'Integrating', 'bubbles': [{'label': 'Employee Fullfilment: 159', 'state':'PV'}] },
        {'id': 3, 'level': 5, 'label': 'Internal cohesion', 'dlabel': 'Self-actualizing', 'bubbles': [] },
        {'id': 4, 'level': 4, 'label': 'Transformation', 'dlabel': 'Self-development', 'bubbles': [{'label': 'Accountability: 408', 'state':'PV'},{'label': 'Continuous improvement: 336', 'state':'PV'},{'label': 'Teamwork: 228', 'state':'PV'},{'label': 'Adaptability: 210', 'state':'PV'},{'label': 'Information sharing: 165', 'state':'PV'}] },
        {'id': 5, 'level': 3, 'label': 'Self-respect', 'dlabel': 'Differentiating', 'bubbles': [{'label': 'Professionalism: 189', 'state':'PV'}] },
        {'id': 6, 'level': 2, 'label': 'Relationships', 'dlabel': 'Conforming', 'bubbles': [{'label': 'Client Satisfaction: 447', 'state': 'PV'},{'label': 'open communication: 210', 'state':'PV'},{'label': 'Respect: 180', 'state': 'PV'}] },
        {'id': 7, 'level': 1, 'label': 'Survive', 'dlabel': 'Surviving', 'bubbles': [] }
        ];
        this.ivop = 'op1';
        this.ivop1 = 'op4';
        this.ivop2 = 'op7';
        this.ivop3 = 'op10';
        this.empdem1 = "op16";
        this.empdem2 = "op13";
        this.ivls = this.ivls_op1;
        this.ivls1 = this.ivls_op2;
        this.showmianscreen = true;



        this.data1 = [
        ['in-ka', 500],
        ['in-ap', 27],
        ['in-tn', 7]
        ];

    }

    openTab(kpi, cps) {
        this.currentDash = kpi;
        this.projObj.currentTab = kpi;
        this.showmianscreen = false;

        if(kpi === 'pskc') {
            setTimeout(()=>{
                this.pskcComponent.initStuff(this.projObj);
            },1000);
        } else if(kpi === 'lsp') {
            setTimeout(()=>{
                this.lspComponent.initStuff(this.projObj);
            },500);
        } else if(kpi === 'inp') {
            setTimeout(()=>{
                this.inpComponent.initStuff();
            },500);
        }
        else if(kpi === 'bns') {
            setTimeout(()=>{
                this.bnsComponent.initStuff(this.projObj);
            },500);
        } else if(kpi === 'cvac') {
            setTimeout(()=>{
                this.cvacComponent.initStuff();
            },500);
        }
    }

    openTabMob(kpi) {
        this.currentDash = kpi;
        this.projObj.currentTab = kpi;
        this.showmianscreen = false;

        if(kpi === 'pskc') {
            setTimeout(()=>{
                this.pskcComponent.initStuff(this.projObj);
            },1000);
        } else if(kpi === 'lsp') {
            setTimeout(()=>{
                this.lspComponent.initStuff(this.projObj);
            },500);
        } else if(kpi === 'inp') {
            setTimeout(()=>{
                this.inpComponent.initStuff();
            },500);
        }
        else if(kpi === 'bns') {
            setTimeout(()=>{
                this.bnsComponent.initStuff(this.projObj);
            },500);
        } else if(kpi === 'cvac') {
            setTimeout(()=>{
                this.cvacComponent.initStuff();
            },500);
        }
    }


    openDashTab(kpi, cps) {
        this.currentDash = kpi;
        this.projObj.currentTab = kpi;
        this.showmianscreen = false;
        if(kpi === 'pskc') {
            console.log(this.pskcComponent);
            this.pskcComponent.initStuff(this.projObj);
        }
        else if(kpi === 'lsp') {
            this.lspComponent.initStuff(this.projObj);
        }
        else if(kpi === 'inp') {
            this.inpComponent.initStuff();
        }
        else if(kpi === 'bns') {
            this.bnsComponent.initStuff(this.projObj);
        } else if(kpi === 'cvac') {
            setTimeout(()=>{
                this.cvacComponent.initStuff();
            },500);
        }
        // else if(kpi === 'cstc') {
            //     this.cvacComponent.initStuff();
            // }
        }
startload(){
        this.blockUI.start('Loading...');
}
        showmianscreenfunc(){
            this.showmianscreen = true;
            console.log(this.showmianscreen);
        }
        ivopChange(opval){
            this.ivop = opval;
        }
        ivopCustChange(opval1){
            this.ivop1 = opval1;
            // if(this.ivls !== undefined){
                //     if(opval == 'op1')this.ivls = this.ivls_op1;

                //     if(opval == 'op2')this.ivls = this.ivls_op2;

                //     if(opval == 'op3')this.ivls = this.ivls_op3;
                // }
            }
            ivopCustInsChange1(opval2){
                this.ivop2 = opval2;
            }
            switchView(view) {
              this.currentView = view;

              if(this.currentView == 'Overall'){
                this.generateGenderChart1();
                this.generateDesignationChart1();
                this.generateAssociateBankChart1();
                this.LEmployees= "330";
                this.LDepartments = "92.4 %";
                this.LDesignations ="10";
                this.r1a="4 years 11 months";
                this.r1b="2 years 6 months";
                this.r2a="28 years 9 months";
                this.r2b="29 years 3 months";
                this.r3a="33 years 8 months";
                this.r3b="31 years 9 months";
              }

              if(this.currentView == 'Angul'){
                this.generateGenderChart2();
                this.generateDesignationChart2();
                this.generateAssociateBankChart2();
                this.LEmployees= "249";
                this.LDepartments= "93.2 %";
                this.LDesignations= "20";
                this.r1a="7 years 2 months";
                this.r1b="7 years 6 months";
                this.r2a="31 years";
                this.r2b="29 years 6 months";
                this.r3a="38 years 2 months";
                this.r3b="37 years";
              }

              if(this.currentView == 'Raigarh'){
                this.generateGenderChart3();
                this.generateDesignationChart3();
                this.generateAssociateBankChart3();
                this.LEmployees= "64";
                this.LDepartments= "90.1 %";
                this.LDesignations= "30";
                this.r1a="13 years 4 months";
                this.r1b="13 years 3 months";
                this.r2a="28 years 7 months";
                this.r2b="27 years 1 month";
                this.r3a="41 years 10 months";
                this.r3b="41 years 7 months";
              }

              if(this.currentView == 'Oman'){
                this.generateGenderChart4();
                this.generateDesignationChart4();
                this.generateAssociateBankChart4();
                this.LEmployees= "10";
                this.LDepartments= "100 %";
                this.LDesignations= "40";
                this.r1a="8 years 7 months";
                this.r1b="8 years 10 months";
                this.r2a="30 years 10 months";
                this.r2b="29 years 2 months";
                this.r3a="39 years 5 months";
                this.r3b="37 years 8 months";
              }

              if(this.currentView == 'Corporate Centre'){
                this.generateGenderChart5();
                this.generateDesignationChart5();
                this.generateAssociateBankChart5();
                this.LEmployees= "471";
                this.LDepartments= "70.9 %";
                this.LDesignations= "50";
                this.r1a="8 years 5 months";
                this.r1b="8 years 4 months";
                this.r2a="32 years";
                this.r2b="30 years";
                this.r3a="40 years";
                this.r3b="30 years";
              }



            }

            ivopCustInsChange2(opval3){
                this.ivop3 = opval3;
            }

            ivopEmpInsChange1(el){
                this.empdem1 = el;
            }

            ivopEmpInsChange2(el){
                this.empdem2 = el;
            }

            showKnob(ccqscrore) {
                // console.log(ccqscrore);
                // var bar = new ldBar('#ccq');
                // bar.set(ccqscrore);
            }
            switchcustEmpTab (view) {
                // console.log(view);
                this.custEmpTab = view;
            }
            switchCategoriesTab (view) {
                // console.log(view);
                this.categoriesTab = view;
                if(view == 'networktab') {
                    this.cust_dropdn = this.filter_array.network;
                    this.selectedCategoryTab = 'region';
                    this.selectedCategoryDropDown1 = 'all';
                    this.selectedCategoryDropDown2 = 'all';
                    this.getFilterDetails('empdemo');
                }
                if(view == 'agegrouptab') {
                    this.cust_dropdn = this.filter_array.age;
                    this.selectedCategoryTab = 'age';
                    this.selectedCategoryDropDown1 = 'all';
                    this.selectedCategoryDropDown2 = 'all';
                    this.getFilterDetails('empdemo');
                }
                if(view == 'durationtab') {
                    this.cust_dropdn = this.filter_array.duration_at_sbi;
                    this.selectedCategoryTab = 'duration_at_sbi';
                    this.selectedCategoryDropDown1 = 'all';
                    this.selectedCategoryDropDown2 = 'all';
                    this.getFilterDetails('empdemo');
                }
                if(view == 'joiningagetab') {
                    this.cust_dropdn = this.filter_array.age_at_joining_sbi;
                    this.selectedCategoryTab = 'age_at_joining_sbi';
                    this.selectedCategoryDropDown1 = 'all';
                    this.selectedCategoryDropDown2 = 'all';
                    this.getFilterDetails('empdemo');
                }
                if(view == 'designationtab') {
                    this.cust_dropdn = this.filter_array.designation;
                    this.selectedCategoryTab = 'designation';
                    this.selectedCategoryDropDown1 = 'all';
                    this.selectedCategoryDropDown2 = 'all';
                    this.getFilterDetails('empdemo');
                }
                if(view == 'scaletab') {
                    this.cust_dropdn = this.filter_array.scale;
                    this.selectedCategoryTab = 'scale';
                    this.selectedCategoryDropDown1 = 'all';
                    this.selectedCategoryDropDown2 = 'all';
                    this.getFilterDetails('empdemo');
                }
                if(view == 'payslabtab') {
                    this.cust_dropdn = this.filter_array.pay_slab;
                    this.selectedCategoryTab = 'pay_slab';
                    this.selectedCategoryDropDown1 = 'all';
                    this.selectedCategoryDropDown2 = 'all';
                    this.getFilterDetails('empdemo');
                }
            }
            switchCustCategoriesTab (view) {
                this.empcategoriesTab = view;
                if(view == 'empbranchtab') {
                    this.emp_dropdn = this.filter_array.branch;
                    this.selectedCustCategoryTab = 'branch';
                    this.selectedCustCategoryDropDown1 = 'all';
                    this.selectedCustCategoryDropDown2 = 'all';
                    this.getFilterDetails('custdemo');
                }
                if(view == 'empmergerbanktab') {
                    this.emp_dropdn = this.filter_array.merger_bank;
                    this.selectedCustCategoryTab = 'merger_bank';
                    this.selectedCustCategoryDropDown1 = 'all';
                    this.selectedCustCategoryDropDown2 = 'all';
                    this.getFilterDetails('custdemo');
                }
                if(view == 'empdurationsbitab') {
                    this.emp_dropdn = this.filter_array.duration_of_banking;
                    this.selectedCustCategoryTab = 'duration_of_banking';
                    this.selectedCustCategoryDropDown1 = 'all';
                    this.selectedCustCategoryDropDown2 = 'all';
                    this.getFilterDetails('custdemo');
                }
            }

            getChartData(filters, container, subcontainer) {
                // console.log(subcontainer);
                let source_data = [];
                let filtered_data = [];
                let response;
                source_data =this.project.participants;
                // console.log(source_data);
                for(let data of source_data) {
                    // console.log(data);
                    var toAdd = true;
                    // console.log(data['user_type']);
                    for(let f of filters) {
                        if(f.value !== 'all' && data[f.name] !== f.value) {
                            toAdd = false;
                            break;
                            console.log('check');
                        }
                    }
                    if(toAdd == true) {
                        filtered_data.push(data);
                    }
                }

                let pv_question = this.project.survey_questions.find(q => q.question_type === 'cva' && q.question_text.toLowerCase() === subcontainer);

                if(pv_question !== undefined ) {
                    let ans_values = []; let ansv_data = [];
                    for( let emp of filtered_data){
                        let answers = this.project.survey_answers.filter(ans => ans.question_id === pv_question.id && ans.employee_id === emp.id);

                        if(answers !== undefined) {

                            // console.log(answers);
                            from(answers).pipe(pluck('answer')).subscribe(val => ansv_data.push(val));
                            for(let avd of ansv_data) {
                                let avob = ans_values.find(av => av.value === avd);
                                if(avob !== undefined) {
                                    avob.count++;
                                }
                                else {
                                    ans_values.push({'value': avd, 'count': 1});
                                }
                            }

                        }
                    }
                    ans_values.sort((a,b)=> b.count - a.count);
                    this.cvls = ans_values.slice(0, 10);

                    let i = 1;
                    let j = 1;

                    for(let cvl of this.cvls) {
                        cvl.id = i++;
                    }
                    // console.log(this.cvls);
                    response = this.cvls;
                }
                if (response !== undefined) {
                    return response;
                }
                else {
                    response = [];
                    return response;
                }
            }


            getFilterDetails(comparisontab) {
                let empfilterdetails = [];
                let empfilterdetails1 = [];
                let empfilterdetails2 = [];
                let custfilterdetails = [];
                let custfilterdetails1 = [];
                let custfilterdetails2 = [];

                if(comparisontab === 'empvscust') {
                    empfilterdetails.push({ "name" : "region", "value" : this.selectedNetwork });
                    empfilterdetails.push({ "name" : "module", "value" : this.selectedModule });
                    empfilterdetails.push({ "name" : "branch", "value" : this.selectedModule });
                    empfilterdetails.push({ "name" : "user_type", "value" : "employee" });
                    // console.log(empfilterdetails);

                    this.chartdata.a.personal = this.getChartData(empfilterdetails,"a","personal values");
                    this.chartdata.a.current = this.getChartData(empfilterdetails,"a","current culture values");
                    this.chartdata.a.desired = this.getChartData(empfilterdetails,"a","desired culture values");

                    custfilterdetails.push({ "name" : "region", "value" : this.selectedNetwork });
                    custfilterdetails.push({ "name" : "module", "value" : this.selectedModule });
                    custfilterdetails.push({ "name" : "branch", "value" : this.selectedModule });
                    custfilterdetails.push({ "name" : "user_type", "value" : "customer" });
                    // console.log(custfilterdetails);
                    this.chartdata.b.personal = this.getChartData(custfilterdetails,"b","personal values");
                    this.chartdata.b.current = this.getChartData(custfilterdetails,"b","current culture values");
                    this.chartdata.b.desired = this.getChartData(custfilterdetails,"b","desired culture values");
                }

                if(comparisontab === 'empdemo') {
                    empfilterdetails1.push({ "name" : this.selectedCategoryTab, "value" : this.selectedCategoryDropDown1 });
                    empfilterdetails1.push({ "name" : "user_type", "value" : "employee" });
                    // console.log(empfilterdetails1);

                    this.chartdata.c.personal = this.getChartData(empfilterdetails1,"c","personal values");
                    this.chartdata.c.current = this.getChartData(empfilterdetails1,"c","current culture values");
                    this.chartdata.c.desired = this.getChartData(empfilterdetails1,"c","desired culture values");

                    empfilterdetails2.push({ "name" : this.selectedCategoryTab, "value" : this.selectedCategoryDropDown2 });
                    empfilterdetails2.push({ "name" : "user_type", "value" : "employee" });
                    // console.log(empfilterdetails2);
                    this.chartdata.d.personal = this.getChartData(empfilterdetails2,"d","personal values");
                    this.chartdata.d.current = this.getChartData(empfilterdetails2,"d","current culture values");
                    this.chartdata.d.desired = this.getChartData(empfilterdetails2,"d","desired culture values");
                    // console.log(this.selectedCategoryTab);
                }

                if(comparisontab === 'custdemo') {
                    custfilterdetails1.push({ "name" : this.selectedCustCategoryTab, "value" : this.selectedCustCategoryDropDown1 });
                    custfilterdetails1.push({ "name" : "user_type", "value" : "customer" });
                    // console.log(custfilterdetails1);

                    this.chartdata.e.personal = this.getChartData(custfilterdetails1,"e","personal values");
                    this.chartdata.e.current = this.getChartData(custfilterdetails1,"e","current culture values");
                    this.chartdata.e.desired = this.getChartData(custfilterdetails1,"e","desired culture values");

                    custfilterdetails2.push({ "name" : this.selectedCustCategoryTab, "value" : this.selectedCustCategoryDropDown2 });
                    custfilterdetails2.push({ "name" : "user_type", "value" : "customer" });
                    // console.log(custfilterdetails2);
                    this.chartdata.f.personal = this.getChartData(custfilterdetails2,"f","personal values");
                    this.chartdata.f.current = this.getChartData(custfilterdetails2,"f","current culture values");
                    this.chartdata.f.desired = this.getChartData(custfilterdetails2,"f","desired culture values");
                    // console.log(this.selectedCustCategoryTab);
                }
                //
                this.allchartresponse = this.chartdata;
                console.log(this.chartdata);

            }

            convertToHumanReadable(number : any){
                let hasMinus = String(number).charAt(0) === '-' ? true:false;
                number =  String(number).charAt(0) === '-' ?
                + String(number).substring(1, number.length)  : number;
                // hundreds
                if(number <= 999){
                    number = number ;
                }
                // thousands
                else if(number >= 1000 && number <= 99999){
                    number = (number / 1000).toFixed(1) + 'K';
                }
                // Lakhs
                else if(number >= 100000 && number <= 999999){
                    number = (number / 100000).toFixed(1) + 'L';
                }
                // millions
                else if(number >= 1000000 && number <= 999999999){
                    number = (number / 1000000).toFixed(1) + 'M';
                }
                // billions
                else if(number >= 1000000000 && number <= 999999999999){
                    number = (number / 1000000000).toFixed(1) + 'B';
                }
                if(hasMinus){
                    return '-'+number;
                }else
                {
                    return number;
                }
            }
            ngAfterViewInit(){
                console.log(this.evaComponent);
            }

            ngOnInit() {


                this.currentUser = this.authService.currentUserValue;
                this.dataService.getLeaderProject(this.projectId, this.currentUser.email).subscribe(
                    data => {
                        this.project = data.project;
                        // console.log(this.project);

                        if(this.project.ces !== undefined && this.project.ces.kpis !== undefined) {
                            for(let kpi of this.project.ces.kpis) {
                                if(kpi.title === undefined || kpi.title === '') {
                                    kpi.title = this.kpi_titles[kpi.kpi_code];
                                    kpi.shortTitle = this.kpi_short_titles[kpi.kpi_code];
                                }
                            }
                        }
                        else {
                            Swal.fire('SORRY', 'Data is not available for this section!<br> Please contact to admin', 'error');
                        }
                        console.log(this.project.org.footPrints.length);
                        for(let qobj of this.project.org.footPrints) {
                            qobj.lat = parseFloat(qobj.lat);
                            qobj.lon = parseFloat(qobj.lon);
                        };


                        this.ivopCustChange(this.ivop1);
                        // this.loadIndiaMap(this.data1);
                        this.custEmpTab = 'cusVemp';
                        this.ivopChange(this.ivop);
                        this.categoriesTab = 'networktab';
                        this.selectedCategoryTab = 'region';
                        this.selectedNetwork = 'all';
                        this.selectedModule = 'all';
                        this.selectedBranch = 'all';
                        this.selectedCategoryDropDown1 = 'all';
                        this.selectedCategoryDropDown2 = 'all';
                        this.empcategoriesTab = 'empbranchtab';
                        this.selectedCustCategoryTab = 'branch';



                        this.chartdata = {
                            "a" :
                            {
                                "personal" : {},
                                "current" : {},
                                "desired" : {}
                            }
                            ,
                            "b" :
                            {
                                "personal" : {},
                                "current" : {},
                                "desired" : {}
                            }
                            ,
                            "c" :
                            {
                                "personal" : {},
                                "current" : {},
                                "desired" : {}
                            }
                            ,
                            "d" :
                            {
                                "personal" : {},
                                "current" : {},
                                "desired" : {}
                            }
                            ,
                            "e" :
                            {
                                "personal" : {},
                                "current" : {},
                                "desired" : {}
                            }
                            ,
                            "f" :
                            {
                                "personal" : {},
                                "current" : {},
                                "desired" : {}
                            }

                        };


                        this.getFilterDetails('empvscust');
                        this.getFilterDetails('custdemo');
                        this.getFilterDetails('empdemo');
                        this.generateGenderChart1();
                        this.generateDesignationChart1();
                        this.generateAssociateBankChart1();

                        this.generateCultureChart();
                        this.generategeoChart();



                        let networkdata = []; let moduledata = []; let branchdata = []; let agedata = []; let durationAtBankdata = []; let ageOfJoiningdata = []; let designationdata = []; let scaledata = []; let paySlabdata = []; let mergerBankdata = [];

                        from(this.project.participants).pipe(pluck('region')).subscribe(val => networkdata.push(val));
                        let uniquenetwork = networkdata.filter((item, i, ar) => ar.indexOf(item) === i);

                        from(this.project.participants).pipe(pluck('module')).subscribe(val => moduledata.push(val));
                        let uniquemodule = moduledata.filter((item, i, ar) => ar.indexOf(item) === i);

                        from(this.project.participants).pipe(pluck('branch_name')).subscribe(val => branchdata.push(val));
                        let uniquebranch = branchdata.filter((item, i, ar) => ar.indexOf(item) === i);

                        from(this.project.participants).pipe(pluck('age_group')).subscribe(val => agedata.push(val));
                        let uniqueage = agedata.filter((item, i, ar) => ar.indexOf(item) === i);

                        from(this.project.participants).pipe(pluck('duration_at_SBI')).subscribe(val => durationAtBankdata.push(val));
                        let uniquedurationAtBank = durationAtBankdata.filter((item, i, ar) => ar.indexOf(item) === i);

                        from(this.project.participants).pipe(pluck('designationName')).subscribe(val => designationdata.push(val));
                        let uniquedesignation = designationdata.filter((item, i, ar) => ar.indexOf(item) === i);

                        from(this.project.participants).pipe(pluck('scale')).subscribe(val => scaledata.push(val));
                        let uniquescale = scaledata.filter((item, i, ar) => ar.indexOf(item) === i);

                        from(this.project.participants).pipe(pluck('pay_slab')).subscribe(val => paySlabdata.push(val));
                        let uniquepaySlab = paySlabdata.filter((item, i, ar) => ar.indexOf(item) === i);

                        from(this.project.participants).pipe(pluck('merger_bank')).subscribe(val => mergerBankdata.push(val));
                        let uniquemergerBank = mergerBankdata.filter((item, i, ar) => ar.indexOf(item) === i);

                        from(this.project.participants).pipe(pluck('age_of_joining_SBI')).subscribe(val => ageOfJoiningdata.push(val));
                        let uniqueageOfJoining = ageOfJoiningdata.filter((item, i, ar) => ar.indexOf(item) === i);

                        this.filter_array = {
                            "user_type" : ["employee","customer"],
                            "network" : uniquenetwork,
                            "module" : uniquemodule,
                            "branch" : uniquebranch,
                            "age" : uniqueage,
                            "duration_at_sbi" : uniquedurationAtBank,
                            "age_at_joining_sbi" : uniqueageOfJoining,
                            "designation" : uniquedesignation,
                            "scale" : uniquescale,
                            "pay_slab" : uniquepaySlab,
                            "merger_bank" : uniquemergerBank
                        }


                        this.project.org.emp_count = this.convertToHumanReadable(this.project.org.emp_count);
                        // console.log(this.project.org.emp_count);
                        this.cust_dropdn = this.filter_array.network;
                        this.emp_dropdn = this.filter_array.branch;
                        this.loadMap(this.project.org.footPrints);
                        this.loadMap_mbl(this.project.org.footPrints);
                        this.showKnob(this.project.ces.ces_score);
                        //this.currentDash = this.project.ces.kpis[5].kpi_code;
                        // this.ccqscrore = this.project.ces.ces_score;
                        this.projObj.filters = this.filter_array;
                        this.projObj.id = this.project.id;
                        this.projObj.title = this.project.title;
                        this.projObj.psk = this.project.psk;
                        this.projObj.pskc = this.project.pskc;
                        this.projObj.lsp = this.project.lsp;
                        this.projObj.ilsp = this.project.ilsp;
                        this.projObj.competencies = this.project.competencies;
                        this.projObj.icompetencies = this.project.icompetencies;
                        this.projObj.participants = this.project.participants;
                        this.projObj.myparticipant = this.project.myparticipant;
                        this.projObj.surveys = this.project.surveys;
                        this.projObj.survey_questions = this.project.survey_questions;
                        this.projObj.survey_answers = this.project.survey_answers;
                        this.projObj.values = this.project.values;
                        this.projObj.lsreport = this.project.lsreport;
                        this.projObj.currentTab = '';
                        this.projObj.project_variation_type = this.project.project_variation_type;
                        this.blockUI.stop();
                    },
                    err => {
                        console.log("inside org details error", JSON.stringify(err.message));
                        // alert(JSON.stringify(err.message));
                        Swal.fire('SORRY', 'Data is not available for this project!<br> Please contact to admin', 'error');
                        this.blockUI.stop();
                    });


}
generateCultureChart() {
    var culcomp = this;

    var colors = Highcharts.getOptions().colors,
    categories = ['8.1 <br> VALUE <br>ASSESSMENT', '3.4 <br> INFLUENCER <br>NETWORKS', ' 7.1<br>PEOPLE SPEAK', '7.5 <br> ADAPTABILITY <br> QUOTIENT', '7.4 <br>LEADERSHIP <br> PULSE', '8.9 <br>INPRESSION','8 <br>CULTURE NEEDS<br> SCORECARD'],
    components =  ['pskcComponent', 'lspComponent', 'cvacComponent', 'inpComponent', 'bnsComponent', 'cstcComponent'],
    // .va-bg{
	// 	background-color: #afafaf !important;
	// }
	// .in-bg{
	// 	background-color: #6c6c6c !important;
	// }
	// .ps-bg{
	// 	background-color:#fc66ae !important; 
	// }
	// .aq-bg{
	// 	background-color: #904eec;
	// }
	// .lp-bg{
	// 	background-color: #03c4fd;
	// }
	// .im-bg{
	// 	background-color: #aeaeae;
	// }
	// .bn-bg{
	// 	background-color: #6c6c6c;
	// }
    
    
    data = [{
        y: 33.33,
        color: '#C9E30B',
        shortcode:'cvac',
        useHTML:true,
        drilldown: {
            name: 'Value Assessment ',
            categories: [''],
            data: [ 0.33],
            width:'10px',
            color: ['#C9E30B']
        }
    }, {
        y: 33.33,
        color: '#CF4400',
        shortcode:'cstc',
        drilldown: {
            name: 'Company Sentiment',
            categories: [''],
            data: [ 0.33],
            color: ['#CF4400']
        }
    }, {
        y: 33.33,
        color: '#E6C500',
        shortcode:'pskc',
        drilldown: {
            name: 'People Speak',
            categories: [''],
            data: [0.33],
            color: ['#E6C500']
        }
    }, {
        y: 33.33,
        color: '#E6C500',
        shortcode:'adqc',
        drilldown: {
            name: 'Adaptability Quotient',
            categories: [''],
            data: [0.33],
            color: ['#E6C500']
        }
    }, {
        y: 33.33,
        color: '#E6C500',
        shortcode:'lsp',
        drilldown: {
            name: 'Leadership Pulse',
            categories: [''],
            data: [ 0.33],
            color: ['#E6C500']
        }
    }, {
        y: 33.33,
        color: '#C9E30B',
        shortcode:'inp',
        drilldown: {
            name: 'Inpressions',
            categories: [''],
            data: [ 0.33],
            color: ['#C9E30B']
        }
    }, {
        y: 33.33,
        color: '#E6C500',
        shortcode:'bns',
        drilldown: {
            name: 'Culture needs scorecards',
            categories: [''],
            data: [ 0.33],
            color: ['#E6C500']
        }
    }],
    browserData = [],
    versionsData = [],
    i,
    j,
    dataLen = data.length,
    drillDataLen,
    brightness;


    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add dashboard data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color,
            shortcode:data[i].shortcode

        });

        // add dash value data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: data[i].drilldown.color[j],
                shortcode:data[i].shortcode
            });
        }
    }
    Highcharts1.chart('culture-dash', {
        chart: {
            type: 'pie',
            backgroundColor: 'transparent',
            spacingTop: -80
        },
        title: {
            text: '',
            align: 'center',
            verticalAlign: 'middle',
            y:0
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                shadow: false,
                borderWidth:2,
                borderColor: '#fff',
                dataLabels: {
                    enabled: true,
                },
                center: ['50%', '70%'],
                slicedOffset: 30,
                startAngle: -140,
                endAngle: 140,
                states: {
                    hover: true
                }
            }
        },
        tooltip: {
            enabled: false
        },
        series: [{
            name: 'Culture Genome',
            data: browserData,
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        if(this.shortcode === 'cvac'){
                            culcomp.openTab('cvac',culcomp.cvacComponent);
                        } else if(this.shortcode === 'cstc'){
                            culcomp.openTab('cstc',culcomp.cstcComponent);
                        } else if(this.shortcode === 'pskc'){
                            culcomp.openTab('pskc',culcomp.pskcComponent);
                        } else if(this.shortcode === 'adqc'){
                            culcomp.openTab('adqc',culcomp.adqcComponent);
                        } else if(this.shortcode === 'lsp'){
                            culcomp.openTab('lsp',culcomp.lspComponent);
                        } else if(this.shortcode === 'inp'){
                            culcomp.openTab('inp',culcomp.inpComponent);
                        } else if(this.shortcode === 'bns'){
                            culcomp.openTab('bns',culcomp.bnsComponent);
                        }

                    }
                }
            },
            size: '100%',
            innerSize: '58%',


            enabled: true,
            borderWidth: 15,
            borderColor: '#fff',

                dataLabels: {
                    formatter: function () {
                        return this.point.name;
                    },
                    //  Inner Dashboard Names
                    style: {
                        // fontWeight: 'bold',
                        color: 'white',
                        textShadow: false,
                        // fontVariant:'small-caps',

                        stroke:'white',
                        letterSpacing:'1',
                        lineHeight:'20'
                    },
                    useHTML:true,
                    rotation: -1,
                    distance: -65
                }
            },{
                name: 'Versions',
                data: versionsData,
                cursor: 'pointer',
                allowPointSelect: false,
                stickyTracking: false,
                point:{
                    events:{
                        click: function (event) {
                            if(this.shortcode === 'cvac'){
                                culcomp.openTab('cvac',culcomp.cvacComponent);
                            } else if(this.shortcode === 'cstc'){
                                culcomp.openTab('cstc',culcomp.cstcComponent);
                            } else if(this.shortcode === 'pskc'){
                                culcomp.openTab('pskc',culcomp.pskcComponent);
                            } else if(this.shortcode === 'adqc'){
                                culcomp.openTab('adqc',culcomp.adqcComponent);
                            } else if(this.shortcode === 'lsp'){
                                culcomp.openTab('lsp',culcomp.lspComponent);
                            } else if(this.shortcode === 'inp'){
                                culcomp.openTab('inp',culcomp.inpComponent);
                            } else if(this.shortcode === 'bns'){
                                culcomp.openTab('bns',culcomp.bnsComponent);
                            }
                        },
                        mouseOver: function() {
                            var point = this,
                            points = this.series.points;

                            // unslice sliced element(s)
                            for (var key in points) {
                                if (points[key].sliced) {
                                    points[key].slice(false);
                                }
                            }


                        }

                    }
                },
                events: {
                    mouseOut: function(event) {
                      // unslice sliced element(s)
                      for (var key in this.points) {
                        if (this.points[key].sliced) {
                          this.points[key].slice(false);
                        }
                      }
                    }
                  },
                size: '100%',
                innerSize: '60%',

                dataLabels: {
                    useHTML: true,
                    enabled: true,
                    formatter: function () {
                        return this.point.name;
                    },
                    style: {
                        color: 'white',
                        textShadow: false,
                        stroke:'white',
                        letterSpacing:'1'
                    },
                    color: '#000',
                    inside:false,
                    y: -15,
                    x:15,

                    shadow: false,
                    connectorColor: 'transparent',
                    distance: 5
                }
            }]
        });
    }

    generateGenderChart1() {
      Highcharts1.chart('malefemalechart', {
          chart: {
              type: 'pie',
              backgroundColor: 'transparent',
              // margin: [0, 0, 50, 0],
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              width: 400,
              style: {
                  fontFamily: "'segoeui', sans-serif"
              }
          },
          title: {
              text: ''
          },
          tooltip: {
              pointFormat: '<b>{point.percentage:.1f}%</b>'
          },

          'credits': { enabled: false },
          subtitle: {
              text: ''
          },
          legend: {
              enabled: true,
              align: 'center',
              verticalAlign: 'bottom',
              itemStyle: {
                  color: '#333',
                  // fontSize: 10,
                  position: 'fixed'
              },
              layout: 'horizontal',
              width: '50%',
              y: 20
          },

          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  size: 100,
                  dataLabels: {
                      enabled: true,
                      format: '<b>{point.name}</b>:<br> {point.percentage:.1f} %',
                      // format: '<b>{point.name}</b>: {point.y}',
                      style: {
                          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                      }
                  }
              }
          },

          series: [{
              minPointSize: 10,
              innerSize: '50%',
              zMin: 0,
              name: '',
              data: [{
                  name: 'Women Employees',
                  y: 17,
                  color: '#9688e0'
              },{
                  name: 'Men Employees',
                  y: 319,
                  color: '#D672AE'
              }]
          }]
      });






  }

  generateGenderChart2() {
    Highcharts1.chart('malefemalechart', {
        chart: {
            type: 'pie',
            backgroundColor: 'transparent',
            // margin: [0, 0, 50, 0],
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            width: 400,
            style: {
                fontFamily: "'segoeui', sans-serif"
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },

        'credits': { enabled: false },
        subtitle: {
            text: ''
        },
        legend: {
            enabled: true,
            align: 'center',
            verticalAlign: 'bottom',
            itemStyle: {
                color: '#333',
                // fontSize: 10,
                position: 'fixed'
            },
            layout: 'horizontal',
            width: '50%',
            y: 20
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                size: 100,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    // format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },

        series: [{
            minPointSize: 10,
            innerSize: '50%',
            zMin: 0,
            name: '',
            data: [{
                name: 'Women Employees',
                y: 8,
                color: '#9688e0'
            },{
                name: 'Men Employees',
                y: 244,
                color: '#D672AE'
            }]
        }]
    });






}

generateGenderChart3() {
  Highcharts1.chart('malefemalechart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '50%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 100,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },

      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [{
              name: 'Women Employees',
              y: 5,
              color: '#9688e0'
          },{
              name: 'Men Employees',
              y: 66,
              color: '#D672AE'
          }]
      }]
  });






}

generateGenderChart4() {
  Highcharts1.chart('malefemalechart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '50%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 100,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },

      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [{
              name: 'Women Employees',
              y: 4,
              color: '#9688e0'
          },{
              name: 'Men Employees',
              y: 9,
              color: '#D672AE'
          }]
      }]
  });






}

generateGenderChart5() {
  Highcharts1.chart('malefemalechart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '50%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 100,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },

      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [{
              name: 'Women Employees',
              y: 54,
              color: '#9688e0'
          },{
              name: 'Men Employees',
              y: 417,
              color: '#D672AE'
          }]
      }]
  });






}
generateDesignationChart1() {
    Highcharts1.chart('designationchart', {
        chart: {
            type: 'pie',
            backgroundColor: 'transparent',
            // margin: [0, 0, 50, 0],
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            width: 400,
            style: {
                fontFamily: "'segoeui', sans-serif"
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },

        'credits': { enabled: false },
        subtitle: {
            text: ''
        },
        legend: {
            enabled: true,
            align: 'center',
            verticalAlign: 'bottom',
            itemStyle: {
                color: '#333',
                // fontSize: 10,
                position: 'fixed'
            },
            layout: 'horizontal',
            width: '100%',
            y: 20
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                size: 120,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    // format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            minPointSize: 10,
            innerSize: '50%',
            zMin: 0,
            name: '',
            data: [{name: 'Area Sales Manager', y: 19, color: '#F08080'},
            {name: 'Sales Executive', y: 38, color: '#FA8072'},
            {name: 'Veterinary Sales Officer', y: 114, color: '#E9967A'},
            {name: 'Deputy Regional Manager', y: 8, color: '#FFA07A'},
            {name: 'Zonal Business Manager', y: 5, color: '#DC143C'},
            {name: 'Territory Manager', y: 24, color: '#FF0000'},
            {name: 'Senior Manager', y: 1, color: '#B22222'},
            {name: 'Regional Business Manager', y: 2, color: '#8B0000'},
            {name: 'Director-Corporate Affairs & Strategic Liaisoning', y: 1, color: '#FFC0CB'},
            {name: 'Asst. Manager-Administration', y: 1, color: '#FFB6C1'},
            {name: 'Assistant Manager - Accounts', y: 1, color: '#FF69B4'},
            {name: 'Manager-Supply Chain & Sales Admn', y: 1, color: '#FF1493'},
            {name: 'Manager - Finance & Accounts', y: 1, color: '#C71585'},
            {name: 'Manager-Sales Coordination', y: 1, color: '#DB7093'},
            {name: 'National Sales Head', y: 1, color: '#FF7F50'},
            {name: 'Dy. Manager-Finance & Accounts', y: 2, color: '#FF6347'},
            {name: 'Sr. Manager-Finance & Accounts', y: 2, color: '#FF4500'},
            {name: 'Assistant Manager-Print Media', y: 1, color: '#FF8C00'},
            {name: 'Chief Financial Officer', y: 1, color: '#FFA500'},
            {name: 'Sr. Manager-Human Resources', y: 1, color: '#FFD700'},
            {name: 'Customer Care Executive', y: 5, color: '#FFFF00'},
            {name: 'Sr. Executive-Export Documentation', y: 1, color: '#FFFFE0'},
            {name: 'Office Attendant', y: 1, color: '#FFFACD'},
            {name: 'Director-Technical Operation & Research', y: 1, color: '#FAFAD2'},
            {name: 'Assistant Manager-Graphic Designer', y: 1, color: '#FFEFD5'},
            {name: 'Chief Human Resources Officer', y: 1, color: '#FFE4B5'},
            {name: 'Company Secretary', y: 1, color: '#FFDAB9'},
            {name: 'Sr. Assistant-It', y: 1, color: '#EEE8AA'},
            {name: 'Manager-It', y: 1, color: '#F0E68C'},
            {name: 'Asst. Manager-Human Resources', y: 2, color: '#BDB76B'},
            {name: 'Assistant Manager - Commercial', y: 1, color: '#E6E6FA'},
            {name: 'Product Manager', y: 1, color: '#D8BFD8'},
            {name: 'Dgm-Marketing & Institutional Business', y: 1, color: '#DDA0DD'},
            {name: 'Deputy Manager - Exports', y: 1, color: '#EE82EE'},
            {name: 'Senior Executive - Hr', y: 1, color: '#DA70D6'},
            {name: 'Asst. Manager- Medicinal Plant', y: 1, color: '#FF00FF'},
            {name: 'Assistant Manager - Csr', y: 1, color: '#BA55D3'},
            {name: 'Sr. Executive - Accounts', y: 1, color: '#9370DB'},
            {name: 'Sr. Executive - Marketing', y: 1, color: '#8A2BE2'},
            {name: 'Assistant Manager - Product', y: 1, color: '#9400D3'},
            {name: 'Senior Manager - International Business', y: 1, color: '#9932CC'},
            {name: 'Head- Sales & Marketing (Poultry)', y: 1, color: '#8B008B'},
            {name: 'Asst. Manager- Digital Marketing', y: 1, color: '#800080'},
            {name: 'Asst. Executive Accounts', y: 1, color: '#663399'},
            {name: 'Sr. Executive- Technical & Pmt (Poultry)', y: 1, color: '#4B0082'},
            {name: 'DGM-Commercial', y: 1, color: '#7B68EE'},
            {name: 'Assistant Manager-EDP', y: 1, color: '#6A5ACD'},
            {name: 'Assistant Officer-HR & Admin.', y: 1, color: '#483D8B'},
            {name: 'Assistant Manager-Production', y: 1, color: '#ADFF2F'},
            {name: 'Assistant Manager-HR & Admin.', y: 1, color: '#7FFF00'},
            {name: 'GM-Operations', y: 1, color: '#7CFC00'},
            {name: 'Executive-QA/QC', y: 2, color: '#00FF00'},
            {name: 'Manager-Engineering', y: 1, color: '#32CD32'},
            {name: 'Executive-HR & Admin.', y: 1, color: '#98FB98'},
            {name: 'Assistant Officer-Store', y: 1, color: '#90EE90'},
            {name: 'Executive-Accounts', y: 1, color: '#00FA9A'},
            {name: 'Officer-Purchase', y: 3, color: '#00FF7F'},
            {name: 'Assistant Officer-Production', y: 2, color: '#3CB371'},
            {name: 'Sr. Scientist-R&D', y: 1, color: '#2E8B57'},
            {name: 'Sr. Assistant-Store', y: 2, color: '#228B22'},
            {name: 'Sr. Manager-Fin.& A/C', y: 1, color: '#8000'},
            {name: 'Sr. Executive-Accounts', y: 1, color: '#6400'},
            {name: 'Manager-Production', y: 1, color: '#9ACD32'},
            {name: 'Asst. Officer-QA/QC', y: 1, color: '#6B8E23'},
            {name: 'Dy. Manager-Analytical Development', y: 1, color: '#808000'},
            {name: 'Executive-Production', y: 1, color: '#556B2F'},
            {name: 'GM-QA/QC', y: 1, color: '#66CDAA'},
            {name: 'Executive-Store ', y: 1, color: '#8FBC8F'},
            {name: 'Sr. Chemist-QA/QC', y: 1, color: '#20B2AA'},
            {name: 'Assistant-Store', y: 1, color: '#008B8B'},
            {name: 'Sr. Chemist-Production', y: 2, color: '#8080'},
            {name: 'Junior Assistant-Store', y: 2, color: '#00FFFF'},
            {name: 'Sr. Chemist-QA/QC ', y: 1, color: '#E0FFFF'},
            {name: 'Scientist-Regulatory Affairs', y: 1, color: '#AFEEEE'},
            {name: 'Assistant Officer-Accounts ', y: 1, color: '#7FFFD4'},
            {name: 'Assistant-Purchase', y: 1, color: '#40E0D0'},
            {name: 'Assistant Manager-Clinical Research', y: 1, color: '#48D1CC'},
            {name: 'Sr. Assistant- HR & Admin. ', y: 1, color: '#00CED1'},
            {name: 'Assistant- Store', y: 1, color: '#5F9EA0'},
            {name: 'Chemist-QA/QC', y: 1, color: '#4682B4'},
            {name: 'Chemist-Production', y: 2, color: '#B0C4DE'},
            {name: 'Supervisor-Maintenance', y: 2, color: '#B0E0E6'},
            {name: 'Research Associate-R&D', y: 1, color: '#ADD8E6'},
            {name: 'Research Associate-Clinical Research', y: 1, color: '#87CEEB'},
            {name: 'Trainee Chemist-Production', y: 2, color: '#87CEFA'},
            {name: 'Microbiologist-QA/QC', y: 1, color: '#00BFFF'},
            {name: 'Trainee-Production', y: 1, color: '#1E90FF'},
            {name: 'Trainee Chemist-QA/QC', y: 4, color: '#6495ED'},
            {name: 'Trainee-F&D', y: 2, color: '#41690'},
            {name: 'Dy. Manager-Phytochemistry', y: 1, color: '#0000FF'},
            {name: 'Trainee-Phytochemistry', y: 1, color: '#0000CD'},
            {name: 'Officer-Store', y: 1, color: '#00008B'},
            {name: 'Asst. Supervisor-Production', y: 1, color: '#80'},
            {name: 'Sr. Technician-Production', y: 2, color: '#191970'},
            {name: 'Sr. Foreman-Electrical', y: 1, color: '#FFF8DC'},
            {name: 'Driver', y: 1, color: '#FFEBCD'},
            {name: 'Assistant Supervisor-Production', y: 1, color: '#FFE4C4'},
            {name: 'Foreman-Boiler', y: 1, color: '#FFDEAD'},
            {name: 'Sr. Fitter', y: 1, color: '#F5DEB3'},
            {name: 'Foreman-Technical', y: 2, color: '#DEB887'},
            {name: 'Technician', y: 2, color: '#D2B48C'},
            {name: 'Sr. Electrician', y: 2, color: '#BC8F8F'},
            {name: 'Foreman Boiler', y: 1, color: '#F4A460'},
            {name: 'Electrician', y: 1, color: '#DAA520'},
            {name: 'Operator-Production', y: 1, color: '#B8860B'},
            {name: 'Boiler Operator', y: 1, color: '#CD853F'},
            {name: 'Fitter', y: 1, color: '#D2691E'},
            {name: 'Trainee Operator-Production', y: 1, color: '#8B4513'}]
        }]
    });
}
generateDesignationChart2() {
  Highcharts1.chart('designationchart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '100%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 120,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [{name: 'Area Sales Manager',y: 19,color: '#F08080'},
          {name: 'Sales Executive',y: 38,color: '#FA8072'},
          {name: 'Veterinary Sales Officer',y: 114,color: '#E9967A'},
          {name: 'Deputy Regional Manager',y: 8,color: '#FFA07A'},
          {name: 'Zonal Business Manager',y: 5,color: '#DC143C'},
          {name: 'Territory Manager',y: 24,color: '#FF0000'},
          {name: 'Regional Business Manager',y: 2,color: '#B22222'},
          {name: 'Asst. Manager-Administration',y: 1,color: '#8B0000'},
          {name: 'Assistant Manager - Accounts',y: 1,color: '#FFB6C1'},
          {name: 'Manager-Supply Chain & Sales Admn',y: 1,color: '#FF69B4'},
          {name: 'Manager - Finance & Accounts',y: 1,color: '#FF1493'},
          {name: 'Manager-Sales Coordination',y: 1,color: '#C71585'},
          {name: 'National Sales Head',y: 1,color: '#DB7093'},
          {name: 'Dy. Manager-Finance & Accounts',y: 2,color: '#FF7F50'},
          {name: 'Sr. Manager-Finance & Accounts',y: 2,color: '#FF4500'},
          {name: 'Assistant Manager-Print Media',y: 1,color: '#FF8C00'},
          {name: 'Chief Financial Officer',y: 1,color: '#FFA500'},
          {name: 'Sr. Manager-Human Resources',y: 1,color: '#FFD700'},
          {name: 'Customer Care Executive',y: 5,color: '#FFFF00'},
          {name: 'Sr. Executive-Export Documentation',y: 1,color: '#FFFACD'},
          {name: 'Office Attendant',y: 1,color: '#FAFAD2'},
          {name: 'Director-Technical Operation & Research',y: 1,color: '#7FFF00'},
          {name: 'Assistant Manager-Graphic Designer',y: 1,color: '#7CFC00'},
          {name: 'Chief Human Resources Officer',y: 1,color: '#00FF00'},
          {name: 'Company Secretary',y: 1,color: '#32CD32'},
          {name: 'Sr. Assistant-It',y: 1,color: '#00FF7F'},
          {name: 'Manager-It',y: 1,color: '#3CB371'},
          {name: 'Asst. Manager-Human Resources',y: 2,color: '#2E8B57'},
          {name: 'Assistant Manager - Commercial',y: 1,color: '#228B22'},
          {name: 'Product Manager',y: 1,color: '#556B2F'},
          {name: 'Dgm-Marketing & Institutional Business',y: 1,color: '#66CDAA'},
          {name: 'Deputy Manager - Exports',y: 1,color: '#8FBC8F'},
          {name: 'Senior Executive - Hr',y: 1,color: '#20B2AA'},
          {name: 'Assistant Manager - Csr',y: 1,color: '#008B8B'},
          {name: 'Sr. Executive - Accounts',y: 1,color: '#7FFFD4'},
          {name: 'Sr. Executive - Marketing',y: 1,color: '#40E0D0'},
          {name: 'Assistant Manager - Product',y: 1,color: '#48D1CC'},
          {name: 'Senior Manager - International Business',y: 1,color: '#F5DEB3'},
          {name: 'Head- Sales & Marketing (Poultry)',y: 1,color: '#DEB887'},
          {name: 'Asst. Manager- Digital Marketing',y: 1,color: '#D2B48C'},
          {name: 'Asst. Executive Accounts',y: 1,color: '#FFFAFA'},
          {name: 'Sr. Executive- Technical & Pmt (Poultry)',y: 1,color: '#F0FFF0'}          
          
          ]
      }]
  });
}

generateDesignationChart3() {
  Highcharts1.chart('designationchart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '100%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 120,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [{name: 'DGM-Commercial',y: 1,color: '#F08080'},
          {name: 'Assistant Manager-EDP',y: 1,color: '#FA8072'},
          {name: 'Assistant Officer-HR & Admin.',y: 1,color: '#E9967A'},
          {name: 'Assistant Manager-Production',y: 1,color: '#FFA07A'},
          {name: 'Assistant Manager-HR & Admin.',y: 1,color: '#DC143C'},
          {name: 'GM-Operations',y: 1,color: '#FF0000'},
          {name: 'Executive-QA/QC',y: 2,color: '#B22222'},
          {name: 'Manager-Engineering',y: 1,color: '#8B0000'},
          {name: 'Executive-HR & Admin.',y: 1,color: '#FFB6C1'},
          {name: 'Assistant Officer-Store',y: 1,color: '#FF69B4'},
          {name: 'Executive-Accounts',y: 1,color: '#FF1493'},
          {name: 'Officer-Purchase',y: 3,color: '#C71585'},
          {name: 'Assistant Officer-Production',y: 2,color: '#DB7093'},
          {name: 'Sr. Assistant-Store',y: 2,color: '#FF7F50'},
          {name: 'Sr. Manager-Fin.& A/C',y: 1,color: '#FF4500'},
          {name: 'Sr. Executive-Accounts',y: 1,color: '#FF8C00'},
          {name: 'Manager-Production',y: 1,color: '#FFA500'},
          {name: 'Asst. Officer-QA/QC',y: 1,color: '#FFD700'},
          {name: 'Executive-Production',y: 1,color: '#FFFF00'},
          {name: 'GM-QA/QC',y: 1,color: '#FFFACD'},
          {name: 'Executive-Store ',y: 1,color: '#FAFAD2'},
          {name: 'Sr. Chemist-QA/QC',y: 1,color: '#7FFF00'},
          {name: 'Assistant-Store',y: 1,color: '#7CFC00'},
          {name: 'Sr. Chemist-Production',y: 2,color: '#00FF00'},
          {name: 'Junior Assistant-Store',y: 2,color: '#32CD32'},
          {name: 'Sr. Chemist-QA/QC ',y: 1,color: '#00FF7F'},
          {name: 'Assistant Officer-Accounts ',y: 1,color: '#3CB371'},
          {name: 'Assistant-Purchase',y: 1,color: '#2E8B57'},
          {name: 'Sr. Assistant- HR & Admin. ',y: 1,color: '#228B22'},
          {name: 'Assistant- Store',y: 1,color: '#556B2F'},
          {name: 'Chemist-QA/QC',y: 1,color: '#66CDAA'},
          {name: 'Chemist-Production',y: 2,color: '#8FBC8F'},
          {name: 'Supervisor-Maintenance',y: 2,color: '#20B2AA'},
          {name: 'Trainee Chemist-Production',y: 2,color: '#008B8B'},
          {name: 'Microbiologist-QA/QC',y: 1,color: '#7FFFD4'},
          {name: 'Trainee-Production',y: 1,color: '#40E0D0'},
          {name: 'Trainee Chemist-QA/QC',y: 4,color: '#48D1CC'},
          {name: 'Officer-Store',y: 1,color: '#F5DEB3'},
          {name: 'Asst. Supervisor-Production',y: 1,color: '#DEB887'},
          {name: 'Sr. Technician-Production',y: 2,color: '#D2B48C'},
          {name: 'Sr. Foreman-Electrical',y: 1,color: '#FFFAFA'},
          {name: 'Driver',y: 1,color: '#F0FFF0'},
          {name: 'Assistant Supervisor-Production',y: 1,color: '#F5FFFA'},
          {name: 'Foreman-Boiler',y: 1,color: '#F0FFFF'},
          {name: 'Sr. Fitter',y: 1,color: '#F0F8FF'},
          {name: 'Foreman-Technical',y: 2,color: '#DCDCDC'},
          {name: 'Technician',y: 2,color: '#D3D3D3'},
          {name: 'Sr. Electrician',y: 2,color: '#D3D3D3'},
          {name: 'Foreman Boiler',y: 1,color: '#C0C0C0'},
          {name: 'Electrician',y: 1,color: '#A9A9A9'},
          {name: 'Operator-Production',y: 1,color: '#DAA520'},
          {name: 'Boiler Operator',y: 1,color: '#B8860B'},
          {name: 'Fitter',y: 1,color: '#CD853F'},
          {name: 'Trainee Operator-Production',y: 1,color: '#D2691E'}
          ]
      }]
  });
}

generateDesignationChart4() {
  Highcharts1.chart('designationchart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '100%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 120,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [{name: 'Senior Manager',y: 1,color: '#F08080'},
          {name: 'Director-Corporate Affairs & Strategic Liaisoning',y: 1,color: '#FA8072'},
          {name: 'Asst. Manager- Medicinal Plant',y: 1,color: '#E9967A'},
          {name: 'Sr. Scientist-R&D',y: 1,color: '#FFA07A'},
          {name: 'Dy. Manager-Analytical Development',y: 1,color: '#DC143C'},
          {name: 'Scientist-Regulatory Affairs',y: 1,color: '#FF0000'},
          {name: 'Assistant Manager-Clinical Research',y: 1,color: '#B22222'},
          {name: 'Research Associate-R&D',y: 1,color: '#8B0000'},
          {name: 'Research Associate-Clinical Research',y: 1,color: '#FFB6C1'},
          {name: 'Trainee-F&D',y: 2,color: '#FF69B4'},
          {name: 'Dy. Manager-Phytochemistry',y: 1,color: '#FF1493'},
          {name: 'Trainee-Phytochemistry',y: 1,color: '#C71585'}          
          ]
      }]
  });
}

generateDesignationChart5() {
  Highcharts1.chart('designationchart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '100%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 120,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [{name: 'Asst Gen Manager',y: 70,color: '#e6194b'},
          {name: 'Manager',y: 99,color:  '#3cb44b'},
          {name: 'Dy General Manager',y: 39,color:  '#ffe119'},
          {name: 'Assistant Manager',y: 72,color:  '#4363d8'},
          {name: 'General Manager',y: 34,color:  '#f58231'},
          {name: 'Deputy Manager',y: 95,color:  '#911eb4'},
          {name: 'Asso Vice President',y: 23,color:  '#46f0f0'},
          {name: 'Vice President',y: 15,color:  '#f032e6'},
          {name: 'Exec Vice President',y: 8,color:  '#bcf60c'},
          {name: 'President',y: 8,color:  '#fabebe'},
          {name: 'Director',y: 3,color:  '#008080'},
          {name: 'Chairman',y: 1,color:  '#e6beff'},
          {name: 'Management Trainee',y: 3,color:  '#9a6324'}
          ]
      }]
  });
}


generateAssociateBankChart1() {
    Highcharts1.chart('associatebankchart', {
        chart: {
            type: 'pie',
            backgroundColor: 'transparent',
            // margin: [0, 0, 50, 0],
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            width: 400,
            style: {
                fontFamily: "'segoeui', sans-serif"
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },

        'credits': { enabled: false },
        subtitle: {
            text: ''
        },
        legend: {
            enabled: true,
            align: 'center',
            verticalAlign: 'bottom',
            itemStyle: {
                color: '#333',
                // fontSize: 10,
                position: 'fixed'
            },
            layout: 'horizontal',
            width: '100%',
            y: 20
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                size: 100,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    // format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            minPointSize: 10,
            innerSize: '50%',
            zMin: 0,
            name: '',
            data: [
                {name: 'Sales', y: 212, color: '#E32636'},
                {name: 'Commercial', y: 5, color: '#E52B50'},
                {name: 'Company Secretary', y: 1, color: '#FFBF00'},
                {name: 'CSR', y: 1, color: '#A4C639'},
                {name: 'EDP', y: 1, color: '#8DB600'},
                {name: 'Engineering             ', y: 14, color: '#FBCEB1'},
                {name: 'Executive Director', y: 1, color: '#7FFFD4'},
                {name: 'Export', y: 3, color: '#4B5320'},
                {name: 'Factory Administration', y: 1, color: '#3B444B'},
                {name: 'Finance & Accounts', y: 13, color: '#E9D66B'},
                {name: 'HR & Administration', y: 11, color: '#B2BEB5'},
                {name: 'IT', y: 2, color: '#87A96B'},
                {name: 'Logistics', y: 1, color: '#FF9966'},
                {name: 'Marketing', y: 7, color: '#6D351A'},
                {name: 'MD Office', y: 2, color: '#007FFF'},
                {name: 'Medicinal Plants', y: 1, color: '#89CFF0'},
                {name: 'PMT', y: 2, color: '#A1CAF1'},
                {name: 'Print Media', y: 2, color: '#F4C2C2'},
                {name: 'Production              ', y: 20, color: '#FFD12A'},
                {name: 'Production & R/D', y: 1, color: '#848482'},
                {name: 'Quality Assurance / Quality Control', y: 12, color: '#98777B'},
                {name: 'Research & Development', y: 10, color: '#F5F5DC'},
                {name: 'Sales Admin', y: 4, color: '#3D2B1F'},
                {name: 'Store & Warehouse', y: 9, color: '#318CE7'}
               ]
        }]
    });
}
generateAssociateBankChart2() {
  Highcharts1.chart('associatebankchart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '100%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 100,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [{name: 'Sales',y: 212,color: '#F08080'},
          {name: 'Md Office',y: 1,color: '#FA8072'},
          {name: 'Hr & Admin.',y: 6,color: '#E9967A'},
          {name: 'Finance & Accounts',y: 9,color: '#FFA07A'},
          {name: 'Logistics',y: 1,color: '#DC143C'},
          {name: 'Marketing',y: 7,color: '#FF0000'},
          {name: 'Print Media',y: 2,color: '#B22222'},
          {name: 'Sales Admin',y: 4,color: '#8B0000'},
          {name: 'Export',y: 3,color: '#FFB6C1'},
          {name: 'Production & R/D',y: 1,color: '#FF69B4'},
          {name: 'Company Secretary',y: 1,color: '#FF1493'},
          {name: 'It',y: 2,color: '#C71585'},
          {name: 'Pmt',y: 2,color: '#DB7093'},
          {name: 'Csr',y: 1,color: '#FF7F50'}]
      }]
  });
}

generateAssociateBankChart3() {
  Highcharts1.chart('associatebankchart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '100%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 100,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [
            {name: 'Finance & Accounts',y: 4,color: '#F08080'},
            {name: 'Commercial',y: 5,color: '#FA8072'},
            {name: 'EDP',y: 1,color: '#E9967A'},
            {name: 'HR & Administration',y: 5,color: '#FFA07A'},
            {name: 'Production',y: 20,color: '#DC143C'},
            {name: 'Factory Administration',y: 1,color: '#FF0000'},
            {name: 'Quality Assurance / Quality Control',y: 12,color: '#B22222'},
            {name: 'Engineering',y: 14,color: '#8B0000'},
            {name: 'Store & Warehouse',y: 9,color: '#FFB6C1'}]
      }]
  });
}

generateAssociateBankChart4() {
  Highcharts1.chart('associatebankchart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '100%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 100,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [{name: 'Md Office',y: 1,color: '#F08080'},
          {name: 'Executive Director',y: 1,color: '#FA8072'},
          {name: 'Medicinal Plants',y: 1,color: '#E9967A'},
          {name: 'Research & Development',y: 10,color: '#FFA07A'}]
      }]
  });
}
generateAssociateBankChart5() {
  Highcharts1.chart('associatebankchart', {
      chart: {
          type: 'pie',
          backgroundColor: 'transparent',
          // margin: [0, 0, 50, 0],
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          width: 400,
          style: {
              fontFamily: "'segoeui', sans-serif"
          }
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>'
      },

      'credits': { enabled: false },
      subtitle: {
          text: ''
      },
      legend: {
          enabled: true,
          align: 'center',
          verticalAlign: 'bottom',
          itemStyle: {
              color: '#333',
              // fontSize: 10,
              position: 'fixed'
          },
          layout: 'horizontal',
          width: '100%',
          y: 20
      },

      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              size: 100,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  // format: '<b>{point.name}</b>: {point.y}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          minPointSize: 10,
          innerSize: '50%',
          zMin: 0,
          name: '',
          data: [
            {name: 'Finance & Accounts',y: 52,color: '#F0F8FF'},
            {name: 'SSC - Finance & Accounts',y: 20,color: '#E32636'},
            {name: 'SSC-HR',y: 13,color: '#E52B50'},
            {name: 'Sourcing & Planning',y: 7,color: '#FFBF00'},
            {name: 'S&M - Semis',y: 1,color: '#A4C639'},
            {name: 'S&M TMT',y: 78,color: '#8DB600'},
            {name: 'SBU - Plates & Coils',y: 38,color: '#FBCEB1'},
            {name: 'S&M - Wire Rod',y: 10,color: '#7FFFD4'},
            {name: 'S&M - Structures',y: 34,color: '#4B5320'},
            {name: 'Port Office Vizag',y: 4,color: '#3B444B'},
            {name: 'Marketing Finance',y: 15,color: '#E9D66B'},
            {name: 'Chairman Secretariat',y: 8,color: '#B2BEB5'},
            {name: 'Legal',y: 6,color: '#87A96B'},
            {name: 'S&M - Rail',y: 5,color: '#FF9966'},
            {name: 'S&M - Export',y: 7,color: '#6D351A'},
            {name: 'Corporate Communication',y: 7,color: '#007FFF'},
            {name: 'Projects',y: 8,color: '#89CFF0'},
            {name: 'Port Office Paradeep',y: 5,color: '#A1CAF1'},
            {name: 'Corporate Services',y: 2,color: '#F4C2C2'},
            {name: 'Strategy & Business Development',y: 8,color: '#FFD12A'},
            {name: 'Construction Material Business',y: 4,color: '#848482'},
            {name: 'SSC-Supply Chain Management',y: 12,color: '#98777B'},
            {name: 'S&M - SSD',y: 6,color: '#F5F5DC'},
            {name: 'S&M - Stockyard',y: 5,color: '#3D2B1F'},
            {name: 'Logistics',y: 11,color: '#000000'},
            {name: 'Iron Ore',y: 2,color: '#318CE7'},
            {name: 'Legal ( C )',y: 1,color: '#FAF0BE'},
            {name: 'Contract',y: 3,color: '#0000FF'},
            {name: 'S&M - PAG',y: 7,color: '#DE5D83'},
            {name: 'HR & Employee Services',y: 19,color: '#79443B'},
            {name: 'Port Office - Gopalpur',y: 1,color: '#CC0000'},
            {name: 'Corporate Affairs',y: 2,color: '#B5A642'},
            {name: 'Environment',y: 2,color: '#66FF00'},
            {name: 'Office of MD',y: 6,color: '#BF94E4'},
            {name: 'Climate Change & Sustainability',y: 1,color: '#C32148'},
            {name: 'Domestic Coal Sourcing',y: 4,color: '#FF007F'},
            {name: 'Office of CEO',y: 5,color: '#08E8DE'},
            {name: 'Coal Management Group',y: 1,color: '#D19FE8'},
            {name: 'Information Technology',y: 16,color: '#004225'},
            {name: 'Office of CCO',y: 1,color: '#CD7F32'},
            {name: 'BHUBANESHWAR OFFICE',y: 1,color: '#964B00'},
            {name: 'Management Assurance and Audit System',y: 2,color: '#FFC1CC'},
            {name: 'S&M Branding',y: 2,color: '#E7FEFF'},
            {name: 'Power Trading',y: 5,color: '#F0DC82'},
            {name: 'Sales & Operation Planning',y: 2,color: '#800020'},
            {name: 'Engineering',y: 1,color: '#DEB887'},
            {name: 'Civil',y: 3,color: '#CC5500'},
            {name: 'Office of MD & CEO - JPL',y: 1,color: '#E97451'},
            {name: 'Liasining  & Coordination',y: 1,color: '#8A3324'},
            {name: 'Human Resource & Employee Services',y: 1,color: '#BD33A4'},
            {name: 'Survey',y: 1,color: '#702963'},
            {name: 'Corporate Office -Power',y: 1,color: '#536878'},
            {name: 'Hydro',y: 1,color: '#006B3C'}
            ]
      }]
  });
}
generategeoChart() {
    Highcharts1.chart('associategeochart', {
        chart: {
            type: 'pie',
            backgroundColor: 'transparent',
            // margin: [0, 0, 50, 0],
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            width: 400,
            style: {
                fontFamily: "'segoeui', sans-serif"
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },

        'credits': { enabled: false },
        subtitle: {
            text: ''
        },
        legend: {
            enabled: true,
            align: 'center',
            verticalAlign: 'bottom',
            itemStyle: {
                color: '#333',
                // fontSize: 10,
                position: 'fixed'
            },
            layout: 'horizontal',
            width: '100%',
            y: 20
        },

        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                size: 100,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    // format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            minPointSize: 10,
            innerSize: '50%',
            zMin: 0,
            name: '',
            data: [{
                name: 'India',
                y: 795,
                color: '#9688e0'
            },{
                name: 'USA',
                y: 946,
                color: '#D672AE'
            },{
                name: 'Austria',
                y: 17,
                fontSize: '1.2em',
                color: '#FF0000'
            },{
                name: 'Europe',
                y: 220,
                color: '#FF1493'
            },{
                name: 'China',
                y: 14,
                color: '#00FFFF'
            },{
                name: 'Indonesia',
                y: 47,
                color: '#CD5C5C'
            },{
                name: 'Japan',
                y: 12,
                color: '#20B2AA'
            }]
        }]
    });
}



loadMap(data) {
    Highcharts.mapChart('dlp-map',
    {
        chart: {
            backgroundColor: 'transparent',
            events: {
                load: function () {
                    this.mapZoom(1,100,100);
                }
            },

        },
        title: {
            // text: '<p>GLOBAL FOOTPRINT</p>'
            text: null
        },
        credits: { enabled: false },
        mapNavigation: {
            enabled: false
        },
        legend: {
            enabled: false,
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top'
        },

        series: [
        {
            name: 'Countries',
            mapData: mapGeoJSON,
            nullColor: '#aaa',
            enableMouseTracking: false,
            tooltip: {
                // pointFormat: '{point.name}: {point.footPrint}'
                enabled: false
            }
        },
        {
            type: 'mappoint',
            name: 'National Footprint',
            joinBy: ['iso-a2', 'code'],
            color: Highcharts.getOptions().colors[1],
            cursor : 'pointer',
            
            dataLabels: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{point.name}: {point.footPrint}'
            },
            data: data
        }
        ]
    }
    );

}



showmandate(){
    $("#mandate").modal('show');
}

showsample(){
    $("#sample").modal('show');
}

showCCQ(){
    $("#CCQDesc").modal('show');
}

loadMap_mbl(data) {
    Highcharts.mapChart('dlp-map-mbl',
    {
        chart: {
            backgroundColor: 'transparent',
            height:230,
            events: {
                load: function () {
                    this.mapZoom(1,100,100);
                }
            },

        },
        title: {
            // text: '<p>GLOBAL FOOTPRINT</p>'
            text: null
        },
        credits: { enabled: false },
        mapNavigation: {
            enabled: false
        },
        legend: {
            enabled: false,
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top'
        },

        series: [
        {
            name: 'Countries',
            mapData: mapGeoJSON,
            nullColor: '#aaa',
            enableMouseTracking: false,
            tooltip: {
                // pointFormat: '{point.name}: {point.footPrint}'
                enabled: false
            }
        },
        {
            type: 'mappoint',
            name: 'Global Footprint',
            joinBy: ['iso-a2', 'code'],
            color:'#dc3545',
            cursor : 'pointer',
            // point:{
            //     events:{
            //         click: function(){
            //             // alert(this.name);
            //             $("#worldmyModal").modal('show');
            //         },
            //     }
            // },
            dataLabels: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{point.name}: {point.footPrint}'
            },
            data: data
        }
        ]
    }
    );

}

}


