import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlregistrationComponent } from './dlregistration.component';

describe('DlregistrationComponent', () => {
  let component: DlregistrationComponent;
  let fixture: ComponentFixture<DlregistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlregistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
