import { Component, OnInit, Input } from '@angular/core';

import { Project } from '../../models/project.model';

declare var ldBar: any;
 
@Component({ 
  selector: 'app-peoplespeak',
  templateUrl: './peoplespeak.component.html',
  styleUrls: ['./peoplespeak.component.css']
})
export class PeoplespeakComponent implements OnInit {
  @Input() project: Project;
  public rcs: any;
  public countUpReviews: any;
  public countUpInterviews: any;
  public countUpView: any;

  constructor() { 
    this.rcs = [
      {'code': 'rcwl2', 'prop': 'rc_work_life_balance1', 'label': 'Work-Life Balance', 'icon': 'balance-scale'},
      {'code': 'rcoe2', 'prop': 'rc_org_experience1', 'label': 'Organizational Experience', 'icon': 'sitemap'},
      {'code': 'rcsb2', 'prop': 'rc_salary_benefits1', 'label': 'Salary / Benefits', 'icon': 'wallet'},
      {'code': 'rcjc', 'prop': 'rc_job_security', 'label': 'Job Security', 'icon': 'user-shield'},
      {'code': 'rcmn', 'prop': 'rc_management', 'label': 'Management', 'icon': 'tasks'}
    ]

    this.countUpReviews = 0;
    this.countUpInterviews = 0;
    this.countUpView = false;
  }

  resetCountUp() {
    this.countUpReviews = 0;
    this.countUpInterviews = 0;
    this.countUpView = false;
  }

  setCountUp() {
    this.countUpReviews = this.project.psk.review;
    this.countUpInterviews = this.project.psk.interview;
    this.countUpView = true;
  }
   
  showKnob(el, prop) {
    //console.log(prop);
    var bar = new ldBar('#' + el);
    // console.log(this.project.psk[prop]);
    bar.set( (this.project.psk[prop] * 20));
    //$('.' + el).data('percent', (this.project.lsp[prop] * 10));
    //$('.' + el).loading();
  }

  hideKnob(el, prop) {
    var bar = new ldBar('#' + el);
    bar.set(0);
    //$('.' + el).data('percent', 0);
    //$('.' + el).loading();
  }
  initStuff(){
    
    let self = this;
    self.resetCountUp();
    setTimeout(function() { 
        for(let rc of self.rcs) {
          self.hideKnob(rc.code, rc.prop);
          self.showKnob(rc.code, rc.prop);
        }
        self.setCountUp();
    }, 1000);
  }
  ngOnInit() {
      
    this.project.psk = {
      "executiveSummary": "People Speak is a comprehensive report telling your company's story with actionable insights taking into account factors that offer more value than internal employee engagement surveys.<br>A mixed opinion on the work life balance but overall the company is a greate environment to learn, grow and nurture your skills in the specific fields.",
      "opinion_index": 3.33,
      "review": "1612",
      "interview":"72",
      "ceo_approval":"90",
      "friend_recommend":"73",
      "rc_work_life_balance1": 3.52,
      "rc_org_experience1": 3.47,
      "rc_salary_benefits1": 3.48,
      "rc_job_security": 3.55,
      "rc_management": 2.67
    };

 
    
  }

}

