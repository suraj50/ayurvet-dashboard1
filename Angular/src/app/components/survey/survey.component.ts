import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Options } from 'ng5-slider';
//import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

import { DataService } from '../../services/data.service';
import * as $ from 'jquery';
import Sortable from '../../../assets/Sortable/Sortable';

// Detect touch support
$.support.touch = 'ontouchend' in document;
 
@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {

    @BlockUI() blockUI: NgBlockUI;
    @ViewChild('auto') auto;

    public surveyCode: any;
    public survey: any;
    public surveyView: any;
    public currentQuestion: any;
    public currentQuestionIndex: any;
    public userAnswers: any;
    public surveyError: any;
    public currentYear: any;
    public eumTimeoutHandler: any;
    public dldata: any;
    public onaEmp: any;
    public showdl1form: boolean;
    constructor(
        private dataService: DataService,
        private route: ActivatedRoute,
        private router: Router,
        private title: Title
    ) { 
        this.dldata = {};
        this.showdl1form = false;
        this.currentYear = (new Date()).getFullYear();
        this.surveyCode = this.route.snapshot.params.surveyCode;
        this.blockUI.start('Loading...'); // Start blocking
        this.surveyView = '-';
        this.surveyError =  '';

        this.userAnswers = [];
        this.onaEmp = {
            data: [],
            keyword: 'searchEmpString',
            obj: '',
            name: '',
            rating: '',
            error: false,
            ratestar: 0,
            onaSliderOptions: {
                floor: 0,
                ceil: 5,
                step: 0.1,
                showTicksValues: false,
                animate: true
            },
            ratestars: [5, 4.5, 4, 3.5, 3, 2.5, 2, 1.5, 1, 0.5, 0]
        };

    }

  ngOnInit() {
      this.dldata.dloption = '';
    if(this.surveyCode !== undefined && this.surveyCode !== '') {
        this.dataService.getSurvey(this.surveyCode).subscribe(
            data => {
                console.log(data);
                 this.survey = data.survey;
                 this.showdl1form = false;
                this.survey.instructions = JSON.parse(this.survey.instructions);

                this.title.setTitle(this.survey.projectTitle);
                this.onaEmp.data = this.survey.onaEmployees;

                for(let qobj of this.survey.questions) {
                   if(qobj.options !== undefined && qobj.options.length > 0) {
                       for(let opt of qobj.options) {
                           opt.selected = false;
                       };
                   }
                };
                this.currentQuestionIndex = 0;
                //this.startSurvey();
                 if(data.code === 200 && this.survey.survey_status !== 'complete') {
                    this.surveyView = 'intro';
                 }   
                 else if(data.code === 200 && this.survey.survey_status === 'complete') {
                    this.surveyView = 'athanks';
                 }
                 else if(data.code !== 200) {
                    this.surveyView = 'error';
                 }
                 this.blockUI.stop();
            },
            err => {
                this.surveyView = 'error';
                this.surveyError = 'Survey Code not valid';
                    this.blockUI.stop();
            });
    }
    else {
        this.surveyView = 'error';
        this.surveyError = 'Survey Code not valid';
        this.blockUI.stop();
    }
  }

  startSurvey() {
    this.surveyView = 'survey';
    this.loadQuestion();
  }

  loadQuestion() {
    this.currentQuestion = this.survey.questions[this.currentQuestionIndex];
    if(this.userAnswers[this.currentQuestionIndex] === undefined) {
        this.userAnswers[this.currentQuestionIndex] = {
            questionId: this.currentQuestion.id,
            questionType: this.currentQuestion.questionType,
            surveyId: this.currentQuestion.surveyId,
            answers: []
        };
    }

    if(this.currentQuestion.questionType === 'eum') {
        let self = this;
        $(document).ready(function() {
            let i = 1;

            // for(let qopt of self.currentQuestion.options) {
            //     var el = document.getElementById('lgv_' + self.currentQuestionIndex + '_' + i);
            //     Sortable.create(el, {
            //         animation: 200,
            //         easing: "cubic-bezier(1, 0, 0, 1)",
	           //  group: {
            //           name: "shared",
            //           pull: "clone",
            //           revertClone: true
            //         },
            //         sort: true,
            //         onSort: function (evt) {

            //             self.eumActionListener(evt.to, evt.item, evt.from, evt.oldIndex);
            //         },
            //         onEnd: function (evt) {
            //             self.eumActionListener(evt.to, evt.item, evt.from, evt.oldIndex);
            //             var itemElDI = evt.item;
            //             var itemElDA = evt.to;

            //             // after dropping, remove the highlight of dropped item
            //             let opti = 1;

            //             for(let optb of self.currentQuestion.optBuckets) {
            //                 var bbucket = '#lgo_'+ self.currentQuestionIndex + '_' + opti;
            //                 if($(itemElDA).attr('id') === $(bbucket).attr('id') && $(bbucket).hasClass('highlighted_true') === true) {
            //                     let optj = 1;
            //                     for(let opt of self.currentQuestion.options) {
            //                         $('#lgv_'+self.currentQuestionIndex+'_'+optj).children('.list-group-item').each(function(this) {
            //                             if($(this).attr('title') === $(itemElDI).attr('title')) {
            //                                 $(this).removeClass('eumhl');
            //                             }

            //                         });
            //                         optj++;
            //                     };
            //                 }
            //                 opti++;
            //             };
            //         },
            //     });


            //     i++;
            // };

            let j = 1;
            for(let qopt of self.currentQuestion.optBuckets) {

                qopt.acknowledged = false;
                qopt.active = ( j === 1 ) ? true : false;
                var el = document.getElementById('lgo_' + self.currentQuestionIndex + '_' + j);
                Sortable.create(el, {
                    animation: 200,
                    easing: "cubic-bezier(1, 0, 0, 1)",
                    // group: {
                    //   name: "shared",
                    //   pull: true,
                    //   revertClone: true
                    // },
                    sort: true,
                    onSort: function (evt) {
                        self.eumActionListener(evt.to, evt.item, evt.from, evt.oldIndex);
                    }
                });
                j++;
            };
            console.log( self.currentQuestion.optBuckets);
        });
    }

    
  }
   

  eumHighlightOpts(lbl) {
       return false; // this function not required
       let j = 1;
       $('.eumhl').removeClass('eumhl');
       clearTimeout(this.eumTimeoutHandler);
       for(let qopt of this.currentQuestion.optBuckets) { 
            var el = document.getElementById('lgo_' + this.currentQuestionIndex + '_' + j); 
             $(el).children().each(function(this) { 
                if($(this).attr('title') === lbl) {
                    $(el).addClass('eumhl');
                    $(this).addClass('eumhl');
                }
                else {
                    $(this).removeClass('eumhl');
                }
             });
            j++;
       };

       this.eumTimeoutHandler = setTimeout( function() { $('.eumhl').removeClass('eumhl'); }, 5000);

  };

  eumActionListener(itemElDA, itemElDI, itemElDF, oldIndex) {
    if($(itemElDA).data("da") === 'lgo') { // right area
        // check if this item is already added in this div
        var found = 0;
        $(itemElDA).children().each(function(this) { 
            if($(this).attr('title') === $(itemElDI).attr('title')) {
                if(found > 0) {
                    if($(itemElDF).data('da') === 'lgo') {
                        $(this).appendTo(itemElDF);
                    }
                    else {
                         $(this).remove();
                    }
                }
                else { 
                    found++; 
                }
                
            }
        });

        var order = 1;
        $(itemElDA).children().each(function(this) { 
            $(this).children('.list-group-index').text(order);
            order++;
        });


        var forder = 1;
        $(itemElDF).children().each(function(this) { 
            $(this).children('.list-group-index').text(forder);
            forder++;
        });
        // this.eumProcessOptData();

    }
    // else if($(itemElDA).data("da") === 'lgv') {
    //     if($(itemElDA).attr('id') !== $(itemElDF).attr('id')) {
    //         $(itemElDA).children().each(function(this) {
    //             var atitle = $(this).attr('title');
    //             if(atitle === $(itemElDI).attr('title')) {
    //                 if($(itemElDF).data('da') === 'lgo') {
    //                     var sfound = 0;
    //                     $(itemElDF).children().each(function(this) {
    //                         if(atitle === $(this).attr('title')) { sfound = 1; }
    //                     });

    //                     if(sfound === 0) {
    //                         $(this).appendTo(itemElDF);
    //                     }
    //                 }
    //                 else {
    //                     console.log('removing from lgv');
    //                      $(this).remove();
    //                 }
    //             }
    //         });
    //     }
    // }
    else {
        console.log('no mans land');
    }
  }

  cvaToggleOption(opt) {
    opt.selected = !opt.selected;
    if(opt.selected) {
        this.userAnswers[this.currentQuestionIndex].answers.push(opt);
    }
    else {
        this.userAnswers[this.currentQuestionIndex].answers = this.userAnswers[this.currentQuestionIndex].answers.filter(item => item.value !== opt.value);
    }
    this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 10) ? true : false;
  }
   mcsToggleOption(opt) {
    opt.selected = !opt.selected;
    if(opt.selected) {
        this.userAnswers[this.currentQuestionIndex].answers.push(opt);
    }
    else {
        this.userAnswers[this.currentQuestionIndex].answers = this.userAnswers[this.currentQuestionIndex].answers.filter(item => item.value !== opt.value);
    }
    this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 1) ? true : false;
  }

  enableStSave(){
    var valLength = this.dldata.stqanswer;
    if(valLength.length >= 1 ){
      this.showdl1form = true;
    } else{
      this.showdl1form = false;
    }
  }
  datalake_stsave() {
   if(this.dldata.stqanswer){
      this.userAnswers[this.currentQuestionIndex].answers.push(this.dldata.stqanswer);
    }
    this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 1) ? true : false;
  }
  enableTtSave(){
    var opvalue1 = this.dldata.ttqanswer1;
    var opvalue2 = this.dldata.ttqanswer2;
    console.log(opvalue1.length);
    console.log(opvalue2.length);
    if(opvalue1.length >= 1 && opvalue2.length >= 1){
      this.showdl1form = true;
    } else{
      this.showdl1form = false;
    }
  }
  datalake_ttsave() {
   if(this.dldata.ttqanswer1 && this.dldata.ttqanswer2){
     var finalratio = (this.dldata.ttqanswer1)/(this.dldata.ttqanswer2);
     console.log(finalratio);
      this.userAnswers[this.currentQuestionIndex].answers.push(finalratio);
    }
    this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 1) ? true : false;
  }
   mcmToggleOption(opt) {
    opt.selected = !opt.selected;
    if(opt.selected) {
        this.userAnswers[this.currentQuestionIndex].answers.push(opt);
    }
    else {
        this.userAnswers[this.currentQuestionIndex].answers = this.userAnswers[this.currentQuestionIndex].answers.filter(item => item.value !== opt.value);
    }
    this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 3) ? true : false;
  }
  mcm2ToggleOption(opt) {
    opt.selected = !opt.selected;
    if(opt.selected) {
        this.userAnswers[this.currentQuestionIndex].answers.push(opt);
    }
    else {
        this.userAnswers[this.currentQuestionIndex].answers = this.userAnswers[this.currentQuestionIndex].answers.filter(item => item.value !== opt.value);
    }
    this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 2) ? true : false;
  }

  datalake_dl1() {
    var valLength = this.dldata.dloption;
    console.log("valLength "+ valLength );

    if(this.dldata.dloption){
      this.userAnswers[this.currentQuestionIndex].answers.push(this.dldata.dloption);
    }
    this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 1) ? true : false;
  }
  enableSave(){
    var valLength = this.dldata.dloption;
    console.log("valLength "+ valLength.length );
      this.showdl1form = true;
      console.log(this.showdl1form);
  }
  nextQuestion() {    
    
    this.onaEmp.error = false;
    this.onaEmp.obj = '';
    this.onaEmp.name = '';
    this.onaEmp.rating = 0;
    this.dldata.dloption = '';
    this.showdl1form = false;
    this.survey.questions[this.currentQuestionIndex].answered = true;
    this.userAnswers[this.currentQuestionIndex].answered = true;
    
    if(this.currentQuestionIndex < this.survey.questions.length - 1) {
        // this.currentQuestionIndex++;
        // this.loadQuestion();
        // Check next question has dependency question
        this.checkDependancyQuestion();
    }
    else {
        this.finishSurvey();
    }

  }
    checkDependancyQuestion() {
          let current_question_index = this.currentQuestionIndex;
          this.currentQuestionIndex++; // next question
          let dependent_on_ques_id = this.survey.questions[this.currentQuestionIndex].dependent_on_ques_id;
          if(dependent_on_ques_id != '' && dependent_on_ques_id != null) {
              
              let dependent_on_ques_ans = JSON.parse(this.survey.questions[this.currentQuestionIndex].dependent_ques_config);
              let answer_given_by_user = this.userAnswers[current_question_index].answers;
              console.log(dependent_on_ques_ans);
              console.log(answer_given_by_user);
              // check answer is correct
              // let check_ans = true;
              let union_of_ans = [];
              union_of_ans = dependent_on_ques_ans["dependent_answer"].filter(function(v) { // iterate over the array
                  // check element present in the second array
                  return answer_given_by_user.indexOf(v) > -1;
                  // or array2.includes(v)
              })

              console.log(union_of_ans.length);
              console.log(dependent_on_ques_ans["dependent_answer"].length);
              if(union_of_ans.length == dependent_on_ques_ans["dependent_answer"].length) {
                  this.loadQuestion();
              } else {
                  this.checkDependancyQuestion();
              }
          } else {
              this.loadQuestion();
          }
      }

  finishSurvey() {

    this.blockUI.start('Finishing up...'); // Start blocking
    this.survey.questions[this.currentQuestionIndex].answered = true;
    this.userAnswers[this.currentQuestionIndex].answered = true;
    //this.currentQuestionIndex++;

    var surveyObj = {
        userId: this.survey.userId,
        surveyLink:this.surveyCode,
        projectId: this.survey.projectId,
        projectType: this.survey.projectType,
        userAnswers: this.userAnswers
    }

    console.log(surveyObj);
    console.log('inside finish survey');
    this.dataService.finishSurvey(surveyObj).subscribe(
        data => {
            if(data.code === 200 && data.survey.survey_status == 'complete') {
              
              if (data.survey.projectType === 'datalake') {
                  this.router.navigate(['dlsurvey/'+data.survey.surveyLink]); 
              } else {
                this.surveyView = 'thanks';
              }
             }   
             else {
                this.surveyView = 'aerror';
             }

             this.blockUI.stop();
        },
        err => {
            //this.surveyView = 'aerror';
            this.surveyError = 'Survey not submitted';
                this.blockUI.stop();
        });

   }

 
 
  selectEvent(item) {
    if(item !== undefined && item !== '') {
        this.onaEmp.name = item.empName;
    }
    else {
        alert('removed');
    }
  }
 
  onChangeSearch(val: string) {    
    console.log(val);
  }
  
  onFocused(e){
    // do something when input is focused
    
    //setTimeout(function() {     alert($('li.item').length);

   // $('.suggestions-container').children('ul').each(function() { alert($(this).children('li').length); });
//}, 2000);
        //this.auto.close();
  }
    
  onaSetRating(val) {
    this.onaEmp.rating = val;
  }
  
  onaAddEmp() {
    if(this.onaEmp.name !== '' && this.onaEmp.rating > 0) {
        let empName = this.onaEmp.name;
        let emp = this.onaEmp.data.find(function(item) { return item.empName === empName; });
        if(emp !== undefined) {
            emp.empRating = this.onaEmp.rating;
            let obj =  Object.assign({}, emp);

            if(this.userAnswers[this.currentQuestionIndex] === undefined) {
                this.userAnswers[this.currentQuestionIndex] = {
                    questionId: this.currentQuestion.id,
                    questionType: this.currentQuestion.questionType,
                    surveyId: this.currentQuestion.surveyId,
                    answers: []
                };
            }
            
            let aemp = this.userAnswers[this.currentQuestionIndex].answers.find(function(item) { return item.empId === emp.empId; });
            if(aemp === undefined) {
                this.userAnswers[this.currentQuestionIndex].answers.push(obj);
            }

            this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 3) ? true : false;
            
            this.onaEmp.error = false;
            this.onaEmp.obj = '';
            this.onaEmp.name = '';
            this.onaEmp.rating = 0;
            this.onaEmp.ratestar = 0;
            this.auto.clear();
            
            if(this.userAnswers[this.currentQuestionIndex].answers.length === 3) { this.auto.close(); } 
            
        }
        else {
            this.onaEmp.error = true;
        }
    }
  }

  onaRemoveUser(opt) {
    this.userAnswers[this.currentQuestionIndex].answers = this.userAnswers[this.currentQuestionIndex].answers.filter(item => item.empId !== opt.empId);
    this.userAnswers[this.currentQuestionIndex].answered = false;
    this.auto.open();
  }

    eumValidateNumber() {

        this.userAnswers[this.currentQuestionIndex].answers = [];
        this.userAnswers[this.currentQuestionIndex].answered = false;

        for(let qopt of this.currentQuestion.options) {

            if(qopt.rate !== undefined && qopt.rate !== '' && parseInt(qopt.rate) >= 0) {
                qopt.rate = parseInt(qopt.rate);
                if(qopt.rate > 15) { qopt.rate = 15; }
                let obj =  Object.assign({}, qopt);
                this.userAnswers[this.currentQuestionIndex].answers.push(obj);
            }
            else {
                qopt.rate = '';
            }
        };

        if(this.userAnswers[this.currentQuestionIndex].answers.length === this.currentQuestion.options.length) {
            this.userAnswers[this.currentQuestionIndex].answered = true;
        }
    }

    eumHighlightBktOpts(blabel) {
        let i = 1;
        $('.eumhl').removeClass('eumhl');
        for(let qopt of this.currentQuestion.optBuckets) {
            var bucket = '#'+'lgo_'+this.currentQuestionIndex+'_'+i;
            if(qopt.label === blabel) {
                qopt.highlighted = !qopt.highlighted;
                if(qopt.highlighted === true) {
                    let j = 1;
                    for(let opt of this.currentQuestion.options) {
                        if($(bucket).children("[title*='"+opt.label+"']").length === 0) {
                            $('#lgv_'+this.currentQuestionIndex+'_'+j).children('.list-group-item').addClass('eumhl');
                        }
                        j++;
                    };
                }
                else {

                }
            }
            else {
                qopt.highlighted = false;
                //$('#'+'lgo_'+this.currentQuestionIndex+'_'+i).children('
            }
            i++;
        };
    }

    enablenextoption(optindex) {
        if(confirm("Are you sure you want to move next ?")) {
            let i = 1;
            this.currentQuestion.optBuckets[optindex].acknowledged = true;
            console.log(this.currentQuestion.optBuckets);
            if((optindex + 1) === this.currentQuestion.optBuckets.length) { // this is last question
                this.eumProcessOptData();
            }
            else {
                this.currentQuestion.optBuckets[optindex+1].active = true;
            }
        }

        let allchecked = true;
        for(let optb of this.currentQuestion.optBuckets) {
            //console.log(optb.acknowledged, allchecked);
            if(optb.acknowledged === false ) {
                allchecked = false;
            }
        }

        if(allchecked === true) {
            this.userAnswers[this.currentQuestionIndex].answered = true;
        }
        else {
            this.userAnswers[this.currentQuestionIndex].answered = false;
        }

    }




    eumProcessOptData() {
        this.userAnswers[this.currentQuestionIndex].answers = []; var self = this;
        var i = 1; var answers = []; var opts_count = [];
        //console.log(this.currentQuestion.optBuckets);
        for(let qopt of this.currentQuestion.optBuckets) {
            var j = 1;
            $('#lgo_'+this.currentQuestionIndex+'_'+i).children().each(function(this) {
                var lbl = $(this).attr('title');
                if(opts_count[lbl] === undefined) { opts_count[lbl] = 0; }
                var key = self.currentQuestion.options.find(function(item) { return item.label === lbl; });
                if(key !== undefined) {
                    let obj =  Object.assign({}, key);
                    obj.option_category = qopt.label;
                    obj.option_order = j;
                    answers.push(obj);
                    opts_count[lbl]++;
                    j++;
                }
            });
            i++;
        };

        /*
        for(let qopt of this.currentQuestion.options) {
            if(opts_count[qopt.label] >= 3) { 

            }
            else {
                
            }
        }
        */
        //console.log(answers);
        this.userAnswers[this.currentQuestionIndex].answers = answers;
        var shouldLength = this.currentQuestion.options.length * this.currentQuestion.optBuckets.length;
       /* if(this.userAnswers[this.currentQuestionIndex].answers.length === shouldLength) {
            this.userAnswers[this.currentQuestionIndex].answered = true;
        }
        else {
            this.userAnswers[this.currentQuestionIndex].answered = false;
        }*/
    }
}
