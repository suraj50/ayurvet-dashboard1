import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlsurveyComponent } from './dlsurvey.component';

describe('DlsurveyComponent', () => {
  let component: DlsurveyComponent;
  let fixture: ComponentFixture<DlsurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlsurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlsurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
