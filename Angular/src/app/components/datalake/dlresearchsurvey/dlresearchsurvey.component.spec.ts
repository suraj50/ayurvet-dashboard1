import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlresearchsurveyComponent } from './dlresearchsurvey.component';

describe('DlresearchsurveyComponent', () => {
  let component: DlresearchsurveyComponent;
  let fixture: ComponentFixture<DlresearchsurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlresearchsurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlresearchsurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
