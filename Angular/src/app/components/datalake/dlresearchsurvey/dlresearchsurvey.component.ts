import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Options } from 'ng5-slider';

import { DataService } from '../../../services/data.service';
// import * as $ from 'jquery';
import Sortable from '../../../../assets/Sortable/Sortable';
import Swal from 'sweetalert2';
 
// Detect touch support
$.support.touch = 'ontouchend' in document;
declare var $: any;
// declare var $: any;

@Component({
	selector: 'app-dlresearchsurvey',
	templateUrl: './dlresearchsurvey.component.html',
	styleUrls: ['./dlresearchsurvey.component.css']
}) 
export class DlresearchsurveyComponent implements OnInit {

	@BlockUI() blockUI: NgBlockUI;
	@ViewChild('auto') auto;

	public surveyCode: any;
	public survey: any;
	public surveyView: any;
	public currentQuestion: any;
	public currentQuestionIndex: any;
	public userAnswers: any;
	public surveyError: any;
	public currentYear: any;
	public eumTimeoutHandler: any;
	public dldata: any;
	public showdl1form: boolean;
	public showpriQbtn: any;
	public currentQuesAnswers: any;
	public visibilityflag: any;
	public priarray: any;
	public prioroty_level: any;
	public priorityanswer: any;
	public mdlSampleIsOpen : boolean = false;
	public $: any;
	public lastQstatus: boolean;
	public infoInstruction: any;
	constructor(
		private dataService: DataService,
		private route: ActivatedRoute,
		private router: Router,
		private title: Title
	) {
		this.dldata = {};
		this.showdl1form = false;
		this.lastQstatus = false;
		this.infoInstruction = false;
		this.showpriQbtn = 'no';
		this.currentYear = (new Date()).getFullYear();
		this.surveyCode = this.route.snapshot.params.surveyCode;
		this.blockUI.start('Loading...'); // Start blocking
		this.surveyView = '-';
		this.surveyError =  '';
		this.currentQuesAnswers = [];
		this.userAnswers = [];
		this.priarray = ["1","2","3","4","5","6"];
		// this.dldata.otheropt = ['one', 'two', 'three', 'four','five','six','seven','eight','none','','','twelve'];
		// this.dldata.otheropt = ['one', 'two', 'three', 'four','five','six','seven','eight','none','ten','eleven','twelve'];
		// itemList = ['thing zero', 'thing one', 'thing two', '']
		this.dldata.otheropt = '';
	}


	ngOnInit() {
		this.dldata.dloption = '';
		if(this.surveyCode !== undefined && this.surveyCode !== '') {
			this.dataService.getSurvey(this.surveyCode).subscribe(
				data => {
					console.log(data);
					this.survey = data.survey;
					this.showdl1form = false;
					this.survey.instructions = JSON.parse(this.survey.instructions);

					this.title.setTitle(this.survey.projectTitle);

					for(let qobj of this.survey.questions) {
						if(qobj.options !== undefined && qobj.options.length > 0) {
							for(let opt of qobj.options) {
								opt.selected = false;
							};
						}

						if(qobj.questionType === 'pri_order') {
							qobj.selectedOptions = []; let qo = 0;
							for(let qopt of qobj.options) {
								qopt.order = qo+1;
								qobj.selectedOptions[qo] = qo+1; qo++;
							}
						}

					};
					this.currentQuestionIndex = 0;
					this.visibilityflag = 0;
					//this.startSurvey();
					if(data.code === 200 && this.survey.survey_status !== 'complete') {
						// this.surveyView = 'intro';
						this.startSurvey();
					}   
					else if(data.code === 200 && this.survey.survey_status === 'complete') {
						this.surveyView = 'athanks';
					}
					else if(data.code !== 200) {
						this.surveyView = 'error';
					}
					this.blockUI.stop();
				},
				err => {
					this.surveyView = 'error';
					this.surveyError = 'Survey Code not valid';
					this.blockUI.stop();
				});
		}
		else {
			this.surveyView = 'error';
			this.surveyError = 'Survey Code not valid';
			this.blockUI.stop();
		}
	}

	startSurvey() {
		this.surveyView = 'survey';
		this.loadQuestion();
	}
	checkpriority(){
		console.log(this.prioroty_level);
	}
	showInstructions(curState) {
		this.infoInstruction = curState;
		console.log(this.infoInstruction);
	}
	loadQuestion() {
		this.currentQuestion = this.survey.questions[this.currentQuestionIndex];
		if(this.userAnswers[this.currentQuestionIndex] === undefined) {
			this.userAnswers[this.currentQuestionIndex] = {
				questionId: this.currentQuestion.id,
				questionType: this.currentQuestion.questionType,
				surveyId: this.currentQuestion.surveyId,
				answers: []
			};
		}
	}

	dlToggleOption(opt,currentquesindex) {
		console.log(opt);
		opt.selected = !opt.selected;
		if(opt.selected) {
			console.log(opt);
			this.currentQuesAnswers.push(opt);
		}
		else {
			this.currentQuesAnswers = this.currentQuesAnswers.filter(item => item.value !== opt.value);
		}
	}

	otherpopup(opt,currentquesindex){
		console.log(opt);
		opt.selected = !opt.selected;
		console.log(this.dldata.otheropt);
		if(opt.selected){
			opt.value = this.dldata.otheropt;
			this.currentQuesAnswers.push(opt);
			this.dldata.otheropt = "";
			console.log("#dtlakemdl_" + opt.id + "_" + currentquesindex);
			 $("#dtlakemdl_" + opt.id + "_" + currentquesindex).modal('hide');
			 
		}else{
			console.log('uncheck');
			this.dldata.otheropt = "";
			this.currentQuesAnswers = this.currentQuesAnswers.filter(item => item.label !== opt.label);
		}
		// this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 1) ? true : false;
	}
	dloptdselect(opt,currentquesindex){
		opt.selected = !opt.selected;
		this.dldata.otheropt = "";
		this.currentQuesAnswers = this.currentQuesAnswers.filter(item => item.label !== opt.label);
	}
	mcsToggleOption(opt,currentquesindex) {
		// console.log(opt);
		// console.log(currentquesindex);
		
		opt.selected = !opt.selected;
		if(opt.selected) {
			this.currentQuesAnswers.push(opt);
		}
		else {
			this.currentQuesAnswers = this.currentQuesAnswers.filter(item => item.value !== opt.value);
		}		
	}

	mcmToggleOption(opt,currentquesindex) {
		opt.selected = !opt.selected;
		if(opt.selected) {
			this.userAnswers[currentquesindex].answers.push(opt);
		}
		else {
			this.userAnswers[currentquesindex].answers = this.userAnswers[currentquesindex].answers.filter(item => item.value !== opt.value);
		}
		this.userAnswers[currentquesindex].answered = (this.userAnswers[currentquesindex].answers.length === 3) ? true : false;

		this.dldata.dloption = '';
		this.survey.questions[currentquesindex].answered = true;
		this.userAnswers[currentquesindex].answered = true;

		currentquesindex++;
		if(currentquesindex < this.survey.questions.length) {
			this.currentQuestionIndex = currentquesindex;
		}
		this.loadQuestion();
	}

	mcm2ToggleOption(opt,currentquesindex) {
		opt.selected = !opt.selected;
		if(opt.selected) {
			this.userAnswers[currentquesindex].answers.push(opt);
		}
		else {
			this.userAnswers[currentquesindex].answers = this.userAnswers[currentquesindex].answers.filter(item => item.value !== opt.value);
		}
		this.userAnswers[currentquesindex].answered = (this.userAnswers[currentquesindex].answers.length === 2) ? true : false;

		this.dldata.dloption = '';
		this.survey.questions[currentquesindex].answered = true;
		this.userAnswers[currentquesindex].answered = true;

		currentquesindex++;
		if(currentquesindex < this.survey.questions.length) {
			this.currentQuestionIndex = currentquesindex;
		}
		this.loadQuestion();
	}

	enableStSave(){
		var valLength = this.dldata.stqanswer;
		if(valLength.length >= 1 ){
			this.showdl1form = true;
		} else{
			this.showdl1form = false;
		}
	}
	datalake_stsave() {
		if(this.dldata.stqanswer){
			this.userAnswers[this.currentQuestionIndex].answers.push(this.dldata.stqanswer);
		}
		this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 1) ? true : false;
	}
	enableTtSave(){
		var opvalue1 = this.dldata.ttqanswer1;
		var opvalue2 = this.dldata.ttqanswer2;
		console.log(opvalue1.length);
		console.log(opvalue2.length);
		if(opvalue1.length >= 1 && opvalue2.length >= 1){
			this.showdl1form = true;
		} else{
			this.showdl1form = false;
		}
	}
	datalake_ttsave() {
		if(this.dldata.ttqanswer1 && this.dldata.ttqanswer2){
			var finalratio = (this.dldata.ttqanswer1)/(this.dldata.ttqanswer2);
			console.log(finalratio);
			this.userAnswers[this.currentQuestionIndex].answers.push(finalratio);
		}
		this.userAnswers[this.currentQuestionIndex].answered = (this.userAnswers[this.currentQuestionIndex].answers.length === 1) ? true : false;
		console.log(this.userAnswers[this.currentQuestionIndex].answers);
	}
	

	enableSave(){
		var valLength = this.dldata.dloption;
		console.log("valLength "+ valLength.length );
		this.showdl1form = true;
		console.log(this.showdl1form);
	}
	// nextQuestion() {    
	// 	this.dldata.dloption = '';
	// 	this.showdl1form = false;
	// 	this.survey.questions[this.currentQuestionIndex].answered = true;
	// 	this.userAnswers[this.currentQuestionIndex].answered = true;

	// 	if(this.currentQuestionIndex < this.survey.questions.length - 1) {
	// 		// Check next question has dependency question
	// 		this.checkDependancyQuestion();
	// 	}
	// 	else {
	// 		this.finishSurvey();
	// 	}

	// }

	checkDependancyQuestion() {
		let current_question_index = this.currentQuestionIndex;
		this.currentQuestionIndex++; // next question
		let dependent_on_ques_id = this.survey.questions[this.currentQuestionIndex].dependent_on_ques_id;
		if(dependent_on_ques_id != '' && dependent_on_ques_id != null) {

			let dependent_on_ques_ans = JSON.parse(this.survey.questions[this.currentQuestionIndex].dependent_ques_config);
			let answer_given_by_user = this.userAnswers[current_question_index].answers;
			console.log(dependent_on_ques_ans);
			console.log(answer_given_by_user);
			// check answer is correct
			// let check_ans = true;
			let union_of_ans = [];
			union_of_ans = dependent_on_ques_ans["dependent_answer"].filter(function(v) { // iterate over the array
				// check element present in the second array
				return answer_given_by_user.indexOf(v) > -1;
				// or array2.includes(v)
			})

			console.log(union_of_ans.length);
			console.log(dependent_on_ques_ans["dependent_answer"].length);
			if(union_of_ans.length == dependent_on_ques_ans["dependent_answer"].length) {
				this.loadQuestion();
			} else {
				this.checkDependancyQuestion();
			}
		} else {
			this.loadQuestion();
		}
	}

	finishSurvey(continue_survey) {
		console.log(this.userAnswers);
		if(continue_survey == 'finish') {
			this.blockUI.start('Finishing Up...');
			this.surveyView = 'thanks';
		}
		if(continue_survey == 'continue') {
			this.blockUI.start('Loading TIPE Assessment...'); // Start blocking
		}
		this.survey.questions[this.currentQuestionIndex].answered = true;
		this.userAnswers[this.currentQuestionIndex].answered = true;
		//this.currentQuestionIndex++;

		var surveyObj = {
			userId: this.survey.userId,
			surveyLink:this.surveyCode,
			projectId: this.survey.projectId,
			projectType: this.survey.projectType,
			userAnswers: this.userAnswers
		}

		console.log(surveyObj);
		console.log('inside finish survey');
		this.dataService.finishSurvey(surveyObj).subscribe(
			data => {
				if(data.code === 200 && data.survey.survey_status == 'complete') {

					if (data.survey.projectType === 'datalake' && continue_survey == 'continue') {
						this.router.navigate(['dlsurveyins/'+data.survey.surveyLink]); 
					} else {
						this.surveyView = 'thanks';
					}
				}   
				else {
					this.surveyView = 'aerror';
				}

				this.blockUI.stop();
			},
			err => {
				//this.surveyView = 'aerror';
				this.surveyError = 'Survey not submitted';
				this.blockUI.stop();
			});

	}
 
	
	priSwapOrder(question, curIndex) {
		var optPreVal = question.options[curIndex].order;
		var optCurVal = question.selectedOptions[curIndex];
		
		var index = 0; var prevIndex = '';
		for(let qopt of question.options) {
			if(qopt.order === optCurVal) {
				prevIndex = index.toString();
			}
			index++;
		}

		question.options[curIndex].order =  parseInt(optCurVal);

		if(optPreVal !== '') {
			question.selectedOptions[prevIndex] = optPreVal;
			question.options[prevIndex].order =  parseInt(optPreVal);
		}
		else {
			if(prevIndex !== '') {
				let tindex = 0;
				for(let topt of question.options) {
					if(optPreVal === '' && topt.order === '') {
						optPreVal = tindex + 1;
					}
					tindex++;
				}

				question.selectedOptions[prevIndex] = optPreVal;
				question.options[prevIndex].order =  parseInt(optPreVal);

			}
		}
		this.showpriQbtn = 'yes';
		this.priorityanswer = question.options; 
	}

	saveprianswer(currentquesindex){
		Swal.fire({
      title: "Are you sure?",
      // type: 'warning',
      showConfirmButton: true,
      showCancelButton: true     
    })
    .then((willDelete) => {
        if(willDelete.value){
             console.log(this.priorityanswer);
             for(let cqopt of this.priorityanswer) {
				cqopt.selected = true;
				if(cqopt.selected) {
					this.currentQuesAnswers.push(cqopt);
				}
				else {
					this.currentQuesAnswers = this.currentQuesAnswers.filter(item => item.value !== cqopt.value);
				}
			}
			this.saveDlAnswer(currentquesindex);
	        }
	    });
		
	}
	
	loadComAssessment() {
		this.router.navigate(['dlsurveyins/'+this.surveyCode]); 
	}

	OPenlastQ(){
		this.lastQstatus = true;
		console.log(this.lastQstatus);
	}
	// Changes made by Tejashri 
	saveDlAnswer(currentquesindex) {
		// console.log(currentquesindex);
		this.currentQuestion = this.survey.questions[currentquesindex];
		if(this.userAnswers[currentquesindex] === undefined) {
			this.userAnswers[currentquesindex] = {
				questionId: this.currentQuestion.id,
				questionType: this.currentQuestion.questionType,
				surveyId: this.currentQuestion.surveyId,
				answers: []
			};
		}
		// console.log(this.userAnswers[currentquesindex]);
		// MCS
		if(this.currentQuestion.questionType === 'mcs' && this.currentQuesAnswers.length == 1) {
			this.userAnswers[currentquesindex].answers = this.currentQuesAnswers;
			this.userAnswers[currentquesindex].answered = (this.userAnswers[currentquesindex].answers.length === 3) ? true : false;
			this.dldata.dloption = '';
			this.survey.questions[currentquesindex].answered = true;
			this.userAnswers[currentquesindex].answered = true;
			this.currentQuesAnswers = [];
			this.visibilityflag++;
		} else if(this.currentQuestion.questionType === 'mcs' && this.currentQuesAnswers.length != 1) {
			Swal.fire(
				'Please select one option.'
			)
		}

		// MCM2
		if(this.currentQuestion.questionType === 'mcm2' && this.currentQuesAnswers.length == 2) {
			this.userAnswers[currentquesindex].answers = this.currentQuesAnswers;
			this.userAnswers[currentquesindex].answered = (this.userAnswers[currentquesindex].answers.length === 3) ? true : false;
			this.dldata.dloption = '';
			this.survey.questions[currentquesindex].answered = true;
			this.userAnswers[currentquesindex].answered = true;
			this.currentQuesAnswers = [];
			this.visibilityflag++;
		} else if(this.currentQuestion.questionType === 'mcm2' && this.currentQuesAnswers.length != 2) {
			Swal.fire(
				'Please select two option.'
			)
		}

		// MCM2
		if(this.currentQuestion.questionType === 'pri_order') {
			this.userAnswers[currentquesindex].answers = this.currentQuesAnswers;
			this.userAnswers[currentquesindex].answered = true;
			this.dldata.dloption = '';
			this.survey.questions[currentquesindex].answered = true;
			this.userAnswers[currentquesindex].answered = true;
			this.currentQuesAnswers = [];
			this.visibilityflag++;
		} 


		// MCM
		if(this.currentQuestion.questionType === 'mcm' && this.currentQuesAnswers.length == 3) {
			this.userAnswers[currentquesindex].answers = this.currentQuesAnswers;
			this.userAnswers[currentquesindex].answered = (this.userAnswers[currentquesindex].answers.length === 3) ? true : false;
			this.dldata.dloption = '';
			this.survey.questions[currentquesindex].answered = true;
			this.userAnswers[currentquesindex].answered = true;
			this.currentQuesAnswers = [];
			this.visibilityflag++;
		} else if(this.currentQuestion.questionType === 'mcm' && this.currentQuesAnswers.length != 3) {
			Swal.fire(
				'Please select three option.'
			)
		}
		if(this.currentQuestion.questionType === 'tt') {
			if(this.dldata.ttqanswer1 && this.dldata.ttqanswer2){
				var finalratio = (this.dldata.ttqanswer2)/(this.dldata.ttqanswer1);
				console.log(finalratio);
				// this.userAnswers[this.currentQuestionIndex].answers.push(finalratio);
				this.currentQuesAnswers.push(finalratio);
				if(this.currentQuesAnswers) {
					this.userAnswers[currentquesindex].answers = this.currentQuesAnswers;
					this.userAnswers[currentquesindex].answered = (this.userAnswers[currentquesindex].answers.length === 3) ? true : false;
					this.dldata.dloption = '';
					this.survey.questions[currentquesindex].answered = true;
					this.userAnswers[currentquesindex].answered = true;
					this.currentQuesAnswers = [];
					this.visibilityflag++;
				}
			}else  {
				Swal.fire(
					'Please enter integrations number.'
				)
			}
		}

		// ST
		if(this.currentQuestion.questionType === 'st') {
			if(this.dldata.stqanswer !== undefined){
				if(this.dldata.stqanswer){
					this.currentQuesAnswers.push(this.dldata.stqanswer);
				}
				if(this.currentQuesAnswers) {
					this.userAnswers[currentquesindex].answers = this.currentQuesAnswers;
					this.userAnswers[currentquesindex].answered = (this.userAnswers[currentquesindex].answers.length === 3) ? true : false;
					this.dldata.dloption = '';
					this.survey.questions[currentquesindex].answered = true;
					this.userAnswers[currentquesindex].answered = true;
					this.currentQuesAnswers = [];
					this.visibilityflag++;
				}
			} else  {
				Swal.fire(
					'Please enter integrations number.'
				)
			}
		} 

	}

}
