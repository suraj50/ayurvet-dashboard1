import { Component, OnInit, ViewChild } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

import { DataService } from '../../../services/data.service';
import { AuthenticationService } from '../../../services/authentication.service';

import { User } from '../../../models/user.model';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-dlprojects',
	templateUrl: './dlprojects.component.html',
	styleUrls: ['./dlprojects.component.css']
})
export class DlprojectsComponent implements OnInit {
	@BlockUI() blockUI: NgBlockUI;
	@ViewChild('csvReader') csvReader: any;

	public currentUser: User;
	public orgId: any;

	constructor(
		private dataService: DataService,
        private authService: AuthenticationService
    ) { }

	ngOnInit() {
		this.currentUser = this.authService.currentUserValue;
		this.orgId = this.currentUser.org_id;
	}

	requestProject() {
		// console.log(this.orgId);

		// check if employee data is added first
		this.dataService.getOrganizationEmployees(this.orgId).subscribe(
			data => {
				// console.log(data);
				Swal.fire({
					title: 'Are you sure?',
					text: "You want to request project!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes'
				}).then((result) => {
					if (result.value) {
						this.dataService.requestOrgProject(this.orgId).subscribe(
							data => {
								// console.log(data);
								if(data.code == 200) {
									Swal.fire(
										'Success!',
										'Your project request has been sent.',
										'success'
									)
								}
							},
							err => {
								// console.log("inside My Organization error", JSON.stringify(err.message));
								Swal.fire(
									'Failed!',
									'Failed to sent your project request.',
									'error'
								)
								this.blockUI.stop();
							}
						);
								
					}
				});
			},
			err => {
				// console.log("inside My Organization error", JSON.stringify(err.message));
				Swal.fire('SORRY', 'Data is not available for organization employees!<br> Please add organization employees first and then try again.', 'error');
				this.blockUI.stop();
			}
		);
	}

}
