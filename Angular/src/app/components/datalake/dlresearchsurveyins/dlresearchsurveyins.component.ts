import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { DataService } from '../../../services/data.service';
import Swal from 'sweetalert2';

@Component({
	selector: 'app-dlresearchsurveyins',
	templateUrl: './dlresearchsurveyins.component.html',
	styleUrls: ['./dlresearchsurveyins.component.css']
})
export class DlresearchsurveyinsComponent implements OnInit {
	@BlockUI() blockUI: NgBlockUI;

	public surveyCode: any;
	public survey: any;
	public surveyView: any;
	public currentQuestion: any;
	public currentQuestionIndex: any;
	public userAnswers: any;
	public surveyError: any;
	public currentYear: any;
	public eumTimeoutHandler: any;
	public dldata: any;
	public showdl1form: boolean;
	public showpriQbtn: any;
	public currentQuesAnswers: any;
	public visibilityflag: any;
	public priarray: any;
	public prioroty_level: any;
	public priorityanswer: any;
	public mdlSampleIsOpen : boolean = false;
	public $: any;
	constructor(
		private dataService: DataService,
		private route: ActivatedRoute,
		private router: Router,
		private title: Title
	) {
		this.dldata = {};
		this.showdl1form = false;
		this.showpriQbtn = 'no';
		this.currentYear = (new Date()).getFullYear();
		this.surveyCode = this.route.snapshot.params.surveyCode;
		this.blockUI.start('Loading...'); // Start blocking
		this.surveyView = '-';
		this.surveyError =  '';
		this.currentQuesAnswers = [];
		this.userAnswers = [];
		this.priarray = ["1","2","3","4","5","6"];
		// this.dldata.otheropt = ['one', 'two', 'three', 'four','five','six','seven','eight','none','','','twelve'];
		// this.dldata.otheropt = ['one', 'two', 'three', 'four','five','six','seven','eight','none','ten','eleven','twelve'];
		// itemList = ['thing zero', 'thing one', 'thing two', '']
		this.dldata.otheropt = '';
	}


	ngOnInit() {
		this.dldata.dloption = '';
		if(this.surveyCode !== undefined && this.surveyCode !== '') {
			this.dataService.getSurvey(this.surveyCode).subscribe(
				data => {
					console.log(data);
					this.survey = data.survey;
					this.showdl1form = false;
					this.survey.instructions = JSON.parse(this.survey.instructions);

					this.title.setTitle(this.survey.projectTitle);

					this.currentQuestionIndex = 0;
					this.visibilityflag = 0;
					//this.startSurvey();
					if(data.code === 200 && this.survey.survey_status !== 'complete') {
						this.surveyView = 'intro';
					}   
					else if(data.code !== 200) {
						this.surveyView = 'error';
					}
					this.blockUI.stop();
				},
				err => {
					this.surveyView = 'error';
					this.surveyError = 'Survey Code not valid';
					this.blockUI.stop();
				});
		}
		else {
			this.surveyView = 'error';
			this.surveyError = 'Survey Code not valid';
			this.blockUI.stop();
		}
	}

	startSurvey() {
		this.router.navigate(['dlresearchsurvey/'+this.surveyCode]); 
	}
	loadComAssessment() {
		this.router.navigate(['dlsurveyins/'+this.surveyCode]); 
	}

}
