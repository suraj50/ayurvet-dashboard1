import { Component, OnInit, ViewChild } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

import { DataService } from '../../../services/data.service';
import { AuthenticationService } from '../../../services/authentication.service';

import { User } from '../../../models/user.model';
 
@Component({
	selector: 'app-dlemployee',
	templateUrl: './dlemployee.component.html',
	styleUrls: ['./dlemployee.component.css']
})
export class DlemployeeComponent implements OnInit {

	@BlockUI() blockUI: NgBlockUI;
	@ViewChild('csvReader') csvReader: any;

	public currentUser: User;
	public records: any[] = [];
	public csvFile: any;
	public errorcsvfile: any;
	public summarypage: any;
	public all_records_valid: any; 
	public fileToUpload: File;
	public userRecords: any;
	public org_id: any;
	public user_id: any;
	public csvRecords: any;
	public viewEmployees: any;
	public orgEmployees: any;
	public uploadtab: any;
	constructor(
		private dataService: DataService,
        private authService: AuthenticationService
	) { 
		this.errorcsvfile = '';
		this.summarypage = false;
		this.viewEmployees = false;
		this.all_records_valid = false;
		this.fileToUpload = null;
		this.uploadtab = 'filetab';
		this.csvRecords = {
			'orgid': '',
			'csvrecords': '',
			'userid': ''
		}
	}

	uploadListener($event: any): void {  
		// this.csvFile = $event.srcElement.files; 
		this.csvFile = $event;  // for old uploadEmployeeCSV()
		// this.csvFile = $event.target.files;  // for new uploadEmployeeCSV()
		// console.log(this.csvFile);
		 
	}

	// new uploadEmployeeCSV()
	// uploadEmployeeCSV() {
	// 	let fileToUpload = this.csvFile.item(0);
	// 	console.log(fileToUpload);
	// 	this.currentUser = this.authService.currentUserValue;
	// 	if(fileToUpload != undefined) {
	// 		let csvFormData: FormData = new FormData();
 //    		csvFormData.append('org_id', this.currentUser.org_id);
 //    		csvFormData.append('csvfile', fileToUpload, fileToUpload.name);
	// 		// let csv = {
	// 		// 	'org_id': this.currentUser.org_id,
	// 		// 	'csvfile': fileToUpload
	// 		// };

			
	// 	}
			
	// 	// console.log(csvFormData);
	// }

	// old uploadEmployeeCSV()
	uploadEmployeeCSV() {
		

		// console.log(this.currentUser);
		this.errorcsvfile  = '';
		if(this.csvFile != undefined) {
			let text = [];  
	   		let files = this.csvFile.srcElement.files; 
			
			if (this.isValidCSVFile(files[0])) {  
				let input = this.csvFile.target;  
				let reader = new FileReader();  
				reader.readAsText(input.files[0]);  

				reader.onload = () => {  
					let csvData = reader.result;  
					let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);  

					let headersRow = this.getHeaderArray(csvRecordsArray);  

					this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);
					console.log(this.records);
					
					this.csvRecords = {
						'csvrecords': this.records,
						'orgid': this.org_id,
						'userid': this.user_id
					};

					// send user records for validation
					this.dataService.validateEmployees(this.csvRecords).subscribe(
						data => {
							console.log(data);
							this.all_records_valid = data.all_records_valid;
							this.userRecords = data.validation_summary;
							this.summarypage = true;
							this.uploadtab = 'previewtab';
							this.viewEmployees = false;
							this.blockUI.stop();
						},
						err => {
							// console.log("inside My Organization error", JSON.stringify(err.message));
							this.blockUI.stop();
						}
					);

					this.fileReset(); 
					// $("#uploadEmployees .close").click();
				};  

				reader.onerror = function () {  
					console.log('error is occured while reading file!');  
				};  

			} else {  
				// alert("Please import valid .csv file.");  
				this.errorcsvfile = "Please import valid .csv file.";
				this.fileReset();  
			} 
		}
		else {
			this.errorcsvfile = "Please select .csv file.";
		}
	}

	resetUploadEmployees() {
		this.summarypage = false;
		this.uploadtab = 'filetab';
		this.fileReset();
	}

	getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {  
		let csvArr = [];  

		for (let i = 1; i < csvRecordsArray.length; i++) {  
			let curruntRecord = (<string>csvRecordsArray[i]).split(',');  
			if (curruntRecord.length == headerLength) {  
				let csvRecord = {
					'csvline': 0,
					'existinguser': '',
					'firstname': '',
					'lastname': '',
					'dob': '',
					'gender': '',
					'marital_status': '',
					'company_email': '',
					'new_email': '',
					'designation': '',
					'total_experience': '',
					'prefered_language': '',
					'department': '',
					'address': '',
					'mobile': ''
				};
				let error = '';
				csvRecord.csvline = i+1;
				csvRecord.existinguser = curruntRecord[0].trim();
				csvRecord.firstname = curruntRecord[1].trim();  
				csvRecord.lastname = curruntRecord[2].trim();  
				csvRecord.dob = curruntRecord[3].trim();
				csvRecord.dob = csvRecord.dob.substr(1).slice(0, -1);  
				csvRecord.gender = curruntRecord[4].trim();  
				csvRecord.marital_status = curruntRecord[5].trim();  
				csvRecord.company_email = curruntRecord[6].trim();  
				csvRecord.new_email = curruntRecord[7].trim();  
				csvRecord.designation = curruntRecord[8].trim();  
				csvRecord.total_experience = curruntRecord[9].trim();  
				csvRecord.prefered_language = curruntRecord[10].trim();  
				csvRecord.department = curruntRecord[11].trim();  
				csvRecord.address = curruntRecord[12].trim();  
				csvRecord.mobile = curruntRecord[13].trim();  
				// console.log(this.csvRecord);
				csvArr.push(csvRecord);  
			}  
		}  
		return csvArr;  
	}  

	isValidCSVFile(file: any) {  
		return file.name.endsWith(".csv");  
	}  

	getHeaderArray(csvRecordsArr: any) {  
		let headers = (<string>csvRecordsArr[0]).split(',');  
		let headerArray = [];  
		for (let j = 0; j < headers.length; j++) {  
			headerArray.push(headers[j]);  
		}  
		return headerArray;  
	}  

	fileReset() {  
		this.csvReader.nativeElement.value = "";  
		this.records = [];  
	} 

	uploadOrgEmployees() {
		console.log(this.csvRecords);

		this.dataService.uploadEmployees(this.csvRecords).subscribe(
			data => {
				console.log(data);
				this.summarypage = false;
				this.uploadtab = 'filetab';
				this.showOrgEmployees();
				$("#uploadEmployees .close").click();
				this.blockUI.stop();
			},
			err => {
				// console.log("inside My Organization error", JSON.stringify(err.message));
				this.blockUI.stop();
			}
		);
	} 

	deleteEmployee(org_id, emp_email) {
		// console.log(org_id+'---'+emp_email);
	}

	showOrgEmployees() {
		// get org employees
		this.dataService.getOrganizationEmployees(this.org_id).subscribe(
			data => {
				// console.log(data);
				this.viewEmployees = true;
				this.orgEmployees = data.org_employees;
			},
			err => {
				// console.log("inside My Organization error", JSON.stringify(err.message));
				this.blockUI.stop();
			}
		);
	}

	ngOnInit() {
		this.currentUser = this.authService.currentUserValue;
		this.org_id = this.currentUser.org_id;
		this.user_id = this.currentUser.id;

		this.showOrgEmployees();
	}

}
