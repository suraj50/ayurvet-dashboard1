import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

import { DataService } from '../../../services/data.service';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
	selector: 'app-dldashboard',
	templateUrl: './dldashboard.component.html',
	styleUrls: ['./dldashboard.component.css']
})
export class DldashboardComponent implements OnInit {

	@BlockUI() blockUI: NgBlockUI;

	constructor(
		private dataService: DataService,
        private authService: AuthenticationService
	) {
		this.blockUI.start('Loading...');
	}

	ngOnInit() {
		this.blockUI.stop();
	}

}
