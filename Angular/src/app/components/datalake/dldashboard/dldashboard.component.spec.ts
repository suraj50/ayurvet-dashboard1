import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DldashboardComponent } from './dldashboard.component';

describe('DldashboardComponent', () => {
  let component: DldashboardComponent;
  let fixture: ComponentFixture<DldashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DldashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DldashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
