import { Component, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

import { DataService } from '../../../services/data.service';
import { AuthenticationService } from '../../../services/authentication.service';

import { User } from '../../../models/user.model';

@Component({
	selector: 'app-dlmyorganization',
	templateUrl: './dlmyorganization.component.html',
	styleUrls: ['./dlmyorganization.component.css']
})
export class DlmyorganizationComponent implements OnInit {

	@BlockUI() blockUI: NgBlockUI;

	public currentUser: User;
	public org_details: any;
	constructor(
		private dataService: DataService,
        private authService: AuthenticationService
	) { 
		this.blockUI.start('Loading...');
	}

	ngOnInit() {
		this.currentUser = this.authService.currentUserValue;
		let org = {
			'org_id': this.currentUser.org_id
		};
		// console.log(this.currentUser);
		// getOrganizationProfileDetails
		this.dataService.getOrganizationProfileDetails(org).subscribe(
			data => {
				console.log(data);
				this.org_details = data.org_details;
				this.blockUI.stop();
			},
			err => {
				console.log("inside My Organization error", JSON.stringify(err.message));
				this.blockUI.stop();
			}
		);
	}

}
