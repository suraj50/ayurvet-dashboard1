import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlmyorganizationComponent } from './dlmyorganization.component';

describe('DlmyorganizationComponent', () => {
  let component: DlmyorganizationComponent;
  let fixture: ComponentFixture<DlmyorganizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlmyorganizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlmyorganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
