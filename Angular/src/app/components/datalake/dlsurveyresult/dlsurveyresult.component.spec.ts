import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DlsurveyresultComponent } from './dlsurveyresult.component';

describe('DlsurveyresultComponent', () => {
  let component: DlsurveyresultComponent;
  let fixture: ComponentFixture<DlsurveyresultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DlsurveyresultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DlsurveyresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
