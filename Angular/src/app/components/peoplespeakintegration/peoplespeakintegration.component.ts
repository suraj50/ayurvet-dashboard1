import { Component, OnInit, Input } from '@angular/core';

import { Project } from '../../models/project.model';

declare var ldBar: any;

@Component({
  selector: 'app-peoplespeakintegration',
  templateUrl: './peoplespeakintegration.component.html',
  styleUrls: ['./peoplespeakintegration.component.css']
})
export class PeoplespeakintegrationComponent implements OnInit {

  @Input() project: Project;
  public rcs: any;
  public currentView: any;
   public countUpView: any;
    public countUpReview: any;
    public countUpInterview: any;
   public countUpVote: any;
  constructor() {
  	 this.rcs = [
      {'code': 'rcwl2', 'prop': 'rc_work_life_balance1', 'label': 'Work-Life Balance', 'icon': 'balance-scale'},
      {'code': 'rcoe2', 'prop': 'rc_org_experience1', 'label': 'Organizational Experience', 'icon': 'sitemap'},
      {'code': 'rcsb2', 'prop': 'rc_salary_benefits1', 'label': 'Salary / Benefits', 'icon': 'wallet'},
      {'code': 'rcjc', 'prop': 'rc_job_security', 'label': 'Job Security', 'icon': 'user-shield'},
      {'code': 'rcmn', 'prop': 'rc_management', 'label': 'Management', 'icon': 'tasks'}
    ]
    this.countUpView = false;
    this.currentView = 'bnsEnt1';
   }

 showKnob(el, prop) {
    var bar = new ldBar('#' + el);
    bar.set( (this.project.pskc[prop] * 20));
  }

  hideKnob(el, prop) {
    var bar = new ldBar('#' + el);
    bar.set(0);
  }
  resetCountUp() {
    this.countUpReview = 0;
    this.countUpInterview = 0;
    this.countUpVote = 0;
    // this.resetfrom = 1;
    // this.countUpLikingOfBank = 0;
    this.countUpView = false;
  }

  setCountUp() {
    // console.log(this.projectDetails);
    this.countUpReview = this.project.pskc.review;
    this.countUpInterview = this.project.pskc.interview;
    this.countUpVote = 41829;
    // this.countUpLikingOfBank = 55;
    this.countUpView = true;
  }
  initStuff(){
  	
    let self = this;
    self.resetCountUp();
    setTimeout(function() { 
        for(let rc of self.rcs) {
           self.hideKnob(rc.code, rc.prop);
          self.showKnob(rc.code, rc.prop);
        }
        self.setCountUp();
    }, 100);
  }
  ngOnInit() {
      console.log(this.project);
    this.project.pskc = {
      "executiveSummary": "People Speak is a comprehensive report telling your organizations's story with actionable insights taking into account factors that offer more value than internal employee engagement surveys.",
      "opinion_index": 3.9,
      "review": "1612",
      "interview":"72",
      "ceo_approval":"90",
      "friend_recommend":"73",
      "rc_work_life_balance1": 3.63,
      "rc_org_experience1": 3.02,
      "rc_salary_benefits1": 3.98,
      "rc_job_security": 4.42,
      "rc_management": 3.88
    };

   
    
  }
 


  switchView(view) {
    this.currentView = view;
     let self = this;
     self.resetCountUp();
    setTimeout(function() { 
        for(let rc of self.rcs) {
           self.hideKnob(rc.code, rc.prop);
          self.showKnob(rc.code, rc.prop);
        }
        self.setCountUp();

    }, 100);
  }

}
