import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { LoginComponent } from './components/login/login.component'; 
import { MainComponent } from './components/main/main.component'; 
import { SurveyComponent } from './components/survey/survey.component'; 
import { DashboardComponent } from './components/dashboard/dashboard.component'; 
import { RegisterComponent } from './components/register/register.component'; 
import { AprojectsComponent } from './components/admin/projects/projects.component'; 
import { AprojectComponent } from './components/admin/project/project.component'; 
import { LSReportComponent } from './components/lsreport/lsreport.component'; 
import { MnagenomeComponent } from './components/mnagenome/mnagenome.component'; 
import { IngenomeComponent } from './components/ingenome/ingenome.component'; 
import { CulturegenomeComponent } from './components/culturegenome/culturegenome.component'; 
import { DlregistrationComponent } from './components/dl/dlregistration/dlregistration.component';
import { OrganizationComponent } from './components/admin/organization/organization.component';
import { DlregisterComponent } from './components/dlregister/dlregister.component';
import { LregisterComponent } from './components/lregister/lregister.component';
import { DlsurveyComponent } from './components/datalake/dlsurvey/dlsurvey.component';
import { DlcontactformComponent } from './components/dlcontactform/dlcontactform.component';
import { DlemployeeComponent } from './components/datalake/dlemployee/dlemployee.component';
import { DlmyorganizationComponent } from './components/datalake/dlmyorganization/dlmyorganization.component';
import { DlprojectsComponent } from './components/datalake/dlprojects/dlprojects.component';
import { DlresearchsurveyComponent } from './components/datalake/dlresearchsurvey/dlresearchsurvey.component';
import { DlsurveyresultComponent } from './components/datalake/dlsurveyresult/dlsurveyresult.component';
import { DlsurveyinsComponent } from './components/datalake/dlsurveyins/dlsurveyins.component';
import { DlresearchsurveyinsComponent } from './components/datalake/dlresearchsurveyins/dlresearchsurveyins.component';


import { AuthGuard } from './guards/auth.guard'; 

const routes: Routes = [
    { path: '', redirectTo: '/main', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'main', component: MainComponent, canActivate: [AuthGuard] },
    { path: 'dashboard/:projectId', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'mnagenome/:projectId', component: MnagenomeComponent, canActivate: [AuthGuard] },
    { path: 'ingenome/:projectId', component: IngenomeComponent, canActivate: [AuthGuard] },
    { path: 'culturegenome/:projectId', component: CulturegenomeComponent, canActivate: [AuthGuard] },

    { path: 'survey/:surveyCode', component: SurveyComponent },
    { path: 'dlresearchsurvey/:surveyCode', component: DlresearchsurveyComponent },
    { path: 'dlsurvey/:surveyCode', component: DlsurveyComponent },
    { path: 'dlsurveyins/:surveyCode', component: DlsurveyinsComponent },
    { path: 'survey', component: SurveyComponent },
    { path: 'register/:registerCode', component: RegisterComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'admin/projects', component: AprojectsComponent, canActivate: [AuthGuard] },
    { path: 'admin/project/:projectId', component: AprojectComponent, canActivate: [AuthGuard] },
    { path: 'lsreport/:lrId', component: LSReportComponent, canActivate: [AuthGuard] },
    { path: 'dl/dlregistration', component: DlregistrationComponent },
    { path: 'admin/organization', component: OrganizationComponent, canActivate: [AuthGuard] },
    { path: 'dlcontactform/:surveyLink', component: DlcontactformComponent },
    { path: 'dlemployee', component: DlemployeeComponent, canActivate: [AuthGuard] },
    { path: 'dlmyorganization', component: DlmyorganizationComponent, canActivate: [AuthGuard] },
    { path: 'dlprojects', component: DlprojectsComponent, canActivate: [AuthGuard] },
    { path: 'dlsurveyresult/:surveyCode', component: DlsurveyresultComponent },
     { path: 'dlresearchsurveyins/:surveyCode', component: DlresearchsurveyinsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
