export class Project {
    id: number;
    title: string;
    psk: any;
    pskc: any;
    lsp: any;
    ilsp: any;
    bns: any;
    bnsc: any;
    cva: any;
    cvac: any;
    inp: any;
    competencies: any;
    icompetencies: any;
    participans: any;
    myparticipant: any;
    surveys: any;
    survey_questions: any;
    survey_answers: any;
    values: any;
    lsreport: any;
    pskcs: any;
    project_variation_type: any;

    constructor()  { 
         this.psk = {};
         this.pskc = {};
         this.cvac = {};
         this.lsp = {};
         this.bnsc = {};
         this.pskcs = {};
        
    }
    
}
